/*
 * parameter_estimation.h
 *
 *  Created on: Nov 20, 2020
 *
 */

#ifndef SRC_PARAMETER_ESTIMATION_H_
#define SRC_PARAMETER_ESTIMATION_H_

/*Boundary Conditions*/
#define K_rate_pos_upper 2.4701*10^(-6)f;
#define K_rate_pos_lower 2.0262*10^(-6)f;
#define K_rate_neg_upper 5.3357*10^(-6)f;
#define K_rate_neg_lower 4.3419*10^(-6)f;
#define epsilon_neg_upper 0.515f;
#define epsilon_neg_lower 0.465f;

#define Theta_upper[3][1]={K_rate_pos_upper},{ K_rate_neg_upper},{epsilon_neg_upper};
#define Theta_lower[3][1]={K_rate_pos_lower}, {K_rate_neg_lower},{epsilon_neg_lower};

/*Latin hypercube sampling*/
#define lhsdesign[]=(0.4995f,0.5284f,0.9404f,0.3180f);


float get_x0[];


#endif /* SRC_PARAMETER_ESTIMATION_H_ */
