/*
 * switching_states.h
 *
 *  Created on: 11 Aug 2020
 *      Author: Nicholas Konidaris
 */

#ifndef INC_SWITCHING_STATES_H_
#define INC_SWITCHING_STATES_H_

#include <stdint.h>
#include "gpio.h"

/* Possible Interconnection States
 *
 *	I2C Data Packet:
 *
 *	|_|_|_|_|_|_|_|_|
 *	 0 1 2 3 4 5 6 7
 *
 *	0 = Interconnect 1
 *	1 = Interconnect 1 : Positive = 1 / Negative Half Cycle = 0
 *	2 = Interconnect 2
 *	3 = Interconnect 2 : Positive = 1 / Negative Half Cycle = 0
 *	4 = Interconnect 3
 *	5 = Interconnect 3 : Positive = 1 / Negative Half Cycle = 0
 *	6 = Interconnect 4
 *	7 = Interconnect 4 : Positive = 1 / Negative Half Cycle = 0
 *
 *
 *	Interconnect 1-3
 *		0b = Series
 *		1b = Parallel
 *
 *	Interconnect 4
 *		0b = Series
 *		1b = Bypass
 *
 * */

/* Module number in sequence
 * - Uncomment for respective module number
 */
#define Module1
//#define Module2
//#define Module3
//#define Module4
//#define Module5

#define interconnect_1 7
#define interconnect_2 5
#define interconnect_3 3
#define interconnect_4 1

#define state_series_positive 		1
#define state_series_negative 	   -1
#define state_bypass_positive  		0
#define state_bypass_negative  		0
#define state_zero		  			0
#define state_parallel_positive 	0
#define state_parallel_negative 	0

/* Modulation Functions */
void startModulation();
void decodeState(uint8_t state);




#endif /* INC_SWITCHING_STATES_H_ */
