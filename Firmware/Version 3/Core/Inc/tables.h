/*
 * tables.h
 *
 *  Created on: 22 Sep 2020
 *      Author: NicholasKonidaris
 */

#ifndef INC_TABLES_H_
#define INC_TABLES_H_

extern const float R_NEG_Table[];
extern const float R_POS_Table[];
extern const float I_BAT[];
extern const float V_BAT_MATLAB[];
extern const float SOC_MATLAB[];

#endif /* INC_TABLES_H_ */
