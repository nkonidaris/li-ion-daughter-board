/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define Vbattery_Pin GPIO_PIN_0
#define Vbattery_GPIO_Port GPIOF
#define EXPIO_PF1_Pin GPIO_PIN_1
#define EXPIO_PF1_GPIO_Port GPIOF
#define FET_2_APWM_Pin GPIO_PIN_10
#define FET_2_APWM_GPIO_Port GPIOG
#define FET_2_BPWM_Pin GPIO_PIN_0
#define FET_2_BPWM_GPIO_Port GPIOA
#define FET_2_EN_Pin GPIO_PIN_1
#define FET_2_EN_GPIO_Port GPIOA
#define Vcurrent_Pin GPIO_PIN_2
#define Vcurrent_GPIO_Port GPIOA
#define Module_Selector_Pin GPIO_PIN_3
#define Module_Selector_GPIO_Port GPIOA
#define FET_1_APWM_Pin GPIO_PIN_4
#define FET_1_APWM_GPIO_Port GPIOA
#define FET_1_BPWM_Pin GPIO_PIN_5
#define FET_1_BPWM_GPIO_Port GPIOA
#define FET_1_EN_Pin GPIO_PIN_6
#define FET_1_EN_GPIO_Port GPIOA
#define LED_PWM_Pin GPIO_PIN_7
#define LED_PWM_GPIO_Port GPIOA
#define Vtemp_Pin GPIO_PIN_0
#define Vtemp_GPIO_Port GPIOB
#define EXPIO_PA15_Pin GPIO_PIN_15
#define EXPIO_PA15_GPIO_Port GPIOA
#define EXPIO_PB4_Pin GPIO_PIN_4
#define EXPIO_PB4_GPIO_Port GPIOB
#define nBootloader_Pin GPIO_PIN_6
#define nBootloader_GPIO_Port GPIOB
#define nBootloader_EXTI_IRQn EXTI9_5_IRQn
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
