/*
 * state_estimation.h
 *
 *  Created on: Aug 27, 2020
 *      Author: Nicholas Konidaris
 */

#ifndef INC_STATE_ESTIMATION_H_
#define INC_STATE_ESTIMATION_H_

/* State Estimation Algorithm:
 *
 * Based off paper by Yang Et al.
 * DOI: 10.1016/j.apenergy.2019.04.143
 * Development of a degradation-conscious physics-based lithium-ion
 * battery model for use in power system planning studies
 *
 */

/* Battery Parameters */

/* Thermodynamics */

#define BAT_R 8.314f
#define BAT_T 295.65f
#define BAT_F 96487

/* Positive Electrode */

#define BAT_POS_Rp 2e-6f
#define BAT_POS_L 80.0e-06f
#define BAT_POS_Porosity 0.3850f
#define BAT_POS_Filler_Fraction 0.025f
#define BAT_POS_Epsilon (1 - BAT_POS_Porosity - BAT_POS_Filler_Fraction)
#define BAT_POS_CS_MAX 51555
#define BAT_POS_Ds 1.0000e-14f
#define BAT_POS_De 7.5e-10f
#define BAT_POS_Rf 0
#define BAT_POS_Brugg 4
#define BAT_POS_Reff 2.2520e-06f
#define BAT_POS_Keff (BAT_POS_Reff / BAT_F)
#define BAT_POS_Lambda 2.1f
#define BAT_POS_Cp 700
#define BAT_POS_Rho 2500
#define BAT_POS_Sigma 100
#define BAT_POS_Ea_Ds 5000
#define BAT_POS_Ea_K 5000
#define BAT_POS_A ((3 / BAT_POS_Rp) * BAT_POS_Epsilon)

/* Negative Electrode */

#define BAT_NEG_Rp 2e-6f
#define BAT_NEG_L 88.0e-06f
#define BAT_NEG_Porosity 0.4850f
#define BAT_NEG_Filler_Fraction 0.025f
#define BAT_NEG_Epsilon (1 - BAT_NEG_Porosity - BAT_NEG_Filler_Fraction)
#define BAT_NEG_CS_MAX 30555
#define BAT_NEG_Ds 3.9000e-14f
#define BAT_NEG_De 7.5e-10f
#define BAT_NEG_Rf 0.01f
#define BAT_NEG_Brugg 4
#define BAT_NEG_Reff 4.854e-6f
#define BAT_NEG_Keff (BAT_NEG_Reff / BAT_F)
#define BAT_NEG_Lambda 1.7f
#define BAT_NEG_Cp 700
#define BAT_NEG_Rho 2500
#define BAT_NEG_Sigma 100
#define BAT_NEG_Ea_Ds 5000
#define BAT_NEG_Ea_K 5000
#define BAT_NEG_A ((3 / BAT_NEG_Rp) * BAT_NEG_Epsilon)

/* Seperator */

#define BAT_SEP_L 20e-6f
#define BAT_SEP_De 7.5e-10f
#define BAT_SEP_Porosity 0.724f
#define BAT_SEP_Brugg 4
#define BAT_SEP_Lambda 0.16f
#define BAT_SEP_Cp 700
#define BAT_SEP_Rho 1100

/* Electrolyte */

#define BAT_CSS 1
#define BAT_CE0 1000
#define BAT_TC0 0.363f
#define BAT_Tplus 0.363f
#define BAT_Rcol 0

/* Degradation */

#define BAT_Kappa 0.01f
#define BAT_I0S 1.5e-6f
#define BAT_US_REF 0.4f
#define BAT_Mp 7.3e4f
#define BAT_Rho 2.1e3f
#define BAT_Qmax0 1.8f

/* Initial Stoichiometry at BOL, when battery is considered 0% SOC */

#define BAT_POS_X0 0.95f
#define BAT_NEG_X0 0.03f

#define BAT_POS_X1 0.487f
#define BAT_NEG_X1 0.885131177119395f

#define BAT_A (3600*BAT_Qmax0/((BAT_POS_X0 - BAT_POS_X1)*BAT_POS_L * BAT_POS_Epsilon * BAT_POS_CS_MAX * BAT_F))

/* Others */


#define X_POS0 0.75f
#define X_NEG0 (BAT_POS_L * BAT_POS_Epsilon * BAT_POS_CS_MAX * (BAT_POS_X0 - X_POS0) / (BAT_NEG_L * BAT_NEG_Epsilon * BAT_NEG_CS_MAX)+ BAT_NEG_X0)


#define Q_POS0 ((BAT_A * BAT_POS_L * BAT_F * BAT_POS_Epsilon * BAT_POS_CS_MAX)*( BAT_POS_X0 - X_POS0)/3600)
#define Q_NEG0 ((BAT_A * BAT_NEG_L * BAT_F * BAT_NEG_Epsilon * BAT_NEG_CS_MAX)*( BAT_NEG_X1 - X_NEG0)/3600)

#define Ts 0.025f // Time-Step
#define T0 298.15f

float get_SOC(float Current);
float get_SOH();
float get_SOE(float SOC);
float get_Estimated_Voltage(float Current, float Temperature);




#endif /* INC_STATE_ESTIMATION_H_ */
