/*
 * state_estimation.c
 *
 *  Created on: Aug 27, 2020
 *      Author: Nicholas Konidaris
 */

#include "state_estimation.h"
#include <math.h>
#include <stdint.h>
#include "tables.h"

/* Local Helper Functions */

float get_Q_POS(float Current, float Q_LOSS_NEG_Previous);
float get_Q_LOSS_NEG(float V_NEG, float Current, float CS_AVG_NEG);
float get_OCP_POS(float x, float Temperature);
float get_OCP_NEG(float x);
float get_XS_AVG_NEG(float Q_NEG, float Q_LOSS_NEG);
float get_XS_AVG_POS(float Q_POS);
float get_R_ETA_POS(float CS_AVG, float Temperature);
float get_R_ETA_NEG(float CS_AVG, float Temperature);
float get_i0_POS(float CS_AVG, float Temperature);
float get_i0_NEG(float CS_AVG, float Temperature);
float get_RF_NEG();
float get_RE();
float get_KAPPA_K();

static float Q_LOSS_NEG = 0;
static float Q_POS = Q_POS0;
/* Gets initialized in SOC *Make sure SOC is calculated first*  */
static float V_NEG;
static float CS_AVG_NEG;
static float XS_AVG_NEG;

/* Parameter Estimation Algorithm */

float get_SOC(float Current) {

	Q_POS = get_Q_POS(Current, Q_LOSS_NEG);

	/* Battery Voltage Simulation BEGIN */

	/* Battery Voltage Simulation END */

	float Q_NEG = ((Q_NEG0 + Q_POS0) - Q_POS + Q_LOSS_NEG);
	XS_AVG_NEG = get_XS_AVG_NEG(Q_NEG, Q_LOSS_NEG);
	CS_AVG_NEG = XS_AVG_NEG * BAT_NEG_CS_MAX;
	V_NEG = get_OCP_NEG(XS_AVG_NEG);
	Q_LOSS_NEG = get_Q_LOSS_NEG(V_NEG, Current, CS_AVG_NEG);

	return ((Q_POS - Q_LOSS_NEG) / (Q_NEG0 + Q_POS0));

}

float get_SOH() {

	/* Only updates with SOC update */

	return ((BAT_Qmax0 - Q_LOSS_NEG) / BAT_Qmax0);

}

float get_SOE(float SOC) {

	return 0;

}

float get_Estimated_Voltage(float Current, float Temperature) {

	float XS_AVG_POS = get_XS_AVG_POS(Q_POS);
	float V_POS = get_OCP_POS(XS_AVG_POS, Temperature);
	float CS_AVG_POS = XS_AVG_POS * BAT_POS_CS_MAX;

	float R_ETA_POS = get_R_ETA_POS(CS_AVG_POS, Temperature);
	float R_ETA_NEG = get_R_ETA_NEG(CS_AVG_NEG, Temperature);

	//2.021598272138229 + (-0.003887688984881)x

	uint16_t tmp_index = (uint16_t) ((-Q_POS + 2.021598272138229f)
			/ 0.003887688984881f);
	float R_POS = 7 * R_POS_Table[tmp_index];
	tmp_index = (uint16_t) ((-XS_AVG_NEG + 0.9923f) / 0.001846935587731f);
	float R_NEG = 7 * R_NEG_Table[tmp_index];

	float RF_NEG = get_RF_NEG();
	float RE = 0.1762f;   //get_RE();

	float tmp_Voltage = R_ETA_POS + R_ETA_NEG + RF_NEG + RE + R_POS + R_NEG;

	tmp_Voltage *= Current;

	return (tmp_Voltage + (V_POS - V_NEG));
}

float get_RE() {

	float Kappa_K = get_KAPPA_K();

	float a = (BAT_POS_L / Kappa_K) + (BAT_SEP_L * 2 / Kappa_K)
			+ (BAT_NEG_L / Kappa_K);

	a *= 0.5 * (1 / BAT_A);

	return a;

}

float get_KAPPA_K() {

	float cem = BAT_CE0 / 1000.0f;

	float a = 4.1253E-4f
			+ (cem * 5.007E-3f) * (-4.7212E-3f * powf(cem, 2))
					* (1.5094E-3f * powf(cem, 3))
					* (-1.6018E-4f * powf(cem, 4));

	return a;

}

float get_RF_NEG() {

	float rf = Q_LOSS_NEG * 3600.0f * (1 / (BAT_NEG_L * BAT_A))
			* (BAT_Mp / (BAT_NEG_A * BAT_Rho * BAT_Kappa * BAT_F));
	rf += BAT_NEG_Rf;

	return (rf / (BAT_NEG_L * BAT_A * BAT_NEG_A));

}

float get_R_ETA_POS(float CS_AVG, float Temperature) {

	float i0 = get_i0_POS(CS_AVG, Temperature);

	float f = BAT_POS_A * BAT_POS_L * BAT_F * BAT_A;

	return (BAT_R * Temperature / i0 / f);

}

float get_R_ETA_NEG(float CS_AVG, float Temperature) {

	float i0 = get_i0_NEG(CS_AVG, Temperature);

	float f = BAT_NEG_A * BAT_NEG_L * BAT_F * BAT_A;

	return (BAT_R * Temperature / i0 / f);

}

float get_i0_POS(float CS_AVG, float Temperature) {

	float c = sqrtf(BAT_CE0 * CS_AVG * (BAT_POS_CS_MAX - CS_AVG));

	float T1 = (1.0f / Temperature) - (1.0f / BAT_T);
	T1 *= (-BAT_POS_Ea_K / BAT_R);

	return (c * BAT_POS_Reff * expf(T1));

}

float get_i0_NEG(float CS_AVG, float Temperature) {

	float c = sqrtf(BAT_CE0 * CS_AVG * (BAT_NEG_CS_MAX - CS_AVG));

	float T1 = (1.0f / Temperature) - (1.0f / BAT_T);
	T1 *= (-BAT_NEG_Ea_K / BAT_R);

	return (c * BAT_NEG_Reff * expf(T1));

}

float get_XS_AVG_POS(float Q_POS) {

	return (BAT_POS_X0
			- ((Q_POS * 3600.0f)
					/ (BAT_A * BAT_POS_L * BAT_F * BAT_POS_Epsilon
							* BAT_POS_CS_MAX)));

}

float get_Q_POS(float Current, float Q_LOSS_NEG_Previous) {
	static float Q_POS_Previous = Q_POS0;
	static float Current_Previous = 0;

	float Q_POS_TMP = ((Current_Previous * Ts) / 3600) + Q_POS_Previous;

	Current_Previous = Current;

	Q_POS_Previous = Q_POS_TMP;

	return Q_POS_TMP;

}

float get_XS_AVG_NEG(float Q_NEG, float Q_LOSS_NEG) {

	/* Not Considering Loss of Active Material */
	Q_LOSS_NEG = 0;

	return (BAT_NEG_X1
			- ((3600 * Q_NEG)
					/ (BAT_A * BAT_NEG_L * BAT_F * BAT_NEG_Epsilon
							* BAT_NEG_CS_MAX * (1 - Q_LOSS_NEG))));

}

float get_Q_LOSS_NEG(float V_NEG, float Current, float CS_AVG_NEG) {

	/* TODO: Rate conversion to 60 option */

	static float Q_LOSS_NEG_Previous = 0;
	static float ISR_NEG = 0;
	float jsr = 0;

	float Q_LOSS_NEG = ((Ts * ISR_NEG) + Q_LOSS_NEG_Previous) / -3600;

	Q_LOSS_NEG_Previous = Q_LOSS_NEG;

	if (Current < 0) { // Current charging battery is Positive

		float A = (-BAT_I0S * BAT_NEG_A)
				* (expf(((BAT_US_REF - V_NEG) * BAT_F) / (BAT_T * 2 * BAT_R)));

		float c = (sqrtf(CS_AVG_NEG * BAT_CE0 * (BAT_NEG_CS_MAX - CS_AVG_NEG))
				* BAT_NEG_Reff) * 2 * BAT_NEG_A;

		float B = Current / (BAT_NEG_L * BAT_A) / c;

		float C = 1 / c;

		float ac = 1 - (2 * A * C);

		jsr = (A * (sqrtf(B * B + ac)) + A * B) / ac;

	}

	ISR_NEG = jsr * BAT_NEG_L * BAT_A;

	return Q_LOSS_NEG;

}

float get_OCP_POS(float x, float Temperature) {

	float x_2 = x * x;
	float x_3 = x_2 * x;
	float x_4 = x_3 * x;
	float x_6 = x_3 * x_3;
	float x_8 = x_4 * x_4;
	float x_10 = x_6 * x_4;

	float dudt = -0.001f
			* (0.199521039f - 0.928373822f * x + 1.364550689000003f * x_2
					- 0.6115448939999998f * x_3);
	dudt /= (1 - 5.661479886999997f * x + 11.47636191f * x_2
			- 9.82431213599998f * x_3 + 3.048755063f * x_4);
	float OCP = (-4.656f + 88.669f * x_2 - 401.119f * x_4 + 342.909f * x_6
			- 462.471f * x_8 + 433.434f * x_10);
	OCP /= (-1 + 18.933f * x_2 - 79.532f * x_4 + 37.311f * x_6 - 73.083f * x_8
			+ 95.96f * x_10);
	OCP += (Temperature - T0) * dudt;

	return OCP;

}

float get_OCP_NEG(float x) {

	return (0.7222f + 0.1387f * x + 0.029f * sqrtf(x) - 0.0172f / x
			+ 0.0019f / sqrtf(x * x * x) + 0.2808f * expf(0.9f - 15 * x)
			- 0.7984f * expf(0.4465f * x - 0.4108f));

}

