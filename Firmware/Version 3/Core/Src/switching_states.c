/*
 * switching_states.c
 *
 *  Created on: Aug 27, 2020
 *      Author: Nicholas Konidaris
 */

#include <stdbool.h>
#include "switching_states.h"
#include "tim.h"

//volatile int NameState, NameState1;

/* Modulator Functions */
void decodeState(uint8_t state) {

	/* Interconnect Between Previous and Next Module */
	bool connectionPrev, connectionNext, positivePrev, positiveNext;
	static bool LED = false;

	if (LED) {
		__HAL_TIM_SET_COMPARE(&htim17, TIM_CHANNEL_1, 0);
		LED = false;
	} else {
		__HAL_TIM_SET_COMPARE(&htim17, TIM_CHANNEL_1, 5000);
		LED = true;
	}

	/* Switching States:
	 * 11 = Series Positive
	 * 10 = Series Negative
	 * 01 = Parallel / Bypass Positive
	 * 00 = Parallel / Bypass Negative
	 */

#ifdef Module1
	/* Connect Previous is constant */
	connectionPrev = state >> interconnect_4 & 1;
	positivePrev = state >> (interconnect_4 - 1) & 1;
	connectionNext = state >> interconnect_1 & 1;
	positiveNext = state >> (interconnect_1 - 1) & 1;
#endif

#ifdef Module2
	connectionPrev = state >> interconnect_1 & 1;
	positivePrev = state >> (interconnect_1 - 1) & 1;
	connectionNext = state >> interconnect_2 & 1;
	positiveNext = state >> (interconnect_2 - 1) & 1;
#endif

#ifdef Module3
	connectionPrev = state >> interconnect_2 & 1;
	positivePrev = state >> (interconnect_2 - 1) & 1;
	connectionNext = state >> interconnect_3 & 1;
	positiveNext = state >> (interconnect_3 - 1) & 1;
#endif

#ifdef Module4
	/* Connect Next is constant */
	connectionPrev = state >> interconnect_3 & 1;
	positivePrev = state >> (interconnect_3 - 1) & 1;
	connectionNext = state >> interconnect_4 & 1;
	positiveNext = state >> (interconnect_4 - 1) & 1;
#endif

#if  defined(Module2) || defined(Module3)

	if (connectionPrev) {
		if (positivePrev) {
			/* Positive Series Interconnection */
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_RESET);
		} else {
			/* Negative Series Interconnection */
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_SET);
		}
	} else {
		if (positivePrev) {
			/* Positive Parallel Interconnection */
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_RESET);
		} else {
			/* Negative Parallel Interconnection */
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_SET);
		}
	}

	if (connectionNext) {
		if (positiveNext) {
			/* Positive Series Interconnection */
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port,
			FET_2_BPWM_Pin, GPIO_PIN_SET);
		} else {
			/* Negative Series Interconnection */
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port,
			FET_2_BPWM_Pin, GPIO_PIN_RESET);
		}
	} else {
		if (positiveNext) {
			/* Positive Parallel Interconnection */
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port,
			FET_2_BPWM_Pin, GPIO_PIN_SET);
		} else {
			/* Negative Parallel Interconnection */
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port,
			FET_2_BPWM_Pin, GPIO_PIN_RESET);
		}
	}

#endif

#ifdef Module1

	if (connectionPrev) {
		if (positivePrev) {
			/* Positive Series Interconnection */
			//NameState = state_series_positive;
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_RESET);
		} else {
			/* Negative Series Interconnection */
			//NameState = state_series_negative;
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_SET);
		}
	} else {
		if (positivePrev) {
			/* Positive Bypass Interconnection */
			//NameState = state_bypass_positive;
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_SET);
		} else {
			/* Negative Bypass Interconnection */
			//NameState = state_bypass_negative;
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_RESET);
		}
	}

	if (connectionNext) {
		if (positiveNext) {
			/* Positive Series Interconnection */
			//NameState1 = state_series_positive;
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port,
			FET_2_BPWM_Pin, GPIO_PIN_SET);
		} else {
			/* Negative Series Interconnection */
			//NameState1 = state_series_negative;
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port,
			FET_2_BPWM_Pin, GPIO_PIN_RESET);
		}
	} else {
		if (positiveNext) {
			/* Positive Parallel Interconnection */
			//NameState1 = state_parallel_positive;
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port,
			FET_2_BPWM_Pin, GPIO_PIN_SET);
		} else {
			/* Negative Parallel Interconnection */
			//NameState1 = state_parallel_negative;
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port,
			FET_2_BPWM_Pin, GPIO_PIN_RESET);
		}
	}

#endif

#ifdef Module4

	if (connectionPrev) {
		if (positivePrev) {
			/* Positive Series Interconnection */
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_RESET);
		} else {
			/* Negative Series Interconnection */
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_SET);
		}
	} else {
		if (positivePrev) {
			/* Positive Parallel Interconnection */
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_RESET);
		} else {
			/* Negative Parallel Interconnection */
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_SET);
		}
	}

	if (connectionNext) {
		if (positiveNext) {
			/* Positive Series Interconnection */
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port, FET_2_BPWM_Pin,
					GPIO_PIN_SET);
		} else {
			/* Negative Series Interconnection */
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port, FET_2_BPWM_Pin,
					GPIO_PIN_RESET);
		}
	} else {
		if (positiveNext) {
			/* Positive Bypass Interconnection */
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port, FET_2_BPWM_Pin,
					GPIO_PIN_SET);
		} else {
			/* Negative Bypass Interconnection */
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port, FET_2_BPWM_Pin,
					GPIO_PIN_RESET);
		}
	}

#endif

#ifdef Module5

	if (connectionPrev) {
		if (positivePrev) {
			/* Positive Series Interconnection */
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_RESET);
		} else {
			/* Negative Series Interconnection */
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_SET);
		}
	} else {
		if (positivePrev) {
			/* Positive Bypass Interconnection */
			//NameState = state_bypass_positive;
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_SET);
		} else {
			/* Negative Bypass Interconnection */
			//NameState = state_bypass_negative;
			HAL_GPIO_WritePin(FET_1_APWM_GPIO_Port, FET_1_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_1_BPWM_GPIO_Port,
			FET_1_BPWM_Pin, GPIO_PIN_RESET);
		}
	}

	if (connectionNext) {
		if (positiveNext) {
			/* Positive Series Interconnection */
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port, FET_2_BPWM_Pin,
					GPIO_PIN_SET);
		} else {
			/* Negative Series Interconnection */
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port, FET_2_BPWM_Pin,
					GPIO_PIN_RESET);
		}
	} else {
		if (positiveNext) {
			/* Positive Bypass Interconnection */
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_SET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port, FET_2_BPWM_Pin,
					GPIO_PIN_SET);
		} else {
			/* Negative Bypass Interconnection */
			HAL_GPIO_WritePin(FET_2_APWM_GPIO_Port, FET_2_APWM_Pin,
					GPIO_PIN_RESET);
			HAL_GPIO_WritePin(FET_2_BPWM_GPIO_Port, FET_2_BPWM_Pin,
					GPIO_PIN_RESET);
		}
	}

#endif

}

/* Call once after initial switching states received */
void startModulation() {
#if  defined(Module1) || defined(Module2) || defined(Module3) || defined(Module4) || defined(Module5)
	HAL_GPIO_WritePin(FET_1_EN_GPIO_Port, FET_1_EN_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(FET_2_EN_GPIO_Port, FET_2_EN_Pin, GPIO_PIN_SET);
#endif

}

