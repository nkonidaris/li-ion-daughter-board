/*
 * parameter_estimation.c
 *
 *  Created on: Nov 11, 2020
 *
 */

#include "parameter_estimation.h"
#include <math.h>
#include <stdint.h>

/* Get Real Battery Voltage data for past one hour */

float get_Battery_Voltage();/*need to extract one hour data from memory*/

/* Nelder-mead simplex constants */
int lhist=0;
int fcount=0;
int rho=1;
int chi=2;
float gamma=0.5;
float sigma=0.5;

float Theta_upper[3];
float Theta_lower[3];

/*setting the initial conditions*/

float get_x0 (Theta_upper, Theta_lower, lhsdesign ) {

		x0= (Theta_lower+((Theta_upper-Theta_lower)*lhsdesign));

		return x0;
}
float get_Estimated_Voltage(float Current, float Temperature);
