/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * @nkonidaris
 * @DULMINIK
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
/* Standard Libraries */
#include <stdio.h>
#include <stdbool.h>
#include "math.h"
#include "usbd_cdc_if.h"

/* Private Libraries */
#include "switching_states.h"
#include "state_estimation.h"
#include "parameter_estimation.h"
#include "tables.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* Order of ADC Sample's Array */
#define ADC_Battery_Current 0
#define ADC_Battery_Voltage 1
#define ADC_Battery_Temperature 2
#define ADC_Internal_Temperature 3
#define ADC_Internal_Vrefint 4

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* ADC Raw Data Samples
 * -Array Structure:
 *  -Element 0: Battery Current
 * 		     1: Battery Voltage
 * 		     2: Battery Temperature
 * 		     3: Temperature Internal
 * 		     4: Vrefint
 */
uint16_t ADC_Samples[5];
float Vref = 3.307f;

/* Current Measurements */
float Battery_Current;

/* Voltage Measurements */
float Battery_Voltage;

/* Temperature Measurements */
float Battery_Temperature;

/* I2C Switching State */
uint8_t state;

/* State Estimation Variables */
bool Trigger_Estimation_Start = false;
float SOC = 0.0f;
float SOH = 0.0f;
float SOE = 0.0f;
float EstimatedVoltage = 0.0f;

/* Simulated MATLAB Current Counter */
uint16_t I_BAT_COUNT = 0;

/* I2C transmit package with state estimations
 * SOC SOH SOE
 */

uint16_t State_Estimation_Transmit[3] = { 0 };

/* USB debugging information */

bool Trigger_USB_Update_Start = false;

/* Fault Flags */
bool OverCurrent = false;
bool nanFlag = false;

/* LED debug */

uint8_t LED_Pulses = 1;
uint16_t LED_Counter = 0;
bool LED_Fade_ON = false;
bool LED_Status = true;


/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* ADC Get Functions */
float get_Battery_Current();
float get_Battery_Voltage();
float get_Battery_Temperature();

/* Enter bootloader mode */
void JumpToBootloader(void);

/* Fault Detection Function */
void FaultDetected(uint8_t fault);

void state_Estimation_Process();

/*Parameter Identification*/
void parameter_identification_process();

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_I2C1_Init();
  MX_I2C3_Init();
  MX_USART1_UART_Init();
  MX_USB_Device_Init();
  MX_TIM17_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_TIM15_Init();
  /* USER CODE BEGIN 2 */

	/* ADC Automatic Calibration */
	HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED);
	//uint32_t VREFINT_CAL = HAL_ADCEx_Calibration_GetValue(&hadc1);

	/* Start DMA ADC Sequence */
	HAL_ADC_Start_DMA(&hadc1, (uint32_t*) ADC_Samples, 5);

	/* Start I2C Listen Mode */
	//HAL_I2C_EnableListen_IT(&hi2c1);
	HAL_I2C_Slave_Receive_IT(&hi2c1, &state, 1);

	//HAL_I2C_Slave_Transmit_DMA(&hi2c1, (uint8_t*) State_Estimation_Transmit, 6);

	/* ADC Timer: 125us */
	HAL_TIM_Base_Start_IT(&htim15);

	/* State Estimation Timer: 2.5ms */
	HAL_TIM_Base_Start_IT(&htim7);


#ifdef DEBUG
	/* USB debugging update: 1s */
	HAL_TIM_Base_Start_IT(&htim6);

#endif

	/* LED PWM Timer */
	HAL_TIM_PWM_Start(&htim17, TIM_CHANNEL_1);

	/* Debug Flags */

	/* Debug LED */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {

		/* Testing Code */

#ifdef DEBUG

		if (Trigger_USB_Update_Start) {

			uint8_t update_Buffer[200];

			uint8_t len_Buffer =
					sprintf((char*) update_Buffer,
							"SOC = %f, SOH = %f, Est-Voltage = %f, Current = %f, Voltage = %f \r\n",
							(double) SOC, (double) SOH,
							(double) EstimatedVoltage,
							(double) get_Battery_Current(),
							(double) get_Battery_Voltage());

			CDC_Transmit_FS(update_Buffer, len_Buffer);

			Trigger_USB_Update_Start = false;

		}

#endif

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};
  RCC_CRSInitTypeDef pInit = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV6;
  RCC_OscInitStruct.PLL.PLLN = 108;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV6;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the peripherals clocks
  */
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_I2C3|RCC_PERIPHCLK_USB
                              |RCC_PERIPHCLK_ADC12;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c3ClockSelection = RCC_I2C3CLKSOURCE_PCLK1;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL;
  PeriphClkInit.Adc12ClockSelection = RCC_ADC12CLKSOURCE_SYSCLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configures CRS
  */
  pInit.Prescaler = RCC_CRS_SYNC_DIV1;
  pInit.Source = RCC_CRS_SYNC_SOURCE_USB;
  pInit.Polarity = RCC_CRS_SYNC_POLARITY_RISING;
  pInit.ReloadValue = __HAL_RCC_CRS_RELOADVALUE_CALCULATE(48000000,1000);
  pInit.ErrorLimitValue = 34;
  pInit.HSI48CalibrationValue = 32;

  HAL_RCCEx_CRSConfig(&pInit);
}

/* USER CODE BEGIN 4 */

/* Callback Routines */

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {

	if (htim->Instance == TIM7) {
		/* 2.5ms Update Interval */
		state_Estimation_Process();
	} else if (htim->Instance == TIM6) {
		/* 1 Second Update Interval */
#ifdef DEBUG
		Trigger_USB_Update_Start = true;
#endif

	}

}

void HAL_ADC_LevelOutOfWindowCallback(ADC_HandleTypeDef *hadc) {

	OverCurrent = true;


}

void HAL_I2C_SlaveTxCpltCallback(I2C_HandleTypeDef *hi2c) {

	//HAL_I2C_Slave_Transmit_DMA(&hi2c1, (uint8_t*) State_Estimation_Transmit, 6);

}

void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c) {
	static bool First_Start = true;

	decodeState(state);

	if (First_Start) {
		startModulation();
		First_Start = false;
	}

	HAL_I2C_Slave_Receive_IT(&hi2c1, &state, 1);

}

void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c) {

	//HAL_NVIC_SystemReset();

}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {

	//if(GPIO_Pin == nBootloader_Pin)
	//	JumpToBootloader();

}

/* Helper Functions */
/* Convert and Get information from ADC */

float get_Battery_Current() {

	float Current_Conv = ADC_Samples[ADC_Battery_Current] / 4095.0f * Vref;
	return (Current_Conv - 1.5f) / 0.2f; //147 offset

}

float get_Battery_Voltage() {

	return ADC_Samples[ADC_Battery_Voltage] / 4095.0f * Vref * 1.5f;

}

float get_Battery_Temperature() {

	return ADC_Samples[ADC_Battery_Temperature] / 4095.0f * Vref;

}

/* State Estimation Function */

void state_Estimation_Process() {

	float Current = I_BAT[I_BAT_COUNT]; //-get_Battery_Current();
	float Temperature = BAT_T; //get_Battery_Temperature()

	SOC = get_SOC(Current);
	SOH = get_SOH();
	SOE = get_SOE(SOC);
	EstimatedVoltage = get_Estimated_Voltage(Current, Temperature);

	if (isnanf(SOC) || isnanf(SOH) || isnanf(SOE) || isnanf(EstimatedVoltage)) {
		nanFlag = true;
	}

	State_Estimation_Transmit[0] = (uint16_t) (SOC * 65535.0);
	State_Estimation_Transmit[1] = (uint16_t) (SOH * 65535.0);
}
/*Parameter Estimation Function*/


		//-----------------------------------------------------------------------------
		// Implementation of a cost function
		//-----------------------------------------------------------------------------

		typedef struct {
		  float a;
		  float b;
		  float c;
		  float d;
		  float e;
		} leastsq_param_t;

		float arg[5]={-2.10, -3.04, 4.50, 5.90,6.78};

		void leastsq_fun(int n,point_t *point, const void *arg){
		    // cast the void pointer to what we expect to find
		const leastsq_param_t *params = (const leastsq_param_t *)arg;

		float Vmodel [5];

		float Vcell[5]={3.7,3.72,4.0,4.5,5.0};

		for (int i = 0; i < n; i++) {
		float Vmodel[5] = {params->a * point->x[i] +params->b * params->c +params->d - params->e, params->a * point->x[i] +params->b * params->c +params->d + params->e, params->a * point->x[i] +params->b * params->c -params->d - params->e,params->a * point->x[i] -params->b * params->c +params->d - params->e,params->a * point->x[i] +params->b * params->c -params->d - params->e};
		}

		// cost function computation for arguments of exp
		int m=5;
		  double dif_V = 0;
		  double dif_Vtotal = 0;
		  double square_dif_V=0;
		  for (int i = 0; i < m; i++) {
		    dif_V+= Vcell[i]-Vmodel[i];
		    square_dif_V = SQUARE(dif_V);
		    dif_Vtotal += square_dif_V;
		    point->fx +=square_dif_V;
		  }


		}

		void parameter_estimation_process(int n) {
		  n = 5;
		  float x[5]={-2.10, -3.04, 4.50, 5.90,6.78};
		  point_t start; // initial point
		  start.x = malloc(n * sizeof(double));
		  for (int i = 0; i < n; i++) {
		    start.x[i] = x[i];
		  }

		  // optimisation settings
		  optimset_t optimset;
		  optimset.tolx = 0.001;    // tolerance on the simplex solutions coordinates
		  optimset.tolf = 0.001;    // tolerance on the function value
		  optimset.max_iter = 1000; // maximum number of allowed iterations
		  optimset.max_eval = 1000; // maximum number of allowed function evaluations
		  optimset.verbose = 1;     // toggle verbose output during minimization

		  // cost function parameters
		  leastsq_param_t leastsq_params; // parameters of our cost function
		  leastsq_params.a = 20.0;
		  leastsq_params.b = 0.2;
		  leastsq_params.c = 2.0*PI;
		  leastsq_params.d = 1.0;
		  leastsq_params.e = 2.5;

		  // call optimization methods
		  point_t solution;    // container for the solution of the minimisation
		  nelder_mead(n, &start, &solution, &leastsq_fun, &leastsq_params, &optimset);

		  // evaluate and print starting point
		  //printf("Initial point\n");
		  //leastsq_params.c = 2.0 * PI;
		  //leastsq_fun(n, &start, &leastsq_params.c = 2.0 * PI;leastsq_params);
		  //printf("x = [ ");
		  //for (int i = 0; i < n; i++) {
		    //printf("%.8f ", start.x[i]);
		  //}
		  //printf("], fx = %.8f \n", start.fx);
		  // print solution
		  //printf("Solution\n");
		  //printf("x = [ ");
		  //for (int i = 0; i < n; i++) {
		    //printf("%.8f ", solution.x[i]);
		  //}
		  //printf("], fx = %.8f \n", solution.fx);

		  // free memory
		  free(start.x);
		  free(solution.x);

		  //return 0;
		}

	/*
#ifdef DEBUG

	if (I_BAT_COUNT >= 719) {
		I_BAT_COUNT = 0;
		uint8_t update_Buffer[] = "END \r\n";
		CDC_Transmit_FS(update_Buffer, sizeof(update_Buffer));
	} else
		I_BAT_COUNT++;

#endif

*/


}

/**
 * Function to perform jump to system memory boot from user application
 *
 * Call function when you want to jump to system memory
 */
void JumpToBootloader(void) {
	void (*SysMemBootJump)(void);

	/* Step: Set system memory address */
	volatile uint32_t addr = 0x1FFF0000;

	/**
	 * Step: Disable RCC, set it to default (after reset) settings
	 *       Internal clock, no PLL, etc.
	 */
	HAL_RCC_DeInit();

	/**
	 * Step: Disable systick timer and reset it to default values
	 */
	SysTick->CTRL = 0;
	SysTick->LOAD = 0;
	SysTick->VAL = 0;

	/**
	 * Step: Disable all interrupts
	 */
	__disable_irq();

	__DSB();

	/* Step: Remap system memory to address 0x0000 0000 in address space */

	__HAL_SYSCFG_REMAPMEMORY_SYSTEMFLASH();

	__DSB();
	__ISB();

	/**
	 * Step: Set jump memory location for system memory
	 *       Use address with 4 bytes offset which specifies jump location where program starts
	 */
	SysMemBootJump = (void (*)(void)) (*((uint32_t*) (addr + 4)));

	/**
	 * Step: Set main stack pointer.
	 *       This step must be done last otherwise local variables in this function
	 *       don't have proper value since stack pointer is located on different position
	 *
	 *       Set direct address location which specifies stack pointer in SRAM location
	 */
	__set_MSP(*(uint32_t*) addr);

	/**
	 * Step: Actually call our function to jump to set location
	 *       This will start system memory execution
	 */
	SysMemBootJump();

}

/* Fault Handling Function
 *
 * ********************************************************
 * Fault Number	|	Fault Type			|	LED Pulses
 * 0 			: System Shutdown 		: 	  OFF
 * 1 			: Over-Current	    	:    1 Pulse
 * 2 			: I2C Communication		: 	 2 Pulse
 * 3 			: Under/Over Voltage	:    3 Pulse
 * ********************************************************
 */

void FaultDetected(uint8_t fault) {

	HAL_GPIO_WritePin(FET_1_EN_GPIO_Port, FET_1_EN_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(FET_2_EN_GPIO_Port, FET_2_EN_Pin, GPIO_PIN_RESET);

	__HAL_TIM_SET_COUNTER(&htim17, 0);

	if (fault == 0) {
		HAL_NVIC_SystemReset();
	} else if (fault == 1) {
		LED_Pulses = 1;

	}

}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
