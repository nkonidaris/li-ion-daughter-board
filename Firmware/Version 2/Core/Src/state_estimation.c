/*
 * state_estimation.c
 *
 *  Created on: Aug 27, 2020
 *      Author: Nicholas Konidaris
 */

#include "state_estimation.h"

/* Parameter Estimation Algorithm */

float get_SOC(float Current, float Voltage, float Temperature) {
	static float z_previous = 0;
	static float Q_EOC_k = Q_MAX_0;
	float z; //Q_1(t)

	z = (Current * Ts / 3600) + z_previous;
	z_previous = z;

	if (Voltage >= V_EOC)
		Q_EOC_k = z;

	return (z - (Q_EOC_k - Q_MAX_0)) / Q_MAX_0;
}

float get_SOE(float SOC) {

	return 0;

}

float get_OCP(float x, float T){

	float x_2 = x*x;
	float x_3 = x_2*x;
	float x_4 = x_3*x;
	float x_6 = x_3*x_3;
	float x_8 = x_4*x_4;
	float x_10 = x_6*x_4;

	float dudt   = -0.001f * (0.199521039f -0.928373822f*x + 1.364550689000003f*x_2-0.6115448939999998f*x_3);
	dudt   /= (1-5.661479886999997f*x +11.47636191f*x_2-9.82431213599998f*x_3+3.048755063f*x_4);
	float OCP    = (-4.656f+88.669f*x_2 - 401.119f*x_4 + 342.909f*x_6 - 462.471f*x_8 + 433.434f*x_10);
	OCP    /= (-1+18.933f*x_2-79.532f*x_4+37.311f*x_6-73.083f*x_8+95.96f*x_10);
	OCP    += (T-1.6f)*dudt;

	return OCP;

}

