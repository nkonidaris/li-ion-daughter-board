/*
 * switching_states.c
 *
 *  Created on: Aug 27, 2020
 *      Author: Nicholas Konidaris
 */

#include "switching_states.h"

/* Module number in sequence
 * - Uncomment for respective module number
 */
#define Module1
//#define Module2
//#define Module3
//#define Module4


/* Modulator Functions */
void decodeState(uint16_t state) {

	/* Interconnect Between Previous and Next Module */
	uint8_t connectionPrev, connectionNext;

#ifdef Module1
	/* Connect Previous is constant */
	connectionPrev = 0x7;
	connectionNext = state >> 13;
#endif

#ifdef Module2
	connectionPrev = state >> 13;
	connectionNext = state >> 10 & 0x7;
#endif

#ifdef Module3
	connectionPrev = state >> 10 & 0x7;
	connectionNext = state >> 5 & 0x7;
#endif

#ifdef Module4
	/* Connect Next is constant */
	connectionPrev = state >> 5 & 0x7;
	connectionNext = 0x7;
#endif

	switch (connectionPrev) {
	case state_series_positive:
		HAL_GPIO_WritePin(O_FET_Previous_Inner_GPIO_Port,
		O_FET_Previous_Inner_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(O_FET_Previous_Outer_GPIO_Port,
		O_FET_Previous_Outer_Pin, GPIO_PIN_RESET);
		break;
	case state_series_negative:
		HAL_GPIO_WritePin(O_FET_Previous_Inner_GPIO_Port,
		O_FET_Previous_Inner_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(O_FET_Previous_Outer_GPIO_Port,
		O_FET_Previous_Outer_Pin, GPIO_PIN_SET);
		break;
	case state_parallel_positive:
		HAL_GPIO_WritePin(O_FET_Previous_Inner_GPIO_Port,
		O_FET_Previous_Inner_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(O_FET_Previous_Outer_GPIO_Port,
		O_FET_Previous_Outer_Pin, GPIO_PIN_RESET);
		break;
	case state_parallel_negative:
		HAL_GPIO_WritePin(O_FET_Previous_Inner_GPIO_Port,
		O_FET_Previous_Inner_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(O_FET_Previous_Outer_GPIO_Port,
		O_FET_Previous_Outer_Pin, GPIO_PIN_SET);
		break;
	case state_bypass_positive:
		HAL_GPIO_WritePin(O_FET_Previous_Inner_GPIO_Port,
		O_FET_Previous_Inner_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(O_FET_Previous_Outer_GPIO_Port,
		O_FET_Previous_Outer_Pin, GPIO_PIN_SET);
		break;
	case state_bypass_negative:
		HAL_GPIO_WritePin(O_FET_Previous_Inner_GPIO_Port,
		O_FET_Previous_Inner_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(O_FET_Previous_Outer_GPIO_Port,
		O_FET_Previous_Outer_Pin, GPIO_PIN_RESET);
		break;
	case state_zero:
		break;
	default:
		break;
	}

	if (connectionPrev == state_zero)
		HAL_GPIO_WritePin(O_FET_Previous_EN_GPIO_Port, O_FET_Previous_EN_Pin,
				GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(O_FET_Previous_EN_GPIO_Port, O_FET_Previous_EN_Pin,
				GPIO_PIN_SET);

	switch (connectionNext) {
	case state_series_positive:
		HAL_GPIO_WritePin(O_FET_Next_Inner_GPIO_Port, O_FET_Next_Inner_Pin,
				GPIO_PIN_SET);
		HAL_GPIO_WritePin(O_FET_Next_Outer_GPIO_Port, O_FET_Next_Outer_Pin,
				GPIO_PIN_SET);
		break;
	case state_series_negative:
		HAL_GPIO_WritePin(O_FET_Next_Inner_GPIO_Port, O_FET_Next_Inner_Pin,
				GPIO_PIN_RESET);
		HAL_GPIO_WritePin(O_FET_Next_Outer_GPIO_Port, O_FET_Next_Outer_Pin,
				GPIO_PIN_RESET);
		break;
	case state_parallel_positive:
		HAL_GPIO_WritePin(O_FET_Next_Inner_GPIO_Port, O_FET_Next_Inner_Pin,
				GPIO_PIN_SET);
		HAL_GPIO_WritePin(O_FET_Next_Outer_GPIO_Port, O_FET_Next_Outer_Pin,
				GPIO_PIN_RESET);
		break;
	case state_parallel_negative:
		HAL_GPIO_WritePin(O_FET_Next_Inner_GPIO_Port, O_FET_Next_Inner_Pin,
				GPIO_PIN_RESET);
		HAL_GPIO_WritePin(O_FET_Next_Outer_GPIO_Port, O_FET_Next_Outer_Pin,
				GPIO_PIN_SET);
		break;
	case state_bypass_positive:
		HAL_GPIO_WritePin(O_FET_Next_Inner_GPIO_Port, O_FET_Next_Inner_Pin,
				GPIO_PIN_SET);
		HAL_GPIO_WritePin(O_FET_Next_Outer_GPIO_Port, O_FET_Next_Outer_Pin,
				GPIO_PIN_SET);
		break;
	case state_bypass_negative:
		HAL_GPIO_WritePin(O_FET_Next_Inner_GPIO_Port, O_FET_Next_Inner_Pin,
				GPIO_PIN_RESET);
		HAL_GPIO_WritePin(O_FET_Next_Outer_GPIO_Port, O_FET_Next_Outer_Pin,
				GPIO_PIN_RESET);
		break;
	case state_zero:
		break;
	default:
		break;
	}

	if (connectionNext == state_zero)
		HAL_GPIO_WritePin(O_FET_Next_EN_GPIO_Port, O_FET_Next_EN_Pin,
				GPIO_PIN_RESET);
	else
		HAL_GPIO_WritePin(O_FET_Next_EN_GPIO_Port, O_FET_Next_EN_Pin,
				GPIO_PIN_SET);

}

void startModulation() {
#ifdef Module1
	HAL_GPIO_WritePin(O_FET_Previous_Inner_GPIO_Port, O_FET_Previous_Inner_Pin,
			GPIO_PIN_SET);
	HAL_GPIO_WritePin(O_FET_Previous_Outer_GPIO_Port, O_FET_Previous_Outer_Pin,
			GPIO_PIN_SET);
#endif

#ifdef Module4
	HAL_GPIO_WritePin(O_FET_Next_Inner_GPIO_Port, O_FET_Next_Inner_Pin, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(O_FET_Next_Outer_GPIO_Port, O_FET_Next_Outer_Pin, GPIO_PIN_RESET);
#endif

}


