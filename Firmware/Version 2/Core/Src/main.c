/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under Ultimate Liberty license
 * SLA0044, the "License"; You may not use this file except in compliance with
 * the License. You may obtain a copy of the License at:
 *                             www.st.com/SLA0044
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdbool.h>
#include "switching_states.h"
#include "state_estimation.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */


/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */


/* ADC Raw Data Samples
 * -Array Structure:
 *  -Element 0: Temperature
 * 		     1: Battery Voltage
 * 		     2: Battery Current
 */
uint16_t ADC_Samples[3];
float Vref = 3.3f;

/* Current Measurements */
float Battery_Current;

/* Voltage Measurements */
float Battery_Voltage;

/* Temperature Measurements */
float Battery_Temperature;

/* I2C Switching State */
uint8_t state[2];

/* Parameter Estimation Variables */
bool Trigger_Estimation_Start = false;
float SOC = 0.0f;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* ADC Get Functions */
float get_Battery_Current();
float get_Battery_Voltage();
float get_Battery_Temperature();


/* Utility Functions */
void PrintUART(uint8_t *data);

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_ADC1_Init();
  MX_I2C2_Init();
  MX_USART2_UART_Init();
  MX_TIM1_Init();
  MX_TIM16_Init();
  /* USER CODE BEGIN 2 */

	/* ADC Automatic Calibration */
	//HAL_ADCEx_Calibration_Start(&hadc1);
	//uint32_t VREFINT_CAL = HAL_ADCEx_Calibration_GetValue(&hadc1);

	/* Start DMA ADC Sequence */
	//HAL_ADC_Start_DMA(&hadc1, (uint32_t*) ADC_Samples, 3);

	/* Start I2C Listen Mode */
	//HAL_I2C_EnableListen_IT(&hi2c2);

	HAL_I2C_Slave_Receive_IT(&hi2c2, state, 2);
	//startModulation();

	HAL_TIM_Base_Start_IT(&htim16);
	HAL_TIM_PWM_Start_IT(&htim1,TIM_CHANNEL_1);

	HAL_GPIO_WritePin(O_FET_Previous_EN_GPIO_Port, O_FET_Previous_EN_Pin,
			GPIO_PIN_SET);
	//HAL_GPIO_WritePin(O_FET_Next_EN_GPIO_Port, O_FET_Next_EN_Pin,
	//			GPIO_PIN_RESET);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1) {

		if (Trigger_Estimation_Start) {
			Trigger_Estimation_Start = false;
			SOC = get_SOC(get_Battery_Current(), get_Battery_Voltage(),
					get_Battery_Temperature());

		} else {

			/* Testing Code */
			HAL_Delay(1000);
			Battery_Current = get_Battery_Current();
			Battery_Voltage = get_Battery_Voltage();
			Battery_Temperature = get_Battery_Temperature();

			//char data[10] = "";
			//sprintf(data, "%d\n", (int) (Battery_Temperature * 1000));
			//PrintUART((uint8_t*) data);

		}
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);
  while(LL_FLASH_GetLatency() != LL_FLASH_LATENCY_2)
  {
  }

  /* HSI configuration and activation */
  LL_RCC_HSI_Enable();
  while(LL_RCC_HSI_IsReady() != 1)
  {
  }

  /* Main PLL configuration and activation */
  LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSI, LL_RCC_PLLM_DIV_1, 8, LL_RCC_PLLR_DIV_2);
  LL_RCC_PLL_Enable();
  LL_RCC_PLL_EnableDomain_SYS();
  while(LL_RCC_PLL_IsReady() != 1)
  {
  }

  /* Set AHB prescaler*/
  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);

  /* Sysclk activation on the main PLL */
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
  while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL)
  {
  }

  /* Set APB1 prescaler*/
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  /* Update CMSIS variable (which can be updated also through SystemCoreClockUpdate function) */
  LL_SetSystemCoreClock(64000000);

   /* Update the time base */
  if (HAL_InitTick (TICK_INT_PRIORITY) != HAL_OK)
  {
    Error_Handler();
  }
  LL_RCC_SetADCClockSource(LL_RCC_ADC_CLKSOURCE_HSI);
}

/* USER CODE BEGIN 4 */

/* Callback Routines */

void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc) {

	//HAL_GPIO_TogglePin(O_TP6_GPIO_Port, O_TP6_Pin);

}

void HAL_ADC_LevelOutOfWindowCallback(ADC_HandleTypeDef *hadc) {

	//HAL_GPIO_TogglePin(LED_Status_GPIO_Port, LED_Status_Pin);

}

void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c) {

	if (state[0] == 'H' && state[1] == '1'){
		HAL_GPIO_TogglePin(LED_Status_GPIO_Port, LED_Status_Pin);
		state[0] = ' ';
		state[1] = ' ';
	}
	//uint16_t fullstate = state[1] | (state[0] << 8);
	//decodeState(fullstate);

	//HAL_I2C_Slave_Receive_IT(&hi2c2, state, 2);
	HAL_I2C_Slave_Receive_IT(&hi2c2, state, 2);

}

void HAL_I2C_ListenCpltCallback(I2C_HandleTypeDef *hi2c) {

	//HAL_GPIO_TogglePin(LED_Status_GPIO_Port, LED_Status_Pin);

}

void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c) {

	HAL_NVIC_SystemReset();

}

void HAL_TIM_PWM_PulseFinishedCallback (TIM_HandleTypeDef * htim){
	static bool sw = false;

	if (htim->Instance == TIM1) {

		if(sw){
			sw = false;
			HAL_GPIO_WritePin(O_FET_Previous_Inner_GPIO_Port,
			O_FET_Previous_Inner_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(O_FET_Previous_Outer_GPIO_Port,
			O_FET_Previous_Outer_Pin, GPIO_PIN_SET);
//			HAL_GPIO_WritePin(O_FET_Next_Inner_GPIO_Port, O_FET_Next_Inner_Pin,
//					GPIO_PIN_RESET);
//			HAL_GPIO_WritePin(O_FET_Next_Outer_GPIO_Port, O_FET_Next_Outer_Pin,
//					GPIO_PIN_SET);
		}else{
			sw = true;
			HAL_GPIO_WritePin(O_FET_Previous_Inner_GPIO_Port,
			O_FET_Previous_Inner_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(O_FET_Previous_Outer_GPIO_Port,
			O_FET_Previous_Outer_Pin, GPIO_PIN_RESET);
//			HAL_GPIO_WritePin(O_FET_Next_Inner_GPIO_Port, O_FET_Next_Inner_Pin,
//					GPIO_PIN_SET);
//			HAL_GPIO_WritePin(O_FET_Next_Outer_GPIO_Port, O_FET_Next_Outer_Pin,
//					GPIO_PIN_RESET);
		}


	}

}

/* Convert and Get information from ADC */

float get_Battery_Current() {

	float Current_Conv = ADC_Samples[2] / 4095 * Vref;
	return (Current_Conv - 1.5f) / 0.2f * 1000;

}

float get_Battery_Voltage() {

	return ADC_Samples[1] / 4095 * Vref * 2;

}

float get_Battery_Temperature() {

	return ADC_Samples[0] / 4095 * Vref;

}

/* Printf overwrite for UART2 */

void PrintUART(uint8_t *data) {

	while (*data != '\n') {
		//HAL_UART_Transmit_IT(&huart2, data++, 1);
	}
	//HAL_UART_Transmit_IT(&huart2, data, 1);

}

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM17 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM17) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

	if (htim->Instance == TIM16) {
		//Trigger_Estimation_Start = true;
	}

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
