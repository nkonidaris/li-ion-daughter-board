/*
 * switching_states.h
 *
 *  Created on: 11 Aug 2020
 *      Author: Nicholas Konidaris
 */

#ifndef INC_SWITCHING_STATES_H_
#define INC_SWITCHING_STATES_H_

#include <stdint.h>
#include "gpio.h"

/* Possible Interconnection States
 *
 *	I2C Data Packet:
 *
 *	|_|_|_|_|_|_|_|_|
 *	 0 1 2 3 4 5 6 7
 *
 *	0-2 = Interconnect 1
 *	3-5 = Interconnect 2
 *	6   = N/A
 *	7	= Reset (0 = Rest, 1 = No Reset ?)
 *
 * */


#define state_series_positive 		0x0
#define state_series_negative 		0x1
#define state_bypass_positive  		0x2
#define state_bypass_negative  		0x3
#define state_zero		  			0x4
#define state_parallel_positive 	0x5
#define state_parallel_negative 	0x6

/* Modulation Functions */
void startModulation();
void decodeState(uint16_t state);




#endif /* INC_SWITCHING_STATES_H_ */
