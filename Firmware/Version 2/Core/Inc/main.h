/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"
#include "stm32g0xx_ll_dma.h"
#include "stm32g0xx_ll_rcc.h"
#include "stm32g0xx_ll_bus.h"
#include "stm32g0xx_ll_system.h"
#include "stm32g0xx_ll_exti.h"
#include "stm32g0xx_ll_cortex.h"
#include "stm32g0xx_ll_utils.h"
#include "stm32g0xx_ll_pwr.h"
#include "stm32g0xx_ll_usart.h"
#include "stm32g0xx_ll_gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define ADC_VCurrent_Pin GPIO_PIN_7
#define ADC_VCurrent_GPIO_Port GPIOB
#define O_FET_Next_Inner_Pin GPIO_PIN_9
#define O_FET_Next_Inner_GPIO_Port GPIOB
#define O_TP6_Pin GPIO_PIN_15
#define O_TP6_GPIO_Port GPIOC
#define ADC_VTemp_Pin GPIO_PIN_0
#define ADC_VTemp_GPIO_Port GPIOA
#define ADC_VBat_Pin GPIO_PIN_1
#define ADC_VBat_GPIO_Port GPIOA
#define O_FET_Previous_Outer_Pin GPIO_PIN_4
#define O_FET_Previous_Outer_GPIO_Port GPIOA
#define LED_Status_Pin GPIO_PIN_5
#define LED_Status_GPIO_Port GPIOA
#define O_FET_Previous_Inner_Pin GPIO_PIN_6
#define O_FET_Previous_Inner_GPIO_Port GPIOA
#define O_FET_Previous_EN_Pin GPIO_PIN_7
#define O_FET_Previous_EN_GPIO_Port GPIOA
#define O_FET_Next_EN_Pin GPIO_PIN_8
#define O_FET_Next_EN_GPIO_Port GPIOA
#define O_FET_Next_Outer_Pin GPIO_PIN_3
#define O_FET_Next_Outer_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
