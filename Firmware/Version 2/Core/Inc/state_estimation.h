/*
 * state_estimation.h
 *
 *  Created on: Aug 27, 2020
 *      Author: Nicholas Konidaris
 */

#ifndef INC_STATE_ESTIMATION_H_
#define INC_STATE_ESTIMATION_H_

/* State Estimation Algorithm:
 *
 * Based off paper by Yang Et al.
 * DOI: 10.1016/j.apenergy.2019.04.143
 * Development of a degradation-conscious physics-based lithium-ion
 * battery model for use in power system planning studies
 *
 */

/* State-of-Charge constants */
#define Ts 60 // Time-Step
#define Q_MAX_0 2.6f // Charge capacity at BOL (Ah)
#define V_EOC 4.2f // Pre-specified maximum voltage level

float get_SOC(float Current, float Voltage, float Temperature);
float get_SOE(float SOC);
float get_OCP(float x, float T);



#endif /* INC_STATE_ESTIMATION_H_ */
