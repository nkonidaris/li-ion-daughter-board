EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title "External PCB Connectors"
Date "2020-09-07"
Rev "3"
Comp "Nicholas Konidaris"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1250 1675 2    50   BiDi ~ 0
I2C_Clock_Ext
Text GLabel 1250 1575 2    50   BiDi ~ 0
I2C_Data_Ext
Text GLabel 1250 1375 2    50   BiDi ~ 0
Batt_Next_Outer
Text GLabel 1250 1475 2    50   BiDi ~ 0
Batt_Next_Inner
Text GLabel 3750 2650 0    50   BiDi ~ 0
SYS_SWCLK
Text GLabel 3750 2750 0    50   BiDi ~ 0
SYS_SWDIO
Text GLabel 4700 2600 0    50   Output ~ 0
VTemp
Text GLabel 3700 1200 2    50   Input ~ 0
BATT_IN
Text GLabel 1925 2275 0    50   BiDi ~ 0
I2C_Clock_Ext
Text GLabel 1925 2375 0    50   BiDi ~ 0
I2C_Data_Ext
Text GLabel 1925 2575 0    50   BiDi ~ 0
Batt_Prev_Outer
Text GLabel 1925 2475 0    50   BiDi ~ 0
Batt_Prev_Inner
$Comp
L Li-ion_Thesis_Symbol_Library:Main_Connector_Out J?
U 1 1 5F11E44F
P 1050 1375
AR Path="/5F11E44F" Ref="J?"  Part="1" 
AR Path="/5F11B4FC/5F11E44F" Ref="J1"  Part="1" 
F 0 "J1" H 1022 1257 50  0000 R CNN
F 1 "Main Connector Out" H 1800 1775 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Horizontal" H 1050 1375 50  0001 C CNN
F 3 "~" H 1050 1375 50  0001 C CNN
F 4 "S1111EC-06-ND" H -6600 -4575 50  0001 C CNN "Digi-Key Part Number"
F 5 "5055680671" H -6600 -4575 50  0001 C CNN "Manufacturer Part Number"
F 6 "1.151" H -6600 -4575 50  0001 C CNN "Cost (A$)"
	1    1050 1375
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:Temperture_Sensor_Header J?
U 1 1 5F11E458
P 4900 2600
AR Path="/5F11E458" Ref="J?"  Part="1" 
AR Path="/5F11B4FC/5F11E458" Ref="J5"  Part="1" 
F 0 "J5" H 4872 2532 50  0000 R CNN
F 1 "Temperature Sensor" H 5400 2125 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 4900 2600 50  0001 C CNN
F 3 "~" H 4900 2600 50  0001 C CNN
F 4 "WM7880CT-ND" H -5500 -3400 50  0001 C CNN "Digi-Key Part Number"
F 5 "501331-0307" H -5500 -3400 50  0001 C CNN "Manufacturer Part Number"
F 6 "0.99" H -5500 -3400 50  0001 C CNN "Cost (A$)"
	1    4900 2600
	-1   0    0    1   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:Programming_Header J?
U 1 1 5F11E461
P 3950 2750
AR Path="/5F11E461" Ref="J?"  Part="1" 
AR Path="/5F11B4FC/5F11E461" Ref="J3"  Part="1" 
F 0 "J3" H 3922 2632 50  0000 R CNN
F 1 "Programming Header" H 4750 2200 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 3950 2750 50  0001 C CNN
F 3 "~" H 3950 2750 50  0001 C CNN
F 4 "WM7881CT-ND" H -6500 -2150 50  0001 C CNN "Digi-Key Part Number"
F 5 "5013310407" H -6500 -2150 50  0001 C CNN "Manufacturer Part Number"
F 6 "0.893" H -6500 -2150 50  0001 C CNN "Cost (A$)"
	1    3950 2750
	-1   0    0    1   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:Main_Connector_In J?
U 1 1 5F11E46A
P 2125 2575
AR Path="/5F11E46A" Ref="J?"  Part="1" 
AR Path="/5F11B4FC/5F11E46A" Ref="J2"  Part="1" 
F 0 "J2" H 2097 2457 50  0000 R CNN
F 1 "Main Connector In" H 2825 2075 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Horizontal" H 2125 2575 50  0001 C CNN
F 3 "~" H 2125 2575 50  0001 C CNN
F 4 "S5481-ND" H -7325 -3475 50  0001 C CNN "Digi-Key Part Number"
F 5 "5055680671" H -7325 -3475 50  0001 C CNN "Manufacturer Part Number"
F 6 "1.151" H -7325 -3475 50  0001 C CNN "Cost (A$)"
	1    2125 2575
	-1   0    0    1   
$EndComp
Wire Notes Line
	825  775  5375 775 
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F11E474
P 3750 2450
AR Path="/5F11E474" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E474" Ref="#PWR051"  Part="1" 
F 0 "#PWR051" H 3750 2200 50  0001 C CNN
F 1 "GND_Internal" V 3755 2323 50  0000 R CNN
F 2 "" H 3750 2450 50  0001 C CNN
F 3 "" H 3750 2450 50  0001 C CNN
	1    3750 2450
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F11E47A
P 3750 2850
AR Path="/5F11E47A" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E47A" Ref="#PWR052"  Part="1" 
F 0 "#PWR052" H 3750 2700 50  0001 C CNN
F 1 "+3.3V_Internal" V 3765 2978 50  0000 L CNN
F 2 "" H 3750 2850 50  0001 C CNN
F 3 "" H 3750 2850 50  0001 C CNN
	1    3750 2850
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F11E480
P 1250 1275
AR Path="/5F11E480" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E480" Ref="#PWR047"  Part="1" 
F 0 "#PWR047" H 1250 1025 50  0001 C CNN
F 1 "GND_External" V 1250 1125 50  0000 R CNN
F 2 "" H 1250 1275 50  0001 C CNN
F 3 "" H 1250 1275 50  0001 C CNN
	1    1250 1275
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F11E486
P 1925 2675
AR Path="/5F11E486" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E486" Ref="#PWR048"  Part="1" 
F 0 "#PWR048" H 1925 2425 50  0001 C CNN
F 1 "GND_External" V 1925 2525 50  0000 R CNN
F 2 "" H 1925 2675 50  0001 C CNN
F 3 "" H 1925 2675 50  0001 C CNN
	1    1925 2675
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F11E48C
P 4700 2700
AR Path="/5F11E48C" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E48C" Ref="#PWR056"  Part="1" 
F 0 "#PWR056" H 4700 2450 50  0001 C CNN
F 1 "GND_Internal" H 4705 2527 50  0000 C CNN
F 2 "" H 4700 2700 50  0001 C CNN
F 3 "" H 4700 2700 50  0001 C CNN
	1    4700 2700
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F11E492
P 4700 2500
AR Path="/5F11E492" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E492" Ref="#PWR055"  Part="1" 
F 0 "#PWR055" H 4700 2350 50  0001 C CNN
F 1 "+3.3V_Internal" H 4715 2673 50  0000 C CNN
F 2 "" H 4700 2500 50  0001 C CNN
F 3 "" H 4700 2500 50  0001 C CNN
	1    4700 2500
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F11E498
P 3350 1650
AR Path="/5F11E498" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E498" Ref="#PWR050"  Part="1" 
F 0 "#PWR050" H 3350 1400 50  0001 C CNN
F 1 "GND_Internal" H 3600 1500 50  0000 R CNN
F 2 "" H 3350 1650 50  0001 C CNN
F 3 "" H 3350 1650 50  0001 C CNN
	1    3350 1650
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F11E49E
P 1925 2775
AR Path="/5F11E49E" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E49E" Ref="#PWR049"  Part="1" 
F 0 "#PWR049" H 1925 2625 50  0001 C CNN
F 1 "+5V_External" V 1925 3175 50  0000 C CNN
F 2 "" H 1925 2775 50  0001 C CNN
F 3 "" H 1925 2775 50  0001 C CNN
	1    1925 2775
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F11E4A4
P 1250 1175
AR Path="/5F11E4A4" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E4A4" Ref="#PWR046"  Part="1" 
F 0 "#PWR046" H 1250 1025 50  0001 C CNN
F 1 "+5V_External" V 1250 1525 50  0000 C CNN
F 2 "" H 1250 1175 50  0001 C CNN
F 3 "" H 1250 1175 50  0001 C CNN
	1    1250 1175
	0    1    1    0   
$EndComp
Text Notes 2725 975  0    98   ~ 0
Internal
Text Notes 1950 950  0    98   ~ 0
External
$Comp
L Device:Battery_Cell BT?
U 1 1 5F11E4AD
P 3350 1550
AR Path="/5F11E4AD" Ref="BT?"  Part="1" 
AR Path="/5F11B4FC/5F11E4AD" Ref="BT1"  Part="1" 
F 0 "BT1" H 3468 1646 50  0000 L CNN
F 1 "Battery_Cell" H 3468 1555 50  0000 L CNN
F 2 "Li-ion_Thesis_Footprint_Library:BatteryHolder_Keystone_1042_1x18650" V 3350 1610 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p27.pdf" V 3350 1610 50  0001 C CNN
F 4 "36-1042-ND" H 3350 1550 50  0001 C CNN "Digi-Key Part Number"
	1    3350 1550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 1350 3350 1200
Wire Wire Line
	3350 1200 3400 1200
$Comp
L Device:Fuse F?
U 1 1 5F11E4B5
P 3550 1200
AR Path="/5F11E4B5" Ref="F?"  Part="1" 
AR Path="/5F11B4FC/5F11E4B5" Ref="F1"  Part="1" 
F 0 "F1" V 3353 1200 50  0000 C CNN
F 1 "Fuse 3.0A" V 3444 1200 50  0000 C CNN
F 2 "Fuse:Fuse_1206_3216Metric" V 3480 1200 50  0001 C CNN
F 3 "~" H 3550 1200 50  0001 C CNN
F 4 "507-1883-1-ND" H 3550 1200 50  0001 C CNN "Digi-Key Part Number"
F 5 "C1F 3" H 3550 1200 50  0001 C CNN "Manufacturer Part Number"
	1    3550 1200
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Male J?
U 1 1 5F11E4BB
P 4525 1300
AR Path="/5F11E4BB" Ref="J?"  Part="1" 
AR Path="/5F11B4FC/5F11E4BB" Ref="J4"  Part="1" 
F 0 "J4" H 4450 1475 50  0000 C CNN
F 1 "Conn_01x04_Male" H 4650 1575 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4525 1300 50  0001 C CNN
F 3 "~" H 4525 1300 50  0001 C CNN
F 4 " ‎PH1-12-UA‎" H 4525 1300 50  0001 C CNN "Digi-Key Part Number"
	1    4525 1300
	1    0    0    -1  
$EndComp
Text GLabel 4725 1200 2    50   BiDi ~ 0
UARTRX
Text GLabel 4725 1300 2    50   BiDi ~ 0
UARTTX
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F11E4C3
P 4725 1500
AR Path="/5F11E4C3" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E4C3" Ref="#PWR054"  Part="1" 
F 0 "#PWR054" H 4725 1350 50  0001 C CNN
F 1 "+3.3V_Internal" V 4800 1775 50  0000 C CNN
F 2 "" H 4725 1500 50  0001 C CNN
F 3 "" H 4725 1500 50  0001 C CNN
	1    4725 1500
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F11E4C9
P 4725 1400
AR Path="/5F11E4C9" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E4C9" Ref="#PWR053"  Part="1" 
F 0 "#PWR053" H 4725 1150 50  0001 C CNN
F 1 "GND_Internal" V 4675 1075 50  0000 C CNN
F 2 "" H 4725 1400 50  0001 C CNN
F 3 "" H 4725 1400 50  0001 C CNN
	1    4725 1400
	0    -1   -1   0   
$EndComp
Text GLabel 3750 2550 0    50   BiDi ~ 0
SYS_SWO
Wire Notes Line
	825  775  825  3225
Wire Notes Line
	2625 775  2625 3225
Wire Notes Line
	5375 775  5375 3225
Wire Notes Line
	825  3225 5375 3225
$EndSCHEMATC
