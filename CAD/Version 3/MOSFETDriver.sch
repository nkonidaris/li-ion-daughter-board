EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title "MOSFET Driver"
Date "2020-09-07"
Rev "3"
Comp "Nicholas Konidaris"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 2475 1275 0    50   Output ~ 0
FET_1_EN
Text GLabel 2475 1675 0    50   Output ~ 0
FET_1_APWM
Text GLabel 2475 1375 0    50   Output ~ 0
FET_1_BPWM
Text GLabel 7675 1275 0    50   Output ~ 0
FET_2_EN
Text GLabel 7675 1375 0    50   Output ~ 0
FET_2_BPWM
Text GLabel 7675 1675 0    50   Output ~ 0
FET_2_APWM
Text GLabel 3675 1175 2    50   Output ~ 0
BHO_1
Text GLabel 1650 3125 0    50   Input ~ 0
BHO_1
Text GLabel 3675 1375 2    50   Output ~ 0
BLO_1
Text GLabel 3675 1675 2    50   Output ~ 0
ALO_1
Text GLabel 3675 1875 2    50   Output ~ 0
AHO_1
Text GLabel 8875 1175 2    50   Output ~ 0
BHO_2
Text GLabel 8875 1375 2    50   Output ~ 0
BLO_2
Text GLabel 8875 1675 2    50   Output ~ 0
ALO_2
Text GLabel 8875 1875 2    50   Output ~ 0
AHO_2
Text GLabel 7675 1875 0    50   Output ~ 0
AHB_2
Text GLabel 7675 1175 0    50   Output ~ 0
BHB_2
Text GLabel 2475 1875 0    50   BiDi ~ 0
AHB_1
Text GLabel 2475 1175 0    50   BiDi ~ 0
BHB_1
Text GLabel 1650 3225 0    50   Input ~ 0
BLO_1
Text GLabel 1650 3325 0    50   Input ~ 0
ALO_1
Text GLabel 1650 3425 0    50   Input ~ 0
AHO_1
Text GLabel 6750 3125 0    50   Input ~ 0
BHO_2
Text GLabel 6750 3225 0    50   Input ~ 0
BLO_2
Text GLabel 6750 3325 0    50   Input ~ 0
ALO_2
Text GLabel 6750 3425 0    50   Input ~ 0
AHO_2
Wire Wire Line
	2850 3575 2800 3575
Wire Wire Line
	2800 3575 2800 3625
Wire Wire Line
	2800 3675 2850 3675
Wire Wire Line
	2800 3625 2750 3625
Connection ~ 2800 3625
Wire Wire Line
	2800 3625 2800 3675
Wire Wire Line
	4250 3375 4300 3375
Wire Wire Line
	4300 3375 4300 3475
Wire Wire Line
	4300 3725 4250 3725
Wire Wire Line
	4250 3625 4300 3625
Connection ~ 4300 3625
Wire Wire Line
	4300 3625 4300 3725
Wire Wire Line
	4250 3475 4300 3475
Connection ~ 4300 3475
Wire Wire Line
	7950 3575 7900 3575
Wire Wire Line
	7900 3575 7900 3625
Wire Wire Line
	7900 3675 7950 3675
Wire Wire Line
	7900 3625 7850 3625
Connection ~ 7900 3625
Wire Wire Line
	7900 3625 7900 3675
Wire Wire Line
	9350 3375 9400 3375
Wire Wire Line
	9400 3375 9400 3475
Wire Wire Line
	9400 3725 9350 3725
Wire Wire Line
	9350 3625 9400 3625
Connection ~ 9400 3625
Wire Wire Line
	9400 3625 9400 3725
Wire Wire Line
	9350 3475 9400 3475
Connection ~ 9400 3475
Wire Wire Line
	9400 3475 9400 3525
Wire Wire Line
	9450 3525 9400 3525
Connection ~ 9400 3525
Wire Wire Line
	9400 3525 9400 3625
Wire Wire Line
	4300 3475 4300 3525
Wire Wire Line
	4350 3525 4300 3525
Connection ~ 4300 3525
Wire Wire Line
	4300 3525 4300 3625
Text GLabel 9350 3125 2    50   BiDi ~ 0
Batt_Next_Inner
Text GLabel 4250 3225 2    50   BiDi ~ 0
Batt_Prev_Inner
Text GLabel 3675 1775 2    50   BiDi ~ 0
Batt_Prev_Inner
Text GLabel 8875 1275 2    50   BiDi ~ 0
Batt_Next_Inner
Text GLabel 9350 3225 2    50   BiDi ~ 0
Batt_Next_Outer
Text GLabel 8875 1775 2    50   BiDi ~ 0
Batt_Next_Outer
Text GLabel 4250 3125 2    50   BiDi ~ 0
Batt_Prev_Outer
Text GLabel 3675 1275 2    50   BiDi ~ 0
Batt_Prev_Outer
Text GLabel 1775 2675 0    50   BiDi ~ 0
AHB_1
Text GLabel 2075 2675 2    50   BiDi ~ 0
Batt_Prev_Inner
Text GLabel 1775 2300 0    50   BiDi ~ 0
BHB_1
Text GLabel 7150 2675 2    50   BiDi ~ 0
Batt_Next_Outer
Text GLabel 6850 2675 0    50   Output ~ 0
AHB_2
Text GLabel 6850 2325 0    50   Output ~ 0
BHB_2
$Comp
L power:+BATT #PWR?
U 1 1 5F0FE656
P 2750 3625
AR Path="/5F0FE656" Ref="#PWR?"  Part="1" 
AR Path="/5E874FE6/5F0FE656" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F0FE656" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 2750 3475 50  0001 C CNN
F 1 "+BATT" H 2765 3798 50  0000 C CNN
F 2 "" H 2750 3625 50  0001 C CNN
F 3 "" H 2750 3625 50  0001 C CNN
	1    2750 3625
	0    -1   -1   0   
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5F0FE65C
P 7850 3625
AR Path="/5F0FE65C" Ref="#PWR?"  Part="1" 
AR Path="/5E874FE6/5F0FE65C" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F0FE65C" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 7850 3475 50  0001 C CNN
F 1 "+BATT" H 7865 3798 50  0000 C CNN
F 2 "" H 7850 3625 50  0001 C CNN
F 3 "" H 7850 3625 50  0001 C CNN
	1    7850 3625
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:FDMQ86530L U2
U 1 1 5F0FE665
P 8650 3725
F 0 "U2" H 8650 4595 50  0000 C CNN
F 1 "FDMQ86530L" H 8650 4504 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:IC_FDMQ8205A" H 8350 4225 50  0001 L BNN
F 3 "ON Semiconductor" H 8350 4125 50  0001 L BNN
F 4 "FDMQ86530L" H 8650 3725 50  0001 C CNN "Manufacturer Part Number"
F 5 "FDMQ86530LCT-ND" H 8650 3725 50  0001 C CNN "Digi-Key Part Number"
F 6 "3.6576" H 6800 -3625 50  0001 C CNN "Cost (A$)"
	1    8650 3725
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:MIC4606-2YTS-TR IC1
U 1 1 5F0FE670
P 2475 1175
F 0 "IC1" H 3075 1440 50  0000 C CNN
F 1 "MIC4606-2YTS-TR" H 3075 1349 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:MIC4606_TSSOP-16_4.4x5mm_P0.65mm" H 3525 1275 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/mic4606-2yts-tr/microchip-technology" H 3525 1175 50  0001 L CNN
F 4 "MIC4606-2YTS-CT-ND" H 3525 1075 50  0001 L CNN "Digi-Key Part Number"
F 5 "1.2" H 3525 975 50  0001 L CNN "Height"
F 6 "Microchip" H 3525 675 50  0001 L CNN "Manufacturer Name"
F 7 "MIC4606-2YTS-TR" H 3525 575 50  0001 L CNN "Manufacturer Part Number"
F 8 "2.962" H -1475 -3125 50  0001 C CNN "Cost (A$)"
	1    2475 1175
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:FDMQ86530L U1
U 1 1 5F0FE679
P 3550 3725
F 0 "U1" H 3550 4595 50  0000 C CNN
F 1 "FDMQ86530L" H 3550 4504 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:IC_FDMQ8205A" H 3250 4225 50  0001 L BNN
F 3 "ON Semiconductor" H 3250 4125 50  0001 L BNN
F 4 "FDMQ86530L" H 3550 3725 50  0001 C CNN "Manufacturer Part Number"
F 5 "FDMQ86530LCT-ND" H 3550 3725 50  0001 C CNN "Digi-Key Part Number"
F 6 "3.6576" H -1000 -3625 50  0001 C CNN "Cost (A$)"
	1    3550 3725
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5F0FE67F
P 1925 2675
F 0 "C2" V 1775 2325 50  0000 L CNN
F 1 "4.7uF" V 1725 2575 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1963 2525 50  0001 C CNN
F 3 "~" H 1925 2675 50  0001 C CNN
F 4 "1276-1244-1-ND" H 1925 2675 50  0001 C CNN "Digi-Key Part Number"
	1    1925 2675
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 5F0FE685
P 1925 2300
F 0 "C1" V 1775 1950 50  0000 L CNN
F 1 "4.7uF" V 1725 2200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1963 2150 50  0001 C CNN
F 3 "~" H 1925 2300 50  0001 C CNN
F 4 "1276-1244-1-ND" H 1925 2300 50  0001 C CNN "Digi-Key Part Number"
	1    1925 2300
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 5F0FE68B
P 7000 2325
F 0 "C4" V 6850 1975 50  0000 L CNN
F 1 "4.7uF" V 6800 2225 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7038 2175 50  0001 C CNN
F 3 "~" H 7000 2325 50  0001 C CNN
F 4 "1276-1244-1-ND" H 7000 2325 50  0001 C CNN "Digi-Key Part Number"
	1    7000 2325
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:MIC4606-2YTS-TR IC2
U 1 1 5F0FE696
P 7675 1175
F 0 "IC2" H 8275 1440 50  0000 C CNN
F 1 "MIC4606-2YTS-TR" H 8275 1349 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:MIC4606_TSSOP-16_4.4x5mm_P0.65mm" H 8725 1275 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/mic4606-2yts-tr/microchip-technology" H 8725 1175 50  0001 L CNN
F 4 "MIC4606-2YTS-CT-ND" H 8725 1075 50  0001 L CNN "Digi-Key Part Number"
F 5 "1.2" H 8725 975 50  0001 L CNN "Height"
F 6 "Microchip" H 8725 675 50  0001 L CNN "Manufacturer Name"
F 7 "MIC4606-2YTS-TR" H 8725 575 50  0001 L CNN "Manufacturer Part Number"
F 8 "2.962" H 6425 -3125 50  0001 C CNN "Cost (A$)"
	1    7675 1175
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5F0FE69C
P 7000 2675
F 0 "C5" V 6850 2325 50  0000 L CNN
F 1 "4.7uF" V 6800 2575 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7038 2525 50  0001 C CNN
F 3 "~" H 7000 2675 50  0001 C CNN
F 4 "1276-1244-1-ND" H 7000 2675 50  0001 C CNN "Digi-Key Part Number"
	1    7000 2675
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR014
U 1 1 5F0FE6A4
P 9175 1575
F 0 "#PWR014" H 9175 1425 50  0001 C CNN
F 1 "+9V_Internal" V 9225 1675 50  0000 L CNN
F 2 "" H 9175 1575 50  0001 C CNN
F 3 "" H 9175 1575 50  0001 C CNN
	1    9175 1575
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR013
U 1 1 5F0FE6AA
P 9175 1475
F 0 "#PWR013" H 9175 1225 50  0001 C CNN
F 1 "GND_Internal" V 9225 1375 50  0000 R CNN
F 2 "" H 9175 1475 50  0001 C CNN
F 3 "" H 9175 1475 50  0001 C CNN
	1    9175 1475
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9175 1475 8875 1475
Wire Wire Line
	8875 1575 9175 1575
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR010
U 1 1 5F0FE6B5
P 9450 3525
F 0 "#PWR010" H 9450 3275 50  0001 C CNN
F 1 "GND_Internal" V 9500 3425 50  0000 R CNN
F 2 "" H 9450 3525 50  0001 C CNN
F 3 "" H 9450 3525 50  0001 C CNN
	1    9450 3525
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR06
U 1 1 5F0FE6BB
P 4350 3525
F 0 "#PWR06" H 4350 3275 50  0001 C CNN
F 1 "GND_Internal" V 4400 3425 50  0000 R CNN
F 2 "" H 4350 3525 50  0001 C CNN
F 3 "" H 4350 3525 50  0001 C CNN
	1    4350 3525
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR08
U 1 1 5F0FE6C1
P 3975 1575
F 0 "#PWR08" H 3975 1425 50  0001 C CNN
F 1 "+9V_Internal" V 4025 1675 50  0000 L CNN
F 2 "" H 3975 1575 50  0001 C CNN
F 3 "" H 3975 1575 50  0001 C CNN
	1    3975 1575
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR07
U 1 1 5F0FE6C7
P 3975 1475
F 0 "#PWR07" H 3975 1225 50  0001 C CNN
F 1 "GND_Internal" V 4025 1375 50  0000 R CNN
F 2 "" H 3975 1475 50  0001 C CNN
F 3 "" H 3975 1475 50  0001 C CNN
	1    3975 1475
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3975 1475 3675 1475
Wire Wire Line
	3675 1575 3975 1575
Text GLabel 7150 2325 2    50   BiDi ~ 0
Batt_Next_Inner
Text GLabel 2075 2300 2    50   BiDi ~ 0
Batt_Prev_Outer
Text Notes 6375 850  0    102  ~ 0
Next
Text Notes 1250 850  0    102  ~ 0
Previous
Text Notes 4600 875  0    50   ~ 0
A = Inner\nB = Outer
Text Notes 9800 875  0    50   ~ 0
A = Outer\nB = Inner
$Comp
L Device:C C6
U 1 1 5F0FE6DA
P 9500 2450
F 0 "C6" H 9615 2496 50  0000 L CNN
F 1 "0.1uF" H 9615 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9538 2300 50  0001 C CNN
F 3 "~" H 9500 2450 50  0001 C CNN
F 4 "1276-1043-1-ND" H 9500 2450 50  0001 C CNN "Digi-Key Part Number"
	1    9500 2450
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR012
U 1 1 5F0FE6E0
P 9500 2600
F 0 "#PWR012" H 9500 2350 50  0001 C CNN
F 1 "GND_Internal" H 9505 2427 50  0000 C CNN
F 2 "" H 9500 2600 50  0001 C CNN
F 3 "" H 9500 2600 50  0001 C CNN
	1    9500 2600
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR011
U 1 1 5F0FE6E6
P 9500 2300
F 0 "#PWR011" H 9500 2150 50  0001 C CNN
F 1 "+9V_Internal" H 9550 2400 50  0000 L CNN
F 2 "" H 9500 2300 50  0001 C CNN
F 3 "" H 9500 2300 50  0001 C CNN
	1    9500 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5F0FE6EC
P 4350 2400
F 0 "C3" H 4465 2446 50  0000 L CNN
F 1 "0.1uF" H 4465 2355 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4388 2250 50  0001 C CNN
F 3 "~" H 4350 2400 50  0001 C CNN
F 4 "1276-1043-1-ND" H 4350 2400 50  0001 C CNN "Digi-Key Part Number"
	1    4350 2400
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR05
U 1 1 5F0FE6F2
P 4350 2550
F 0 "#PWR05" H 4350 2300 50  0001 C CNN
F 1 "GND_Internal" H 4355 2377 50  0000 C CNN
F 2 "" H 4350 2550 50  0001 C CNN
F 3 "" H 4350 2550 50  0001 C CNN
	1    4350 2550
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR04
U 1 1 5F0FE6F8
P 4350 2250
F 0 "#PWR04" H 4350 2100 50  0001 C CNN
F 1 "+9V_Internal" H 4400 2350 50  0000 L CNN
F 2 "" H 4350 2250 50  0001 C CNN
F 3 "" H 4350 2250 50  0001 C CNN
	1    4350 2250
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F14A4E0
P 2650 6675
AR Path="/5F14A4E0" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F14A4E0" Ref="C?"  Part="1" 
AR Path="/5F0F169D/5F14A4E0" Ref="C8"  Part="1" 
F 0 "C8" H 2765 6721 50  0000 L CNN
F 1 "4.7uF" H 2765 6630 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2688 6525 50  0001 C CNN
F 3 "~" H 2650 6675 50  0001 C CNN
F 4 "1276-1244-1-ND" H 2650 6675 50  0001 C CNN "Digi-Key Part Number"
	1    2650 6675
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F14A4E6
P 2650 6825
AR Path="/5F14A4E6" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F14A4E6" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F14A4E6" Ref="#PWR018"  Part="1" 
F 0 "#PWR018" H 2650 6575 50  0001 C CNN
F 1 "GND_Internal" H 2655 6652 50  0000 C CNN
F 2 "" H 2650 6825 50  0001 C CNN
F 3 "" H 2650 6825 50  0001 C CNN
	1    2650 6825
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR?
U 1 1 5F14A4EC
P 2650 6525
AR Path="/5F14A4EC" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F14A4EC" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F14A4EC" Ref="#PWR017"  Part="1" 
F 0 "#PWR017" H 2650 6375 50  0001 C CNN
F 1 "+9V_Internal" H 2665 6698 50  0000 C CNN
F 2 "" H 2650 6525 50  0001 C CNN
F 3 "" H 2650 6525 50  0001 C CNN
	1    2650 6525
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F14A4F2
P 1750 6675
AR Path="/5F14A4F2" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F14A4F2" Ref="C?"  Part="1" 
AR Path="/5F0F169D/5F14A4F2" Ref="C7"  Part="1" 
F 0 "C7" H 1865 6721 50  0000 L CNN
F 1 "22uF" H 1865 6630 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.4" H 1788 6525 50  0001 C CNN
F 3 "~" H 1750 6675 50  0001 C CNN
F 4 "732-8416-1-ND" H 1750 6675 50  0001 C CNN "Digi-Key Part Number"
	1    1750 6675
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F14A4F8
P 1750 6825
AR Path="/5F14A4F8" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F14A4F8" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F14A4F8" Ref="#PWR016"  Part="1" 
F 0 "#PWR016" H 1750 6575 50  0001 C CNN
F 1 "GND_Internal" H 1755 6652 50  0000 C CNN
F 2 "" H 1750 6825 50  0001 C CNN
F 3 "" H 1750 6825 50  0001 C CNN
	1    1750 6825
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR?
U 1 1 5F14A4FE
P 1750 6525
AR Path="/5F14A4FE" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F14A4FE" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F14A4FE" Ref="#PWR015"  Part="1" 
F 0 "#PWR015" H 1750 6375 50  0001 C CNN
F 1 "+9V_Internal" H 1765 6698 50  0000 C CNN
F 2 "" H 1750 6525 50  0001 C CNN
F 3 "" H 1750 6525 50  0001 C CNN
	1    1750 6525
	1    0    0    -1  
$EndComp
Text Notes 2525 6275 0    50   ~ 0
MIC4606
Text Notes 1625 6275 0    50   ~ 0
MIC4606\n22uF\n
$Comp
L Device:C C20
U 1 1 5F4B7761
P 2300 4550
F 0 "C20" H 2415 4596 50  0000 L CNN
F 1 "0.1uF" H 2415 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2338 4400 50  0001 C CNN
F 3 "~" H 2300 4550 50  0001 C CNN
F 4 "1276-1043-1-ND" H 2300 4550 50  0001 C CNN "Digi-Key Part Number"
	1    2300 4550
	1    0    0    -1  
$EndComp
Text GLabel 2150 4175 0    50   BiDi ~ 0
Batt_Prev_Inner
Text GLabel 4150 4175 2    50   BiDi ~ 0
Batt_Prev_Outer
$Comp
L Device:C C21
U 1 1 5F4B8924
P 3925 4550
F 0 "C21" H 4040 4596 50  0000 L CNN
F 1 "0.1uF" H 4040 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3963 4400 50  0001 C CNN
F 3 "~" H 3925 4550 50  0001 C CNN
F 4 "1276-1043-1-ND" H 3925 4550 50  0001 C CNN "Digi-Key Part Number"
	1    3925 4550
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F4B8C53
P 2300 4875
AR Path="/5F4B8C53" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F4B8C53" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F4B8C53" Ref="#PWR074"  Part="1" 
F 0 "#PWR074" H 2300 4625 50  0001 C CNN
F 1 "GND_Internal" H 2305 4702 50  0000 C CNN
F 2 "" H 2300 4875 50  0001 C CNN
F 3 "" H 2300 4875 50  0001 C CNN
	1    2300 4875
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F4B8FA3
P 3925 4875
AR Path="/5F4B8FA3" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F4B8FA3" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F4B8FA3" Ref="#PWR075"  Part="1" 
F 0 "#PWR075" H 3925 4625 50  0001 C CNN
F 1 "GND_Internal" H 3930 4702 50  0000 C CNN
F 2 "" H 3925 4875 50  0001 C CNN
F 3 "" H 3925 4875 50  0001 C CNN
	1    3925 4875
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 4175 2300 4175
Wire Wire Line
	2300 4175 2300 4400
Wire Wire Line
	3925 4400 3925 4175
Wire Wire Line
	3925 4175 4150 4175
$Comp
L Device:C C22
U 1 1 5F4BC0D8
P 7375 4575
F 0 "C22" H 7490 4621 50  0000 L CNN
F 1 "0.1uF" H 7490 4530 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 7413 4425 50  0001 C CNN
F 3 "~" H 7375 4575 50  0001 C CNN
F 4 "1276-1043-1-ND" H 7375 4575 50  0001 C CNN "Digi-Key Part Number"
	1    7375 4575
	1    0    0    -1  
$EndComp
$Comp
L Device:C C23
U 1 1 5F4BC0E5
P 8925 4550
F 0 "C23" H 9040 4596 50  0000 L CNN
F 1 "0.1uF" H 9040 4505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8963 4400 50  0001 C CNN
F 3 "~" H 8925 4550 50  0001 C CNN
F 4 "1276-1043-1-ND" H 8925 4550 50  0001 C CNN "Digi-Key Part Number"
	1    8925 4550
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F4BC0EF
P 7375 4900
AR Path="/5F4BC0EF" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F4BC0EF" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F4BC0EF" Ref="#PWR076"  Part="1" 
F 0 "#PWR076" H 7375 4650 50  0001 C CNN
F 1 "GND_Internal" H 7380 4727 50  0000 C CNN
F 2 "" H 7375 4900 50  0001 C CNN
F 3 "" H 7375 4900 50  0001 C CNN
	1    7375 4900
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F4BC0F9
P 8925 4875
AR Path="/5F4BC0F9" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F4BC0F9" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F4BC0F9" Ref="#PWR077"  Part="1" 
F 0 "#PWR077" H 8925 4625 50  0001 C CNN
F 1 "GND_Internal" H 8930 4702 50  0000 C CNN
F 2 "" H 8925 4875 50  0001 C CNN
F 3 "" H 8925 4875 50  0001 C CNN
	1    8925 4875
	1    0    0    -1  
$EndComp
Wire Wire Line
	7225 4200 7375 4200
Wire Wire Line
	7375 4200 7375 4425
Wire Wire Line
	8925 4400 8925 4175
Wire Wire Line
	8925 4175 9150 4175
Text GLabel 7225 4200 0    50   BiDi ~ 0
Batt_Next_Inner
Text GLabel 9150 4175 2    50   BiDi ~ 0
Batt_Next_Outer
Wire Notes Line
	10250 675  10250 5275
Wire Notes Line
	5100 675  5100 5275
$Comp
L Device:R R11
U 1 1 5F4D1CF6
P 2700 3125
F 0 "R11" V 2625 2975 50  0000 C CNN
F 1 "15R" V 2700 3125 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2630 3125 50  0001 C CNN
F 3 "~" H 2700 3125 50  0001 C CNN
F 4 "541-10390-1-ND" H 2700 3125 50  0001 C CNN "Digi-Key Part Number"
F 5 "RCS040215R0FKED" H 2700 3125 50  0001 C CNN "Manufacturer Part Number"
	1    2700 3125
	0    1    1    0   
$EndComp
$Comp
L Device:R R10
U 1 1 5F4D24EE
P 2425 3225
F 0 "R10" V 2375 3050 50  0000 C CNN
F 1 "15R" V 2425 3225 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2355 3225 50  0001 C CNN
F 3 "~" H 2425 3225 50  0001 C CNN
F 4 "541-10390-1-ND" H 2425 3225 50  0001 C CNN "Digi-Key Part Number"
F 5 "RCS040215R0FKED" H 2425 3225 50  0001 C CNN "Manufacturer Part Number"
	1    2425 3225
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5F4D29D4
P 2175 3325
F 0 "R9" V 2125 3150 50  0000 C CNN
F 1 "15R" V 2175 3325 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 2105 3325 50  0001 C CNN
F 3 "~" H 2175 3325 50  0001 C CNN
F 4 "541-10390-1-ND" H 2175 3325 50  0001 C CNN "Digi-Key Part Number"
F 5 "RCS040215R0FKED" H 2175 3325 50  0001 C CNN "Manufacturer Part Number"
	1    2175 3325
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5F4D2E84
P 1925 3425
F 0 "R8" V 1875 3250 50  0000 C CNN
F 1 "15R" V 1925 3425 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 1855 3425 50  0001 C CNN
F 3 "~" H 1925 3425 50  0001 C CNN
F 4 "541-10390-1-ND" H 1925 3425 50  0001 C CNN "Digi-Key Part Number"
F 5 "RCS040215R0FKED" H 1925 3425 50  0001 C CNN "Manufacturer Part Number"
	1    1925 3425
	0    1    1    0   
$EndComp
Wire Wire Line
	2850 3225 2575 3225
Wire Wire Line
	2850 3325 2325 3325
Wire Wire Line
	2850 3425 2075 3425
Wire Wire Line
	1775 3425 1650 3425
Wire Wire Line
	1650 3325 2025 3325
Wire Wire Line
	2275 3225 1650 3225
Wire Wire Line
	1650 3125 2550 3125
$Comp
L Device:R R15
U 1 1 5F4DAAE7
P 7800 3125
F 0 "R15" V 7725 2975 50  0000 C CNN
F 1 "15R" V 7800 3125 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7730 3125 50  0001 C CNN
F 3 "~" H 7800 3125 50  0001 C CNN
F 4 "541-10390-1-ND" H 7800 3125 50  0001 C CNN "Digi-Key Part Number"
F 5 "RCS040215R0FKED" H 7800 3125 50  0001 C CNN "Manufacturer Part Number"
	1    7800 3125
	0    1    1    0   
$EndComp
$Comp
L Device:R R14
U 1 1 5F4DAAF1
P 7525 3225
F 0 "R14" V 7475 3050 50  0000 C CNN
F 1 "15R" V 7525 3225 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7455 3225 50  0001 C CNN
F 3 "~" H 7525 3225 50  0001 C CNN
F 4 "541-10390-1-ND" H 7525 3225 50  0001 C CNN "Digi-Key Part Number"
F 5 "RCS040215R0FKED" H 7525 3225 50  0001 C CNN "Manufacturer Part Number"
	1    7525 3225
	0    1    1    0   
$EndComp
$Comp
L Device:R R13
U 1 1 5F4DAAFB
P 7275 3325
F 0 "R13" V 7225 3150 50  0000 C CNN
F 1 "15R" V 7275 3325 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7205 3325 50  0001 C CNN
F 3 "~" H 7275 3325 50  0001 C CNN
F 4 "541-10390-1-ND" H 7275 3325 50  0001 C CNN "Digi-Key Part Number"
F 5 "RCS040215R0FKED" H 7275 3325 50  0001 C CNN "Manufacturer Part Number"
	1    7275 3325
	0    1    1    0   
$EndComp
$Comp
L Device:R R12
U 1 1 5F4DAB05
P 7025 3425
F 0 "R12" V 6975 3250 50  0000 C CNN
F 1 "15R" V 7025 3425 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 6955 3425 50  0001 C CNN
F 3 "~" H 7025 3425 50  0001 C CNN
F 4 "541-10390-1-ND" H 7025 3425 50  0001 C CNN "Digi-Key Part Number"
F 5 "RCS040215R0FKED" H 7025 3425 50  0001 C CNN "Manufacturer Part Number"
	1    7025 3425
	0    1    1    0   
$EndComp
Wire Wire Line
	7950 3225 7675 3225
Wire Wire Line
	7950 3325 7425 3325
Wire Wire Line
	7950 3425 7175 3425
Wire Wire Line
	6875 3425 6750 3425
Wire Wire Line
	6750 3325 7125 3325
Wire Wire Line
	7375 3225 6750 3225
Wire Wire Line
	6750 3125 7650 3125
Wire Notes Line
	6325 675  6325 5275
Wire Notes Line
	6325 675  10250 675 
Wire Notes Line
	6325 5275 10250 5275
Wire Notes Line
	1175 5275 1175 675 
Wire Notes Line
	1175 675  5100 675 
Wire Notes Line
	1175 5275 5100 5275
Wire Wire Line
	2300 4700 2300 4875
Wire Wire Line
	3925 4700 3925 4875
Wire Wire Line
	7375 4725 7375 4900
Wire Wire Line
	8925 4700 8925 4875
Wire Notes Line
	850  5675 850  7300
Text Notes 900  5875 0    102  ~ 0
MOSFET Driver Bypass Capacitors
Wire Notes Line
	4125 7300 4125 5675
Wire Notes Line
	850  7300 4125 7300
Wire Notes Line
	850  5675 4125 5675
$EndSCHEMATC
