EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "Central Processing Unit"
Date "2020-09-07"
Rev "3"
Comp "Nicholas Konidaris"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 2925 1775 0    50   Input ~ 0
VBat
Text GLabel 4525 2675 2    50   BiDi ~ 0
I2C_Clock_Int
Text GLabel 4525 2575 2    50   BiDi ~ 0
I2C_Data_Int
Text GLabel 2925 2675 0    50   Output ~ 0
FET_1_EN
Text GLabel 2925 2475 0    50   Output ~ 0
FET_1_APWM
Text GLabel 2925 2575 0    50   Output ~ 0
FET_1_BPWM
Text GLabel 2925 2175 0    50   Output ~ 0
FET_2_EN
Text GLabel 2925 2075 0    50   Output ~ 0
FET_2_BPWM
Text GLabel 4525 2875 2    50   Output ~ 0
FET_2_APWM
Text GLabel 2650 3475 0    50   BiDi ~ 0
SYS_SWCLK
Text GLabel 2650 3375 0    50   BiDi ~ 0
SYS_SWDIO
$Comp
L Device:LED D?
U 1 1 5F116387
P 6900 3375
AR Path="/5F116387" Ref="D?"  Part="1" 
AR Path="/5F112C11/5F116387" Ref="D1"  Part="1" 
F 0 "D1" H 6893 3591 50  0000 C CNN
F 1 "LED" H 6893 3500 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 6900 3375 50  0001 C CNN
F 3 "~" H 6900 3375 50  0001 C CNN
F 4 "732-4983-1-ND" H 6900 3375 50  0001 C CNN "Digi-Key Part Number"
	1    6900 3375
	0    1    1    0   
$EndComp
Text GLabel 2925 2775 0    50   Output ~ 0
LED
Text GLabel 6900 2925 1    50   Output ~ 0
LED
$Comp
L Device:R R?
U 1 1 5F116399
P 6900 3075
AR Path="/5F116399" Ref="R?"  Part="1" 
AR Path="/5F112C11/5F116399" Ref="R1"  Part="1" 
F 0 "R1" H 6970 3121 50  0000 L CNN
F 1 "51" H 6970 3030 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6830 3075 50  0001 C CNN
F 3 "~" H 6900 3075 50  0001 C CNN
F 4 "RMCF0805JT51R0CT-ND" H 6900 3075 50  0001 C CNN "Digi-Key Part Number"
	1    6900 3075
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F11639F
P 4750 1400
AR Path="/5F11639F" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F11639F" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 4750 1250 50  0001 C CNN
F 1 "+3.3V_Internal" H 4475 1575 50  0000 L CNN
F 2 "" H 4750 1400 50  0001 C CNN
F 3 "" H 4750 1400 50  0001 C CNN
	1    4750 1400
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F1163A5
P 4700 3825
AR Path="/5F1163A5" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F1163A5" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 4700 3575 50  0001 C CNN
F 1 "GND_Internal" H 4925 3650 50  0000 R CNN
F 2 "" H 4700 3825 50  0001 C CNN
F 3 "" H 4700 3825 50  0001 C CNN
	1    4700 3825
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F1163AB
P 6900 3525
AR Path="/5F1163AB" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F1163AB" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 6900 3275 50  0001 C CNN
F 1 "GND_Internal" H 6905 3352 50  0000 C CNN
F 2 "" H 6900 3525 50  0001 C CNN
F 3 "" H 6900 3525 50  0001 C CNN
	1    6900 3525
	1    0    0    -1  
$EndComp
Text GLabel 2600 3075 0    50   BiDi ~ 0
UARTRX
Text GLabel 2600 2975 0    50   BiDi ~ 0
UARTTX
$Comp
L Device:C C?
U 1 1 5F1163B9
P 2175 6950
AR Path="/5F1163B9" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F1163B9" Ref="C9"  Part="1" 
F 0 "C9" H 2290 6996 50  0000 L CNN
F 1 "0.1uF" H 2290 6905 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2213 6800 50  0001 C CNN
F 3 "~" H 2175 6950 50  0001 C CNN
F 4 "1276-1043-1-ND" H 2175 6950 50  0001 C CNN "Digi-Key Part Number"
	1    2175 6950
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F1163BF
P 2175 7100
AR Path="/5F1163BF" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F1163BF" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 2175 6850 50  0001 C CNN
F 1 "GND_Internal" H 2180 6927 50  0000 C CNN
F 2 "" H 2175 7100 50  0001 C CNN
F 3 "" H 2175 7100 50  0001 C CNN
	1    2175 7100
	1    0    0    -1  
$EndComp
Text GLabel 2550 6625 2    50   Input ~ 0
VTemp
$Comp
L Device:C C?
U 1 1 5F143357
P 4750 6900
AR Path="/5F143357" Ref="C?"  Part="1" 
AR Path="/5F11FE7E/5F143357" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F143357" Ref="C10"  Part="1" 
F 0 "C10" H 4865 6946 50  0000 L CNN
F 1 "0.1uF" H 4865 6855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4788 6750 50  0001 C CNN
F 3 "~" H 4750 6900 50  0001 C CNN
F 4 "1276-1043-1-ND" H 4750 6900 50  0001 C CNN "Digi-Key Part Number"
	1    4750 6900
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F14335D
P 4750 7050
AR Path="/5F14335D" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F14335D" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F14335D" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 4750 6800 50  0001 C CNN
F 1 "GND_Internal" H 4755 6877 50  0000 C CNN
F 2 "" H 4750 7050 50  0001 C CNN
F 3 "" H 4750 7050 50  0001 C CNN
	1    4750 7050
	1    0    0    -1  
$EndComp
Text GLabel 4775 6675 2    50   Output ~ 0
VBat
Wire Wire Line
	4775 6675 4750 6675
Wire Wire Line
	4750 6675 4750 6750
$Comp
L Device:C C?
U 1 1 5F143366
P 3700 6875
AR Path="/5F143366" Ref="C?"  Part="1" 
AR Path="/5F11FE7E/5F143366" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F143366" Ref="C11"  Part="1" 
F 0 "C11" H 3815 6921 50  0000 L CNN
F 1 "0.1uF" H 3815 6830 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3738 6725 50  0001 C CNN
F 3 "~" H 3700 6875 50  0001 C CNN
F 4 "1276-1043-1-ND" H 3700 6875 50  0001 C CNN "Digi-Key Part Number"
	1    3700 6875
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F14336C
P 3700 7025
AR Path="/5F14336C" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F14336C" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F14336C" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 3700 6775 50  0001 C CNN
F 1 "GND_Internal" H 3705 6852 50  0000 C CNN
F 2 "" H 3700 7025 50  0001 C CNN
F 3 "" H 3700 7025 50  0001 C CNN
	1    3700 7025
	1    0    0    -1  
$EndComp
Text GLabel 4050 6650 2    50   Output ~ 0
VCurrent
Wire Wire Line
	3750 6650 3700 6650
Wire Wire Line
	3700 6650 3700 6725
$Comp
L Device:C C?
U 1 1 5F146429
P 9550 5325
AR Path="/5F146429" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F146429" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F146429" Ref="C12"  Part="1" 
F 0 "C12" H 9665 5371 50  0000 L CNN
F 1 "0.1uF" H 9665 5280 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9588 5175 50  0001 C CNN
F 3 "~" H 9550 5325 50  0001 C CNN
F 4 "1276-1043-1-ND" H 9550 5325 50  0001 C CNN "Digi-Key Part Number"
	1    9550 5325
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F14642F
P 9550 5175
AR Path="/5F14642F" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F14642F" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F14642F" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 9550 5025 50  0001 C CNN
F 1 "+3.3V_Internal" H 9565 5348 50  0000 C CNN
F 2 "" H 9550 5175 50  0001 C CNN
F 3 "" H 9550 5175 50  0001 C CNN
	1    9550 5175
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F146435
P 9550 5475
AR Path="/5F146435" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F146435" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F146435" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 9550 5225 50  0001 C CNN
F 1 "GND_Internal" H 9555 5302 50  0000 C CNN
F 2 "" H 9550 5475 50  0001 C CNN
F 3 "" H 9550 5475 50  0001 C CNN
	1    9550 5475
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F14643B
P 10300 5350
AR Path="/5F14643B" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F14643B" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F14643B" Ref="C13"  Part="1" 
F 0 "C13" V 10550 5400 50  0000 L CNN
F 1 "4.7uF" V 10450 5275 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 10338 5200 50  0001 C CNN
F 3 "~" H 10300 5350 50  0001 C CNN
F 4 "1276-1244-1-ND" H 10300 5350 50  0001 C CNN "Digi-Key Part Number"
	1    10300 5350
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F146441
P 10300 5200
AR Path="/5F146441" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F146441" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F146441" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 10300 5050 50  0001 C CNN
F 1 "+3.3V_Internal" H 10300 5425 50  0000 C CNN
F 2 "" H 10300 5200 50  0001 C CNN
F 3 "" H 10300 5200 50  0001 C CNN
	1    10300 5200
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F146447
P 10300 5500
AR Path="/5F146447" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F146447" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F146447" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 10300 5250 50  0001 C CNN
F 1 "GND_Internal" H 10300 5350 50  0000 C CNN
F 2 "" H 10300 5500 50  0001 C CNN
F 3 "" H 10300 5500 50  0001 C CNN
	1    10300 5500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 6625 2175 6625
Wire Wire Line
	2175 6625 2175 6800
$Comp
L Device:R R7
U 1 1 5F4AC049
P 3900 6650
F 0 "R7" V 3693 6650 50  0000 C CNN
F 1 "51" V 3784 6650 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3830 6650 50  0001 C CNN
F 3 "~" H 3900 6650 50  0001 C CNN
F 4 "RMCF0805JT51R0CT-ND" H 3900 6650 50  0001 C CNN "Digi-Key Part Number"
	1    3900 6650
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5F4AC88C
P 2400 6625
F 0 "R6" V 2193 6625 50  0000 C CNN
F 1 "51" V 2284 6625 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2330 6625 50  0001 C CNN
F 3 "~" H 2400 6625 50  0001 C CNN
F 4 "RMCF0805JT51R0CT-ND" H 2400 6625 50  0001 C CNN "Digi-Key Part Number"
	1    2400 6625
	0    1    1    0   
$EndComp
Text Label 2350 2275 2    50   ~ 0
VCurrent_Filtered
Text Label 3700 6650 2    50   ~ 0
VCurrent_Filtered
Text Label 2175 6625 2    50   ~ 0
VTemp_Filtered
Text Label 4525 2075 0    50   ~ 0
VTemp_Filtered
Wire Notes Line
	6025 7625 6025 5950
Wire Notes Line
	6025 5950 925  5950
Wire Notes Line
	925  5950 925  7625
Wire Notes Line
	925  7625 6025 7625
Text Notes 1025 6150 0    79   ~ 0
ADC Input Filter
$Comp
L Li-ion_Thesis_Symbol_Library:STM32G431KBT6 U3
U 1 1 5F4F905D
P 3725 2675
F 0 "U3" H 3725 4142 50  0000 C CNN
F 1 "STM32G431KBT6" H 3725 4051 50  0000 C CNN
F 2 "Package_QFP:LQFP-32_7x7mm_P0.8mm" H 3175 4125 50  0001 L BNN
F 3 "https://www.st.com/resource/en/datasheet/stm32g431kb.pdf" H 3725 2675 50  0001 L BNN
F 4 "497-19470-ND" H 3725 2675 50  0001 C CNN "Digi-Key Part Number"
F 5 "STM32G431KBT6" H 3725 2675 50  0001 C CNN "Manufacturer Part Number"
	1    3725 2675
	1    0    0    -1  
$EndComp
Wire Wire Line
	4525 1675 4750 1675
Wire Wire Line
	4525 1475 4750 1475
Connection ~ 4750 1475
Wire Wire Line
	4750 1475 4750 1400
Wire Wire Line
	4525 1575 4750 1575
Wire Wire Line
	4750 1475 4750 1575
Connection ~ 4750 1575
Wire Wire Line
	4750 1575 4750 1675
$Comp
L Device:C C?
U 1 1 5F4FBDEC
P 8825 5325
AR Path="/5F4FBDEC" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F4FBDEC" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F4FBDEC" Ref="C25"  Part="1" 
F 0 "C25" H 8940 5371 50  0000 L CNN
F 1 "0.1uF" H 8940 5280 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8863 5175 50  0001 C CNN
F 3 "~" H 8825 5325 50  0001 C CNN
F 4 "1276-1043-1-ND" H 8825 5325 50  0001 C CNN "Digi-Key Part Number"
	1    8825 5325
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F4FBDF6
P 8825 5175
AR Path="/5F4FBDF6" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F4FBDF6" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F4FBDF6" Ref="#PWR080"  Part="1" 
F 0 "#PWR080" H 8825 5025 50  0001 C CNN
F 1 "+3.3V_Internal" H 8840 5348 50  0000 C CNN
F 2 "" H 8825 5175 50  0001 C CNN
F 3 "" H 8825 5175 50  0001 C CNN
	1    8825 5175
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F4FBE00
P 8825 5475
AR Path="/5F4FBE00" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F4FBE00" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F4FBE00" Ref="#PWR081"  Part="1" 
F 0 "#PWR081" H 8825 5225 50  0001 C CNN
F 1 "GND_Internal" H 8830 5302 50  0000 C CNN
F 2 "" H 8825 5475 50  0001 C CNN
F 3 "" H 8825 5475 50  0001 C CNN
	1    8825 5475
	1    0    0    -1  
$EndComp
Wire Wire Line
	4525 3575 4700 3575
Wire Wire Line
	4700 3575 4700 3675
Wire Wire Line
	4525 3675 4700 3675
Connection ~ 4700 3675
Wire Wire Line
	4700 3675 4700 3775
Wire Wire Line
	4525 3775 4700 3775
Connection ~ 4700 3775
Wire Wire Line
	4700 3775 4700 3825
Text GLabel 4750 2175 2    50   BiDi ~ 0
SYS_SWO
$Comp
L dk_USB-DVI-HDMI-Connectors:10118193-0001LF J6
U 1 1 5F50081E
P 8275 2575
F 0 "J6" H 8339 3320 60  0000 C CNN
F 1 "10118193-0001LF" H 8339 3214 60  0000 C CNN
F 2 "digikey-footprints:USB_Micro_B_Female_10118193-0001LF" H 8475 2775 60  0001 L CNN
F 3 "http://www.amphenol-icc.com/media/wysiwyg/files/drawing/10118193.pdf" H 8475 2875 60  0001 L CNN
F 4 "Connectors, Interconnects" H 8475 3175 60  0001 L CNN "Category"
F 5 "USB, DVI, HDMI Connectors" H 8475 3275 60  0001 L CNN "Family"
F 6 "http://www.amphenol-icc.com/media/wysiwyg/files/drawing/10118193.pdf" H 8475 3375 60  0001 L CNN "DK_Datasheet_Link"
F 7 "/product-detail/en/amphenol-icc-fci/10118193-0001LF/609-4616-1-ND/2785380" H 8475 3475 60  0001 L CNN "DK_Detail_Page"
F 8 "CONN RCPT USB2.0 MICRO B SMD R/A" H 8475 3575 60  0001 L CNN "Description"
F 9 "Amphenol ICC (FCI)" H 8475 3675 60  0001 L CNN "Manufacturer"
F 10 "Active" H 8475 3775 60  0001 L CNN "Status"
F 11 "609-4616-1-ND" H 8275 2575 50  0001 C CNN "Digi-Key Part Number"
F 12 "10118193-0001LF" H 8275 2575 50  0001 C CNN "Manufacturer Part Number"
	1    8275 2575
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR085
U 1 1 5F504F21
P 9125 1950
F 0 "#PWR085" H 9125 1800 50  0001 C CNN
F 1 "+9V_Internal" H 9140 2123 50  0000 C CNN
F 2 "" H 9125 1950 50  0001 C CNN
F 3 "" H 9125 1950 50  0001 C CNN
	1    9125 1950
	1    0    0    -1  
$EndComp
Text Notes 9300 2300 0    50   ~ 0
WARNING: Do NOT connect USB cable \nwhen external 5V supply is present\n and USB power jumper bridged!
Wire Wire Line
	8575 2375 9125 2375
Wire Wire Line
	9125 2075 9125 1950
Text Label 8575 2475 0    50   ~ 0
USB_N
Text Label 8575 2575 0    50   ~ 0
USB_P
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F50700C
P 8775 2825
AR Path="/5F50700C" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F50700C" Ref="#PWR084"  Part="1" 
F 0 "#PWR084" H 8775 2575 50  0001 C CNN
F 1 "GND_Internal" H 8780 2652 50  0000 C CNN
F 2 "" H 8775 2825 50  0001 C CNN
F 3 "" H 8775 2825 50  0001 C CNN
	1    8775 2825
	1    0    0    -1  
$EndComp
Wire Wire Line
	8575 2775 8775 2775
Wire Wire Line
	8775 2775 8775 2825
NoConn ~ 8575 2675
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F508680
P 8175 3175
AR Path="/5F508680" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F508680" Ref="#PWR083"  Part="1" 
F 0 "#PWR083" H 8175 2925 50  0001 C CNN
F 1 "GND_Internal" H 8180 3002 50  0000 C CNN
F 2 "" H 8175 3175 50  0001 C CNN
F 3 "" H 8175 3175 50  0001 C CNN
	1    8175 3175
	1    0    0    -1  
$EndComp
Text Label 2925 3175 2    50   ~ 0
USB_N
Text Label 2925 3275 2    50   ~ 0
USB_P
Wire Wire Line
	2600 3075 2925 3075
Wire Wire Line
	2925 2975 2600 2975
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F50CA11
P 1225 2750
AR Path="/5F50CA11" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F50CA11" Ref="#PWR082"  Part="1" 
F 0 "#PWR082" H 1225 2500 50  0001 C CNN
F 1 "GND_Internal" H 1230 2577 50  0000 C CNN
F 2 "" H 1225 2750 50  0001 C CNN
F 3 "" H 1225 2750 50  0001 C CNN
	1    1225 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1225 2375 1225 2450
$Comp
L Device:C C?
U 1 1 5F52D852
P 8075 5300
AR Path="/5F52D852" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F52D852" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F52D852" Ref="C26"  Part="1" 
F 0 "C26" H 8190 5346 50  0000 L CNN
F 1 "0.1uF" H 8190 5255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8113 5150 50  0001 C CNN
F 3 "~" H 8075 5300 50  0001 C CNN
F 4 "1276-1043-1-ND" H 8075 5300 50  0001 C CNN "Digi-Key Part Number"
	1    8075 5300
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F52D85C
P 8075 5150
AR Path="/5F52D85C" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F52D85C" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F52D85C" Ref="#PWR086"  Part="1" 
F 0 "#PWR086" H 8075 5000 50  0001 C CNN
F 1 "+3.3V_Internal" H 8090 5323 50  0000 C CNN
F 2 "" H 8075 5150 50  0001 C CNN
F 3 "" H 8075 5150 50  0001 C CNN
	1    8075 5150
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F52D866
P 8075 5450
AR Path="/5F52D866" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F52D866" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F52D866" Ref="#PWR087"  Part="1" 
F 0 "#PWR087" H 8075 5200 50  0001 C CNN
F 1 "GND_Internal" H 8080 5277 50  0000 C CNN
F 2 "" H 8075 5450 50  0001 C CNN
F 3 "" H 8075 5450 50  0001 C CNN
	1    8075 5450
	1    0    0    -1  
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP?
U 1 1 5F502486
P 9125 2225
AR Path="/5F502486" Ref="JP?"  Part="1" 
AR Path="/5F112C11/5F502486" Ref="JP3"  Part="1" 
F 0 "JP3" V 9079 2293 50  0000 L CNN
F 1 "USB Power" V 9225 2325 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 9125 2225 50  0001 C CNN
F 3 "~" H 9125 2225 50  0001 C CNN
F 4 " ‎PH1-12-UA‎" H 9125 2225 50  0001 C CNN "Digi-Key Part Number"
	1    9125 2225
	0    -1   -1   0   
$EndComp
Text Label 4525 2275 0    50   ~ 0
PB4
Text Label 4525 2475 0    50   ~ 0
PB6
Wire Wire Line
	4750 2175 4525 2175
Text Label 2925 3575 2    50   ~ 0
PA15
Wire Wire Line
	2650 3375 2925 3375
Wire Wire Line
	2925 3475 2650 3475
Text Label 6900 1675 0    50   ~ 0
PB4
Text Label 4150 5125 0    50   ~ 0
I2C3_Data
Text Label 6900 1875 0    50   ~ 0
PB6
Text Label 6900 1575 0    50   ~ 0
PA15
Text Label 2625 1875 2    50   ~ 0
PF1
$Comp
L Connector:Conn_01x06_Male J7
U 1 1 5F56200B
P 6700 1775
F 0 "J7" H 6800 2200 50  0000 C CNN
F 1 "Conn_01x06_Male" H 6800 2125 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 6700 1775 50  0001 C CNN
F 3 "~" H 6700 1775 50  0001 C CNN
F 4 " ‎PH1-12-UA‎" H 6700 1775 50  0001 C CNN "Digi-Key Part Number"
	1    6700 1775
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F563072
P 7000 2125
AR Path="/5F563072" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F563072" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 7000 1875 50  0001 C CNN
F 1 "GND_Internal" H 7225 1950 50  0000 R CNN
F 2 "" H 7000 2125 50  0001 C CNN
F 3 "" H 7000 2125 50  0001 C CNN
	1    7000 2125
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 2075 7000 2075
Wire Wire Line
	7000 2075 7000 2125
$Comp
L Memory_EEPROM:M24C02-RMN U6
U 1 1 5F58773A
P 3750 5225
F 0 "U6" H 4100 5625 50  0000 C CNN
F 1 "M24C02-RMN" H 4150 5525 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 3750 5575 50  0001 C CNN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/b0/d8/50/40/5a/85/49/6f/DM00071904.pdf/files/DM00071904.pdf/jcr:content/translations/en.DM00071904.pdf" H 3800 4725 50  0001 C CNN
F 4 "497-8559-ND" H 3750 5225 50  0001 C CNN "Digi-Key Part Number"
F 5 "M24C02-RMN6P" H 3750 5225 50  0001 C CNN "Manufacturer Part Number"
	1    3750 5225
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F587EBB
P 3100 5350
AR Path="/5F587EBB" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F587EBB" Ref="#PWR088"  Part="1" 
F 0 "#PWR088" H 3100 5100 50  0001 C CNN
F 1 "GND_Internal" H 3325 5175 50  0000 R CNN
F 2 "" H 3100 5350 50  0001 C CNN
F 3 "" H 3100 5350 50  0001 C CNN
	1    3100 5350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 5125 3100 5125
Wire Wire Line
	3100 5125 3100 5225
Wire Wire Line
	3350 5225 3100 5225
Connection ~ 3100 5225
Wire Wire Line
	3100 5225 3100 5325
Wire Wire Line
	3350 5325 3100 5325
Connection ~ 3100 5325
Wire Wire Line
	3100 5325 3100 5350
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F589BC4
P 3750 5600
AR Path="/5F589BC4" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F589BC4" Ref="#PWR090"  Part="1" 
F 0 "#PWR090" H 3750 5350 50  0001 C CNN
F 1 "GND_Internal" H 3975 5425 50  0000 R CNN
F 2 "" H 3750 5600 50  0001 C CNN
F 3 "" H 3750 5600 50  0001 C CNN
	1    3750 5600
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F589DE6
P 3750 4875
AR Path="/5F589DE6" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F589DE6" Ref="#PWR089"  Part="1" 
F 0 "#PWR089" H 3750 4725 50  0001 C CNN
F 1 "+3.3V_Internal" H 3475 5050 50  0000 L CNN
F 2 "" H 3750 4875 50  0001 C CNN
F 3 "" H 3750 4875 50  0001 C CNN
	1    3750 4875
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 4925 3750 4875
Wire Wire Line
	3750 5525 3750 5600
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F58BD2F
P 4250 5375
AR Path="/5F58BD2F" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F58BD2F" Ref="#PWR091"  Part="1" 
F 0 "#PWR091" H 4250 5125 50  0001 C CNN
F 1 "GND_Internal" H 4475 5200 50  0000 R CNN
F 2 "" H 4250 5375 50  0001 C CNN
F 3 "" H 4250 5375 50  0001 C CNN
	1    4250 5375
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 5325 4250 5325
Wire Wire Line
	4250 5325 4250 5375
Text Label 2700 2875 2    50   ~ 0
I2C3_CLK
Wire Wire Line
	2925 2875 2700 2875
Text Label 4150 5225 0    50   ~ 0
I2C3_CLK
Text Label 4525 2375 0    50   ~ 0
I2C3_Data
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F5934AA
P 7400 1750
AR Path="/5F5934AA" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F5934AA" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 7400 1600 50  0001 C CNN
F 1 "+3.3V_Internal" H 7125 1925 50  0000 L CNN
F 2 "" H 7400 1750 50  0001 C CNN
F 3 "" H 7400 1750 50  0001 C CNN
	1    7400 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 1775 7400 1775
Wire Wire Line
	7400 1775 7400 1750
$Comp
L Device:C C?
U 1 1 5F596BBE
P 4975 5250
AR Path="/5F596BBE" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F596BBE" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F596BBE" Ref="C27"  Part="1" 
F 0 "C27" H 5090 5296 50  0000 L CNN
F 1 "0.1uF" H 5090 5205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5013 5100 50  0001 C CNN
F 3 "~" H 4975 5250 50  0001 C CNN
F 4 "1276-1043-1-ND" H 4975 5250 50  0001 C CNN "Digi-Key Part Number"
	1    4975 5250
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F596BC8
P 4975 5100
AR Path="/5F596BC8" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F596BC8" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F596BC8" Ref="#PWR092"  Part="1" 
F 0 "#PWR092" H 4975 4950 50  0001 C CNN
F 1 "+3.3V_Internal" H 4990 5273 50  0000 C CNN
F 2 "" H 4975 5100 50  0001 C CNN
F 3 "" H 4975 5100 50  0001 C CNN
	1    4975 5100
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F596BD2
P 4975 5400
AR Path="/5F596BD2" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F596BD2" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F596BD2" Ref="#PWR093"  Part="1" 
F 0 "#PWR093" H 4975 5150 50  0001 C CNN
F 1 "GND_Internal" H 4980 5227 50  0000 C CNN
F 2 "" H 4975 5400 50  0001 C CNN
F 3 "" H 4975 5400 50  0001 C CNN
	1    4975 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2925 2275 2350 2275
Wire Wire Line
	2925 1875 2625 1875
Text Label 6900 1975 0    50   ~ 0
PF1
$Comp
L Device:R R17
U 1 1 5F517C86
P 2325 5250
F 0 "R17" V 2118 5250 50  0000 C CNN
F 1 "1K" V 2209 5250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 2255 5250 50  0001 C CNN
F 3 "~" H 2325 5250 50  0001 C CNN
F 4 "2019-RK73B2ATTD102JCT-ND" H 2325 5250 50  0001 C CNN "Digi-Key Part Number"
F 5 "RK73B2ATTD102J" H 2325 5250 50  0001 C CNN "Manufacturer Part Number"
	1    2325 5250
	-1   0    0    1   
$EndComp
$Comp
L Device:R R16
U 1 1 5F5182F4
P 2000 5250
F 0 "R16" V 1793 5250 50  0000 C CNN
F 1 "1K" V 1884 5250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 1930 5250 50  0001 C CNN
F 3 "~" H 2000 5250 50  0001 C CNN
F 4 "2019-RK73B2ATTD102JCT-ND" H 2000 5250 50  0001 C CNN "Digi-Key Part Number"
F 5 "RK73B2ATTD102J" H 2000 5250 50  0001 C CNN "Manufacturer Part Number"
	1    2000 5250
	-1   0    0    1   
$EndComp
Text Label 2325 5550 0    50   ~ 0
I2C3_Data
Text Label 2000 5550 2    50   ~ 0
I2C3_CLK
Wire Wire Line
	2325 5550 2325 5400
Wire Wire Line
	2000 5550 2000 5400
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F51A068
P 2150 4950
AR Path="/5F51A068" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F51A068" Ref="#PWR094"  Part="1" 
F 0 "#PWR094" H 2150 4800 50  0001 C CNN
F 1 "+3.3V_Internal" H 1875 5125 50  0000 L CNN
F 2 "" H 2150 4950 50  0001 C CNN
F 3 "" H 2150 4950 50  0001 C CNN
	1    2150 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	2325 5100 2325 4950
Wire Wire Line
	2325 4950 2150 4950
Wire Wire Line
	2150 4950 2000 4950
Wire Wire Line
	2000 4950 2000 5100
Connection ~ 2150 4950
$Comp
L Device:R R19
U 1 1 5F57C57D
P 1225 2600
F 0 "R19" V 1018 2600 50  0000 C CNN
F 1 "1K" V 1109 2600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1155 2600 50  0001 C CNN
F 3 "~" H 1225 2600 50  0001 C CNN
F 4 "2019-RK73B2ATTD102JCT-ND" H 1225 2600 50  0001 C CNN "Digi-Key Part Number"
F 5 "RK73B2ATTD102J" H 1225 2600 50  0001 C CNN "Manufacturer Part Number"
	1    1225 2600
	-1   0    0    1   
$EndComp
$Comp
L Device:R R18
U 1 1 5F57C91B
P 1225 2225
F 0 "R18" V 1018 2225 50  0000 C CNN
F 1 "1K" V 1109 2225 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1155 2225 50  0001 C CNN
F 3 "~" H 1225 2225 50  0001 C CNN
F 4 "2019-RK73B2ATTD102JCT-ND" H 1225 2225 50  0001 C CNN "Digi-Key Part Number"
F 5 "RK73B2ATTD102J" H 1225 2225 50  0001 C CNN "Manufacturer Part Number"
	1    1225 2225
	-1   0    0    1   
$EndComp
Connection ~ 1225 2375
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F57CCB2
P 1225 2075
AR Path="/5F57CCB2" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F57CCB2" Ref="#PWR095"  Part="1" 
F 0 "#PWR095" H 1225 1925 50  0001 C CNN
F 1 "+3.3V_Internal" H 950 2250 50  0000 L CNN
F 2 "" H 1225 2075 50  0001 C CNN
F 3 "" H 1225 2075 50  0001 C CNN
	1    1225 2075
	1    0    0    -1  
$EndComp
Wire Wire Line
	1225 2375 2925 2375
$Comp
L Device:C C?
U 1 1 5F5870C5
P 7300 5275
AR Path="/5F5870C5" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F5870C5" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F5870C5" Ref="C28"  Part="1" 
F 0 "C28" H 7415 5321 50  0000 L CNN
F 1 "2.2uF" H 7415 5230 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7338 5125 50  0001 C CNN
F 3 "~" H 7300 5275 50  0001 C CNN
F 4 "1276-1043-1-ND" H 7300 5275 50  0001 C CNN "Digi-Key Part Number"
	1    7300 5275
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F5870CF
P 7300 5125
AR Path="/5F5870CF" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F5870CF" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F5870CF" Ref="#PWR096"  Part="1" 
F 0 "#PWR096" H 7300 4975 50  0001 C CNN
F 1 "+3.3V_Internal" H 7315 5298 50  0000 C CNN
F 2 "" H 7300 5125 50  0001 C CNN
F 3 "" H 7300 5125 50  0001 C CNN
	1    7300 5125
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F5870D9
P 7300 5425
AR Path="/5F5870D9" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F5870D9" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F5870D9" Ref="#PWR097"  Part="1" 
F 0 "#PWR097" H 7300 5175 50  0001 C CNN
F 1 "GND_Internal" H 7305 5252 50  0000 C CNN
F 2 "" H 7300 5425 50  0001 C CNN
F 3 "" H 7300 5425 50  0001 C CNN
	1    7300 5425
	1    0    0    -1  
$EndComp
Wire Notes Line
	5525 4350 5525 725 
Wire Notes Line
	5525 725  725  725 
Wire Notes Line
	725  725  725  4350
Wire Notes Line
	725  4350 5525 4350
Wire Notes Line
	5575 5925 5575 4400
Wire Notes Line
	5575 4400 1250 4400
Wire Notes Line
	1250 4400 1250 5925
Wire Notes Line
	1250 5925 5575 5925
Text Notes 1350 4575 0    79   ~ 0
EEPROM Expansion I2C IC
Text Notes 800  925  0    79   ~ 0
MCU: STM32G431KB \n
Text Notes 875  2875 1    50   ~ 0
Module Config Selector 
Wire Notes Line
	10900 4125 10900 875 
Wire Notes Line
	10900 875  6075 875 
Wire Notes Line
	6075 875  6075 4125
Wire Notes Line
	6075 4125 10900 4125
Text Notes 6175 1075 0    79   ~ 0
MCU External Connectors and Peripherals\n
Wire Notes Line
	11000 6125 11000 4475
Wire Notes Line
	11000 4475 6600 4475
Wire Notes Line
	6600 4475 6600 6125
Wire Notes Line
	6600 6125 11000 6125
Text Notes 6650 4650 0    79   ~ 0
MCU Bypass Capacitors
$EndSCHEMATC
