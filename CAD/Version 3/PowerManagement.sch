EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "Power Supply"
Date "2020-09-07"
Rev "3"
Comp "Nicholas Konidaris"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Li-ion_Thesis_Symbol_Library:AZ1117-3.3 IC?
U 1 1 5F119AA5
P 8650 1950
AR Path="/5F119AA5" Ref="IC?"  Part="1" 
AR Path="/5F116BFA/5F119AA5" Ref="IC3"  Part="1" 
F 0 "IC3" H 8750 2200 50  0000 C CNN
F 1 "ZLDO1117G33TA" H 8800 2100 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:Voltage_Regulator_SOT-223" H 10000 2050 50  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/AZ1117.pdf" H 10000 1950 50  0001 L CNN
F 4 "0.493" H 8650 1950 50  0001 C CNN "Cost (A$)"
F 5 "AZ1117IH-3.3TRG1DICT-ND" H 8650 1950 50  0001 C CNN "Digi-Key Part Number"
F 6 "AZ1117IH-3.3TRG1" H 8650 1950 50  0001 C CNN "Manufacturer Part Number"
	1    8650 1950
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F119AAB
P 9200 2100
AR Path="/5F119AAB" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AAB" Ref="#PWR041"  Part="1" 
F 0 "#PWR041" H 9200 1950 50  0001 C CNN
F 1 "+3.3V_Internal" V 9215 2228 50  0000 L CNN
F 2 "" H 9200 2100 50  0001 C CNN
F 3 "" H 9200 2100 50  0001 C CNN
	1    9200 2100
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F119AB1
P 8650 2250
AR Path="/5F119AB1" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AB1" Ref="#PWR039"  Part="1" 
F 0 "#PWR039" H 8650 2000 50  0001 C CNN
F 1 "GND_Internal" H 8655 2077 50  0000 C CNN
F 2 "" H 8650 2250 50  0001 C CNN
F 3 "" H 8650 2250 50  0001 C CNN
	1    8650 2250
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR?
U 1 1 5F119AC9
P 8350 1950
AR Path="/5F119AC9" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AC9" Ref="#PWR034"  Part="1" 
F 0 "#PWR034" H 8350 1800 50  0001 C CNN
F 1 "+9V_Internal" V 8365 2077 50  0000 L CNN
F 2 "" H 8350 1950 50  0001 C CNN
F 3 "" H 8350 1950 50  0001 C CNN
	1    8350 1950
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F119AE8
P 2125 2575
AR Path="/5F119AE8" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AE8" Ref="#PWR035"  Part="1" 
F 0 "#PWR035" H 2125 2425 50  0001 C CNN
F 1 "+5V_External" V 2125 2975 50  0000 C CNN
F 2 "" H 2125 2575 50  0001 C CNN
F 3 "" H 2125 2575 50  0001 C CNN
	1    2125 2575
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR?
U 1 1 5F119AEE
P 3125 2575
AR Path="/5F119AEE" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AEE" Ref="#PWR042"  Part="1" 
F 0 "#PWR042" H 3125 2425 50  0001 C CNN
F 1 "+9V_Internal" V 3140 2702 50  0000 L CNN
F 2 "" H 3125 2575 50  0001 C CNN
F 3 "" H 3125 2575 50  0001 C CNN
	1    3125 2575
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F119AF4
P 2125 2725
AR Path="/5F119AF4" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AF4" Ref="#PWR036"  Part="1" 
F 0 "#PWR036" H 2125 2475 50  0001 C CNN
F 1 "GND_External" V 2125 2325 50  0000 C CNN
F 2 "" H 2125 2725 50  0001 C CNN
F 3 "" H 2125 2725 50  0001 C CNN
	1    2125 2725
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F119AFA
P 3125 2725
AR Path="/5F119AFA" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AFA" Ref="#PWR043"  Part="1" 
F 0 "#PWR043" H 3125 2475 50  0001 C CNN
F 1 "GND_Internal" V 3125 2325 50  0000 C CNN
F 2 "" H 3125 2725 50  0001 C CNN
F 3 "" H 3125 2725 50  0001 C CNN
	1    3125 2725
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:PDSE1-S5-S9-S U?
U 1 1 5F119B03
P 2625 2625
AR Path="/5F119B03" Ref="U?"  Part="1" 
AR Path="/5F116BFA/5F119B03" Ref="U4"  Part="1" 
F 0 "U4" H 2625 2940 50  0000 C CNN
F 1 "PDSE1-S5-S9-S" H 2625 2849 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:CONV_PDSE1-S5-S9-S" H 2625 3075 50  0001 C CNN
F 3 "https://www.xppower.com/portals/0/pdfs/SF_IES01.pdf" H 2625 3075 50  0001 C CNN
F 4 "102-6292-ND" H 2625 2625 50  0001 C CNN "Digi-Key Part Number"
	1    2625 2625
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F119B09
P 5900 2150
AR Path="/5F119B09" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F119B09" Ref="C16"  Part="1" 
F 0 "C16" H 6015 2196 50  0000 L CNN
F 1 "2.2uF" H 6015 2105 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.4" H 5938 2000 50  0001 C CNN
F 3 "~" H 5900 2150 50  0001 C CNN
F 4 "732-8416-1-ND" H 5900 2150 50  0001 C CNN "Digi-Key Part Number"
	1    5900 2150
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F119B0F
P 5900 2300
AR Path="/5F119B0F" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B0F" Ref="#PWR045"  Part="1" 
F 0 "#PWR045" H 5900 2050 50  0001 C CNN
F 1 "GND_Internal" H 5905 2127 50  0000 C CNN
F 2 "" H 5900 2300 50  0001 C CNN
F 3 "" H 5900 2300 50  0001 C CNN
	1    5900 2300
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5F119B15
P 5900 2000
AR Path="/5F119B15" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B15" Ref="#PWR044"  Part="1" 
F 0 "#PWR044" H 5900 1850 50  0001 C CNN
F 1 "+BATT" H 5750 2150 50  0000 L CNN
F 2 "" H 5900 2000 50  0001 C CNN
F 3 "" H 5900 2000 50  0001 C CNN
	1    5900 2000
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F119B1B
P 9200 1950
AR Path="/5F119B1B" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B1B" Ref="#PWR040"  Part="1" 
F 0 "#PWR040" H 9200 1800 50  0001 C CNN
F 1 "+3.3V_Internal" V 9215 2078 50  0000 L CNN
F 2 "" H 9200 1950 50  0001 C CNN
F 3 "" H 9200 1950 50  0001 C CNN
	1    9200 1950
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F119B21
P 2275 3550
AR Path="/5F119B21" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F119B21" Ref="C14"  Part="1" 
F 0 "C14" H 2390 3596 50  0000 L CNN
F 1 "22uF" H 2390 3505 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.4" H 2313 3400 50  0001 C CNN
F 3 "~" H 2275 3550 50  0001 C CNN
F 4 "732-8432-1-ND" H 2275 3550 50  0001 C CNN "Digi-Key Part Number"
	1    2275 3550
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F119B27
P 2275 3700
AR Path="/5F119B27" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B27" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 2275 3450 50  0001 C CNN
F 1 "GND_External" H 2280 3527 50  0000 C CNN
F 2 "" H 2275 3700 50  0001 C CNN
F 3 "" H 2275 3700 50  0001 C CNN
	1    2275 3700
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F119B2D
P 2275 3400
AR Path="/5F119B2D" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B2D" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 2275 3250 50  0001 C CNN
F 1 "+5V_External" H 2290 3573 50  0000 C CNN
F 2 "" H 2275 3400 50  0001 C CNN
F 3 "" H 2275 3400 50  0001 C CNN
	1    2275 3400
	1    0    0    -1  
$EndComp
Text Notes 2500 3100 0    50   ~ 0
PDSE1
$Comp
L Device:C C?
U 1 1 5F119B84
P 2900 3575
AR Path="/5F119B84" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F119B84" Ref="C15"  Part="1" 
F 0 "C15" H 3025 3600 50  0000 L CNN
F 1 "4.7uF" H 3025 3500 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2938 3425 50  0001 C CNN
F 3 "~" H 2900 3575 50  0001 C CNN
F 4 "1276-1244-1-ND" H 2900 3575 50  0001 C CNN "Digi-Key Part Number"
	1    2900 3575
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F119B8A
P 2900 3725
AR Path="/5F119B8A" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B8A" Ref="#PWR038"  Part="1" 
F 0 "#PWR038" H 2900 3475 50  0001 C CNN
F 1 "GND_External" H 2950 3550 50  0000 C CNN
F 2 "" H 2900 3725 50  0001 C CNN
F 3 "" H 2900 3725 50  0001 C CNN
	1    2900 3725
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F119B90
P 2900 3425
AR Path="/5F119B90" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B90" Ref="#PWR037"  Part="1" 
F 0 "#PWR037" H 2900 3275 50  0001 C CNN
F 1 "+5V_External" H 2900 3625 50  0000 C CNN
F 2 "" H 2900 3425 50  0001 C CNN
F 3 "" H 2900 3425 50  0001 C CNN
	1    2900 3425
	1    0    0    -1  
$EndComp
Wire Notes Line
	3750 6350 3725 6350
$Comp
L Device:C C24
U 1 1 5F53A028
P 2625 1750
F 0 "C24" V 2373 1750 50  0000 C CNN
F 1 "33pF" V 2464 1750 50  0000 C CNN
F 2 "Capacitor_SMD:C_1806_4516Metric" H 2663 1600 50  0001 C CNN
F 3 "~" H 2625 1750 50  0001 C CNN
F 4 "N/A" H 2625 1750 50  0001 C CNN "Digi-Key Part Number"
	1    2625 1750
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F53A751
P 2325 1850
AR Path="/5F53A751" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F53A751" Ref="#PWR078"  Part="1" 
F 0 "#PWR078" H 2325 1600 50  0001 C CNN
F 1 "GND_External" H 2325 1650 50  0000 C CNN
F 2 "" H 2325 1850 50  0001 C CNN
F 3 "" H 2325 1850 50  0001 C CNN
	1    2325 1850
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F53AA69
P 2900 1850
AR Path="/5F53AA69" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F53AA69" Ref="#PWR079"  Part="1" 
F 0 "#PWR079" H 2900 1600 50  0001 C CNN
F 1 "GND_Internal" H 2900 1650 50  0000 C CNN
F 2 "" H 2900 1850 50  0001 C CNN
F 3 "" H 2900 1850 50  0001 C CNN
	1    2900 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2475 1750 2325 1750
Wire Wire Line
	2325 1750 2325 1850
Wire Wire Line
	2775 1750 2900 1750
Wire Wire Line
	2900 1750 2900 1850
Text Notes 1800 1700 0    50   ~ 0
Y Class Capacitor 
Wire Notes Line
	4575 4475 4575 725 
Wire Notes Line
	4575 725  750  725 
Wire Notes Line
	750  725  750  4475
Wire Notes Line
	750  4475 4575 4475
Wire Notes Line
	4875 1300 4875 2925
Text Notes 4925 1475 0    79   ~ 0
Battery Reservoir Capacitor
Wire Notes Line
	7025 2925 7025 1300
Wire Notes Line
	4875 1300 7025 1300
Wire Notes Line
	4875 2925 7025 2925
Wire Notes Line
	7275 1250 7275 2925
Wire Notes Line
	7275 2925 10350 2925
Wire Notes Line
	10350 2925 10350 1250
Wire Notes Line
	7275 1250 10350 1250
Text Notes 7350 1425 0    79   ~ 0
9V to 3.3V Voltage Regulator
Text Notes 825  1050 0    79   ~ 0
DC-DC Converter / Isolator \n5V to 9V
$EndSCHEMATC
