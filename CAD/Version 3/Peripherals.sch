EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title "Peripherals"
Date "2020-09-07"
Rev "3"
Comp "Nicholas Konidaris"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 2650 2000 2    50   Output ~ 0
VCurrent
Text GLabel 5900 1900 2    50   Output ~ 0
VBat
Wire Wire Line
	5800 1350 5800 1500
Wire Wire Line
	5800 2300 5800 2450
Text GLabel 1350 2000 0    50   Input ~ 0
BATT_IN
$Comp
L Li-ion_Thesis_Symbol_Library:ACS70331EESATR-005B3 IC?
U 1 1 5F12283C
P 1350 2000
AR Path="/5F12283C" Ref="IC?"  Part="1" 
AR Path="/5F11FE7E/5F12283C" Ref="IC4"  Part="1" 
F 0 "IC4" H 2600 2600 50  0000 C CNN
F 1 "ACS70331EESATR-005B3" H 2600 2500 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:Allegro_QFN-12-10-1EP_3x3mm_P0.5mm" H 2400 2400 50  0001 L CNN
F 3 "https://www.allegromicro.com/~/media/Files/Datasheets/ACS70331-Datasheet.ashx?la=en&hash=6ED550430D2A1B2B4764E95E08812EDF083CCD64" H 2400 2300 50  0001 L CNN
F 4 "2.728" H -150 50  50  0001 C CNN "Cost (A$)"
F 5 "620-1887-1-ND" H -150 50  50  0001 C CNN "Digi-Key Part Number"
F 6 "ACS70331EESATR-005B3" H 1350 2000 50  0001 C CNN "Manufacturer Part Number"
F 7 "Allegro Microsystems" H 1350 2000 50  0001 C CNN "Manufacturer Name"
	1    1350 2000
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5F122842
P 1350 2300
AR Path="/5F122842" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122842" Ref="#PWR057"  Part="1" 
F 0 "#PWR057" H 1350 2150 50  0001 C CNN
F 1 "+BATT" H 1050 2350 50  0000 L CNN
F 2 "" H 1350 2300 50  0001 C CNN
F 3 "" H 1350 2300 50  0001 C CNN
	1    1350 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F122848
P 5800 1650
AR Path="/5F122848" Ref="R?"  Part="1" 
AR Path="/5F11FE7E/5F122848" Ref="R4"  Part="1" 
F 0 "R4" H 5870 1696 50  0000 L CNN
F 1 "100K" H 5870 1605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5730 1650 50  0001 C CNN
F 3 "~" H 5800 1650 50  0001 C CNN
F 4 "RMCF0805FT100KCT-ND" H 5800 1650 50  0001 C CNN "Digi-Key Part Number"
	1    5800 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F12284E
P 5800 2150
AR Path="/5F12284E" Ref="R?"  Part="1" 
AR Path="/5F11FE7E/5F12284E" Ref="R5"  Part="1" 
F 0 "R5" H 5870 2196 50  0000 L CNN
F 1 "200K" H 5870 2105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5730 2150 50  0001 C CNN
F 3 "~" H 5800 2150 50  0001 C CNN
F 4 "RMCF0805FT200KCT-ND" H 5800 2150 50  0001 C CNN "Digi-Key Part Number"
	1    5800 2150
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5F122854
P 5800 1350
AR Path="/5F122854" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122854" Ref="#PWR072"  Part="1" 
F 0 "#PWR072" H 5800 1200 50  0001 C CNN
F 1 "+BATT" H 5815 1523 50  0000 C CNN
F 2 "" H 5800 1350 50  0001 C CNN
F 3 "" H 5800 1350 50  0001 C CNN
	1    5800 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2550 2100 2800 2100
Wire Wire Line
	2550 2200 2800 2200
Wire Wire Line
	2800 2100 2800 2200
Connection ~ 2800 2200
Wire Wire Line
	1850 2900 1850 2950
Wire Wire Line
	1850 2950 1900 2950
Wire Wire Line
	1950 2950 1950 2900
Wire Wire Line
	1900 2950 1900 3000
Connection ~ 1900 2950
Wire Wire Line
	1900 2950 1950 2950
$Comp
L Li-ion_Thesis_Symbol_Library:Si8602AB-B-IS U?
U 1 1 5F122864
P 1825 4700
AR Path="/5F122864" Ref="U?"  Part="1" 
AR Path="/5F11FE7E/5F122864" Ref="U5"  Part="1" 
F 0 "U5" H 3125 5100 60  0000 C CNN
F 1 "Si8600AC-B-IS" H 3425 5000 60  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:Si8602AB-B-IS" H 3025 4940 60  0001 C CNN
F 3 "" H 1825 4700 60  0000 C CNN
F 4 "SI8600AC-B-ISRCT-ND" H 1825 4700 50  0001 C CNN "Digi-Key Part Number"
	1    1825 4700
	1    0    0    -1  
$EndComp
Text GLabel 4225 4800 2    50   BiDi ~ 0
I2C_Data_Ext
Text GLabel 4225 4900 2    50   BiDi ~ 0
I2C_Clock_Ext
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F12286C
P 4225 4700
AR Path="/5F12286C" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F12286C" Ref="#PWR058"  Part="1" 
F 0 "#PWR058" H 4225 4550 50  0001 C CNN
F 1 "+5V_External" H 4240 4873 50  0000 C CNN
F 2 "" H 4225 4700 50  0001 C CNN
F 3 "" H 4225 4700 50  0001 C CNN
	1    4225 4700
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F122872
P 4225 5000
AR Path="/5F122872" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122872" Ref="#PWR059"  Part="1" 
F 0 "#PWR059" H 4225 4750 50  0001 C CNN
F 1 "GND_External" H 4230 4827 50  0000 C CNN
F 2 "" H 4225 5000 50  0001 C CNN
F 3 "" H 4225 5000 50  0001 C CNN
	1    4225 5000
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F122878
P 1825 4700
AR Path="/5F122878" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122878" Ref="#PWR070"  Part="1" 
F 0 "#PWR070" H 1825 4550 50  0001 C CNN
F 1 "+3.3V_Internal" H 1840 4873 50  0000 C CNN
F 2 "" H 1825 4700 50  0001 C CNN
F 3 "" H 1825 4700 50  0001 C CNN
	1    1825 4700
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F12287E
P 1825 5000
AR Path="/5F12287E" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F12287E" Ref="#PWR071"  Part="1" 
F 0 "#PWR071" H 1825 4750 50  0001 C CNN
F 1 "GND_Internal" H 1830 4827 50  0000 C CNN
F 2 "" H 1825 5000 50  0001 C CNN
F 3 "" H 1825 5000 50  0001 C CNN
	1    1825 5000
	1    0    0    -1  
$EndComp
Text GLabel 1825 4800 0    50   BiDi ~ 0
I2C_Data_Int
Text GLabel 1825 4900 0    50   BiDi ~ 0
I2C_Clock_Int
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F122887
P 5800 2450
AR Path="/5F122887" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122887" Ref="#PWR073"  Part="1" 
F 0 "#PWR073" H 5800 2200 50  0001 C CNN
F 1 "GND_Internal" H 5805 2277 50  0000 C CNN
F 2 "" H 5800 2450 50  0001 C CNN
F 3 "" H 5800 2450 50  0001 C CNN
	1    5800 2450
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F12288D
P 1900 3000
AR Path="/5F12288D" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F12288D" Ref="#PWR061"  Part="1" 
F 0 "#PWR061" H 1900 2750 50  0001 C CNN
F 1 "GND_Internal" H 1905 2827 50  0000 C CNN
F 2 "" H 1900 3000 50  0001 C CNN
F 3 "" H 1900 3000 50  0001 C CNN
	1    1900 3000
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F122893
P 2800 2350
AR Path="/5F122893" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122893" Ref="#PWR065"  Part="1" 
F 0 "#PWR065" H 2800 2100 50  0001 C CNN
F 1 "GND_Internal" H 2805 2177 50  0000 C CNN
F 2 "" H 2800 2350 50  0001 C CNN
F 3 "" H 2800 2350 50  0001 C CNN
	1    2800 2350
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F122899
P 1850 1400
AR Path="/5F122899" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122899" Ref="#PWR060"  Part="1" 
F 0 "#PWR060" H 1850 1250 50  0001 C CNN
F 1 "+3.3V_Internal" H 1525 1550 50  0000 C CNN
F 2 "" H 1850 1400 50  0001 C CNN
F 3 "" H 1850 1400 50  0001 C CNN
	1    1850 1400
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 1800 5800 1900
Wire Wire Line
	5900 1900 5800 1900
Connection ~ 5800 1900
Wire Wire Line
	5800 1900 5800 2000
Text Notes 3050 5350 0    79   ~ 0
External
Text Notes 2525 5350 0    79   ~ 0
Internal
Wire Notes Line
	3025 5350 3025 4400
Wire Wire Line
	2800 2200 2800 2350
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F1228D9
P 1950 1400
AR Path="/5F1228D9" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1228D9" Ref="#PWR062"  Part="1" 
F 0 "#PWR062" H 1950 1150 50  0001 C CNN
F 1 "GND_Internal" H 1775 1225 50  0000 C CNN
F 2 "" H 1950 1400 50  0001 C CNN
F 3 "" H 1950 1400 50  0001 C CNN
	1    1950 1400
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5F1391C2
P 2425 6225
AR Path="/5F1391C2" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F1391C2" Ref="C?"  Part="1" 
AR Path="/5F11FE7E/5F1391C2" Ref="C17"  Part="1" 
F 0 "C17" H 2540 6271 50  0000 L CNN
F 1 "0.1uF" H 2540 6180 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2463 6075 50  0001 C CNN
F 3 "~" H 2425 6225 50  0001 C CNN
F 4 "1276-1043-1-ND" H 2425 6225 50  0001 C CNN "Digi-Key Part Number"
	1    2425 6225
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F1391C8
P 2425 6375
AR Path="/5F1391C8" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F1391C8" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1391C8" Ref="#PWR064"  Part="1" 
F 0 "#PWR064" H 2425 6125 50  0001 C CNN
F 1 "GND_External" H 2430 6202 50  0000 C CNN
F 2 "" H 2425 6375 50  0001 C CNN
F 3 "" H 2425 6375 50  0001 C CNN
	1    2425 6375
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F1391CE
P 2425 6075
AR Path="/5F1391CE" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F1391CE" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1391CE" Ref="#PWR063"  Part="1" 
F 0 "#PWR063" H 2425 5925 50  0001 C CNN
F 1 "+5V_External" H 2440 6248 50  0000 C CNN
F 2 "" H 2425 6075 50  0001 C CNN
F 3 "" H 2425 6075 50  0001 C CNN
	1    2425 6075
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F1391D4
P 1675 6200
AR Path="/5F1391D4" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F1391D4" Ref="C?"  Part="1" 
AR Path="/5F11FE7E/5F1391D4" Ref="C18"  Part="1" 
F 0 "C18" H 1790 6246 50  0000 L CNN
F 1 "0.1uF" H 1790 6155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 1713 6050 50  0001 C CNN
F 3 "~" H 1675 6200 50  0001 C CNN
F 4 "1276-1043-1-ND" H 1675 6200 50  0001 C CNN "Digi-Key Part Number"
	1    1675 6200
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F1391DA
P 1675 6050
AR Path="/5F1391DA" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F1391DA" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1391DA" Ref="#PWR066"  Part="1" 
F 0 "#PWR066" H 1675 5900 50  0001 C CNN
F 1 "+3.3V_Internal" H 1690 6223 50  0000 C CNN
F 2 "" H 1675 6050 50  0001 C CNN
F 3 "" H 1675 6050 50  0001 C CNN
	1    1675 6050
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F1391E0
P 1675 6350
AR Path="/5F1391E0" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F1391E0" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1391E0" Ref="#PWR067"  Part="1" 
F 0 "#PWR067" H 1675 6100 50  0001 C CNN
F 1 "GND_Internal" H 1680 6177 50  0000 C CNN
F 2 "" H 1675 6350 50  0001 C CNN
F 3 "" H 1675 6350 50  0001 C CNN
	1    1675 6350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F1391E6
P 3725 2325
AR Path="/5F1391E6" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F1391E6" Ref="C?"  Part="1" 
AR Path="/5F11FE7E/5F1391E6" Ref="C19"  Part="1" 
F 0 "C19" H 3840 2371 50  0000 L CNN
F 1 "0.1uF" H 3840 2280 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3763 2175 50  0001 C CNN
F 3 "~" H 3725 2325 50  0001 C CNN
F 4 "1276-1043-1-ND" H 3725 2325 50  0001 C CNN "Digi-Key Part Number"
	1    3725 2325
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F1391EC
P 3725 2175
AR Path="/5F1391EC" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F1391EC" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1391EC" Ref="#PWR068"  Part="1" 
F 0 "#PWR068" H 3725 2025 50  0001 C CNN
F 1 "+3.3V_Internal" H 3740 2348 50  0000 C CNN
F 2 "" H 3725 2175 50  0001 C CNN
F 3 "" H 3725 2175 50  0001 C CNN
	1    3725 2175
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F1391F2
P 3725 2475
AR Path="/5F1391F2" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F1391F2" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1391F2" Ref="#PWR069"  Part="1" 
F 0 "#PWR069" H 3725 2225 50  0001 C CNN
F 1 "GND_Internal" H 3730 2302 50  0000 C CNN
F 2 "" H 3725 2475 50  0001 C CNN
F 3 "" H 3725 2475 50  0001 C CNN
	1    3725 2475
	1    0    0    -1  
$EndComp
Text Notes 2300 5850 0    50   ~ 0
Si860x
Text Notes 1575 5825 0    50   ~ 0
Si860x
Text Notes 3975 1700 0    50   ~ 0
ACS7033
Wire Wire Line
	2550 2000 2650 2000
Wire Notes Line
	4875 3475 4875 750 
Wire Notes Line
	4875 750  875  750 
Wire Notes Line
	875  750  875  3475
Wire Notes Line
	875  3475 4875 3475
Text Notes 950  925  0    79   ~ 0
Current Sensor 
Text Notes 1000 4200 0    79   ~ 0
I2C Isolator 
Wire Notes Line
	6675 3275 6675 750 
Wire Notes Line
	6675 750  5025 750 
Wire Notes Line
	5025 750  5025 3275
Wire Notes Line
	5025 3275 6675 3275
Text Notes 5150 900  0    79   ~ 0
Battery Voltage Divider\n
$Comp
L Device:R R?
U 1 1 5F514825
P 3750 6225
AR Path="/5F514825" Ref="R?"  Part="1" 
AR Path="/5F11FE7E/5F514825" Ref="R2"  Part="1" 
AR Path="/5F112C11/5F514825" Ref="R?"  Part="1" 
F 0 "R2" H 3820 6271 50  0000 L CNN
F 1 "1.5K" H 3820 6180 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3680 6225 50  0001 C CNN
F 3 "~" H 3750 6225 50  0001 C CNN
F 4 "RMCF0805JT3K00CT-ND" H 3750 6225 50  0001 C CNN "Digi-Key Part Number"
	1    3750 6225
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F51482C
P 4425 6225
AR Path="/5F51482C" Ref="R?"  Part="1" 
AR Path="/5F11FE7E/5F51482C" Ref="R3"  Part="1" 
AR Path="/5F112C11/5F51482C" Ref="R?"  Part="1" 
F 0 "R3" H 4495 6271 50  0000 L CNN
F 1 "1.5K" H 4495 6180 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4355 6225 50  0001 C CNN
F 3 "~" H 4425 6225 50  0001 C CNN
F 4 "RMCF0805JT3K00CT-ND" H 4425 6225 50  0001 C CNN "Digi-Key Part Number"
	1    4425 6225
	1    0    0    -1  
$EndComp
Text GLabel 3750 6525 3    50   BiDi ~ 0
I2C_Data_Int
Text GLabel 4425 6525 3    50   BiDi ~ 0
I2C_Clock_Int
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F514834
P 3750 5875
AR Path="/5F514834" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F514834" Ref="#PWR030"  Part="1" 
AR Path="/5F112C11/5F514834" Ref="#PWR?"  Part="1" 
F 0 "#PWR030" H 3750 5725 50  0001 C CNN
F 1 "+3.3V_Internal" H 3765 6048 50  0000 C CNN
F 2 "" H 3750 5875 50  0001 C CNN
F 3 "" H 3750 5875 50  0001 C CNN
	1    3750 5875
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F51483A
P 4425 5875
AR Path="/5F51483A" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F51483A" Ref="#PWR031"  Part="1" 
AR Path="/5F112C11/5F51483A" Ref="#PWR?"  Part="1" 
F 0 "#PWR031" H 4425 5725 50  0001 C CNN
F 1 "+3.3V_Internal" H 4440 6048 50  0000 C CNN
F 2 "" H 4425 5875 50  0001 C CNN
F 3 "" H 4425 5875 50  0001 C CNN
	1    4425 5875
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 6525 3750 6375
Wire Wire Line
	3750 6075 3750 5875
Wire Wire Line
	4425 5875 4425 6075
Wire Wire Line
	4425 6375 4425 6525
Wire Notes Line
	5250 4025 5250 7425
Wire Notes Line
	5250 7425 925  7425
Wire Notes Line
	925  4025 5250 4025
Wire Notes Line
	925  4025 925  7425
$Comp
L Device:C C?
U 1 1 5F58AA18
P 4425 2325
AR Path="/5F58AA18" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F58AA18" Ref="C?"  Part="1" 
AR Path="/5F11FE7E/5F58AA18" Ref="C29"  Part="1" 
F 0 "C29" H 4540 2371 50  0000 L CNN
F 1 "2.2uF" H 4540 2280 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4463 2175 50  0001 C CNN
F 3 "~" H 4425 2325 50  0001 C CNN
F 4 "1276-1043-1-ND" H 4425 2325 50  0001 C CNN "Digi-Key Part Number"
	1    4425 2325
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F58AA22
P 4425 2175
AR Path="/5F58AA22" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F58AA22" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F58AA22" Ref="#PWR098"  Part="1" 
F 0 "#PWR098" H 4425 2025 50  0001 C CNN
F 1 "+3.3V_Internal" H 4440 2348 50  0000 C CNN
F 2 "" H 4425 2175 50  0001 C CNN
F 3 "" H 4425 2175 50  0001 C CNN
	1    4425 2175
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F58AA2C
P 4425 2475
AR Path="/5F58AA2C" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F58AA2C" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F58AA2C" Ref="#PWR099"  Part="1" 
F 0 "#PWR099" H 4425 2225 50  0001 C CNN
F 1 "GND_Internal" H 4430 2302 50  0000 C CNN
F 2 "" H 4425 2475 50  0001 C CNN
F 3 "" H 4425 2475 50  0001 C CNN
	1    4425 2475
	1    0    0    -1  
$EndComp
$EndSCHEMATC
