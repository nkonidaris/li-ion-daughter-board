EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 6
Title "CPU"
Date "2020-07-29"
Rev "2"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1650 1400 0    50   Input ~ 0
VBat
Text GLabel 1650 2100 0    50   BiDi ~ 0
I2C_Clock_Int
Text GLabel 1650 2200 0    50   BiDi ~ 0
I2C_Data_Int
Text GLabel 5250 1500 2    50   Input ~ 0
VCurrent
Text GLabel 1650 2000 0    50   Output ~ 0
FET_1_EN
Text GLabel 1650 1700 0    50   Output ~ 0
FET_1_APWM
Text GLabel 1650 1900 0    50   Output ~ 0
FET_1_BPWM
Text GLabel 5250 1300 2    50   Output ~ 0
FET_2_EN
Text GLabel 1650 1300 0    50   Input ~ 0
VTemp
Text GLabel 5250 1400 2    50   Output ~ 0
FET_2_BPWM
Text GLabel 5250 1600 2    50   Output ~ 0
FET_2_APWM
Text GLabel 1650 2400 0    50   BiDi ~ 0
SYS_SWCLK
Text GLabel 1650 2300 0    50   BiDi ~ 0
SYS_SWDIO
$Comp
L Jumper:SolderJumper_2_Open JP?
U 1 1 5F116381
P 1150 3550
AR Path="/5F116381" Ref="JP?"  Part="1" 
AR Path="/5F112C11/5F116381" Ref="JP1"  Part="1" 
F 0 "JP1" V 1104 3618 50  0000 L CNN
F 1 "NRST Jumper" V 650 3300 50  0000 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 1150 3550 50  0001 C CNN
F 3 "~" H 1150 3550 50  0001 C CNN
F 4 "N/A" H 1150 3550 50  0001 C CNN "Digi-Key Part Number"
	1    1150 3550
	0    1    1    0   
$EndComp
$Comp
L Device:LED D?
U 1 1 5F116387
P 7000 2075
AR Path="/5F116387" Ref="D?"  Part="1" 
AR Path="/5F112C11/5F116387" Ref="D1"  Part="1" 
F 0 "D1" H 6993 2291 50  0000 C CNN
F 1 "LED" H 6993 2200 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 7000 2075 50  0001 C CNN
F 3 "~" H 7000 2075 50  0001 C CNN
F 4 "732-4983-1-ND" H 7000 2075 50  0001 C CNN "Digi-Key Part Number"
	1    7000 2075
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:STM32G030F6P6 U?
U 1 1 5F11638F
P 1650 1300
AR Path="/5F11638F" Ref="U?"  Part="1" 
AR Path="/5F112C11/5F11638F" Ref="U3"  Part="1" 
F 0 "U3" H 3450 1687 60  0000 C CNN
F 1 "STM32G030F6P6" H 3450 1581 60  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 3450 1540 60  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/stm32g030f6.pdf" H 1650 1300 60  0001 C CNN
F 4 "1.782" H -3950 100 50  0001 C CNN "Cost (A$)"
F 5 "STM32G030F6P6-ND" H -3950 100 50  0001 C CNN "Digi-Key Part Number"
	1    1650 1300
	1    0    0    -1  
$EndComp
Text GLabel 5250 2000 2    50   BiDi ~ 0
NRST
Text GLabel 1150 3400 1    50   BiDi ~ 0
NRST
Text GLabel 1650 1800 0    50   Output ~ 0
LED
Text GLabel 7000 1625 1    50   Output ~ 0
LED
$Comp
L Device:R R?
U 1 1 5F116399
P 7000 1775
AR Path="/5F116399" Ref="R?"  Part="1" 
AR Path="/5F112C11/5F116399" Ref="R1"  Part="1" 
F 0 "R1" H 7070 1821 50  0000 L CNN
F 1 "51" H 7070 1730 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6930 1775 50  0001 C CNN
F 3 "~" H 7000 1775 50  0001 C CNN
F 4 "RMCF0805JT51R0CT-ND" H 7000 1775 50  0001 C CNN "Digi-Key Part Number"
	1    7000 1775
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F11639F
P 5250 2200
AR Path="/5F11639F" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F11639F" Ref="#PWR025"  Part="1" 
F 0 "#PWR025" H 5250 2050 50  0001 C CNN
F 1 "+3.3V_Internal" V 5265 2328 50  0000 L CNN
F 2 "" H 5250 2200 50  0001 C CNN
F 3 "" H 5250 2200 50  0001 C CNN
	1    5250 2200
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F1163A5
P 5250 2400
AR Path="/5F1163A5" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F1163A5" Ref="#PWR026"  Part="1" 
F 0 "#PWR026" H 5250 2150 50  0001 C CNN
F 1 "GND_Internal" V 5255 2272 50  0000 R CNN
F 2 "" H 5250 2400 50  0001 C CNN
F 3 "" H 5250 2400 50  0001 C CNN
	1    5250 2400
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F1163AB
P 7000 2225
AR Path="/5F1163AB" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F1163AB" Ref="#PWR029"  Part="1" 
F 0 "#PWR029" H 7000 1975 50  0001 C CNN
F 1 "GND_Internal" H 7005 2052 50  0000 C CNN
F 2 "" H 7000 2225 50  0001 C CNN
F 3 "" H 7000 2225 50  0001 C CNN
	1    7000 2225
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F1163B1
P 1150 3700
AR Path="/5F1163B1" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F1163B1" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 1150 3450 50  0001 C CNN
F 1 "GND_Internal" H 1155 3527 50  0000 C CNN
F 2 "" H 1150 3700 50  0001 C CNN
F 3 "" H 1150 3700 50  0001 C CNN
	1    1150 3700
	1    0    0    -1  
$EndComp
Text GLabel 1650 1600 0    50   BiDi ~ 0
UARTRX
Text GLabel 1650 1500 0    50   BiDi ~ 0
UARTTX
$Comp
L Device:C C?
U 1 1 5F1163B9
P 1975 3550
AR Path="/5F1163B9" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F1163B9" Ref="C9"  Part="1" 
F 0 "C9" H 2090 3596 50  0000 L CNN
F 1 "0.1uF" H 2090 3505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2013 3400 50  0001 C CNN
F 3 "~" H 1975 3550 50  0001 C CNN
F 4 "1276-1043-1-ND" H 1975 3550 50  0001 C CNN "Digi-Key Part Number"
	1    1975 3550
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F1163BF
P 1975 3700
AR Path="/5F1163BF" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F1163BF" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 1975 3450 50  0001 C CNN
F 1 "GND_Internal" H 1980 3527 50  0000 C CNN
F 2 "" H 1975 3700 50  0001 C CNN
F 3 "" H 1975 3700 50  0001 C CNN
	1    1975 3700
	1    0    0    -1  
$EndComp
Text GLabel 2050 3225 2    50   Input ~ 0
VTemp
$Comp
L Connector:TestPoint TP?
U 1 1 5F1163C6
P 5250 1800
AR Path="/5F1163C6" Ref="TP?"  Part="1" 
AR Path="/5F112C11/5F1163C6" Ref="TP6"  Part="1" 
F 0 "TP6" H 5250 2025 50  0000 C CNN
F 1 "TestPoint" V 5250 2300 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 5450 1800 50  0001 C CNN
F 3 "~" H 5450 1800 50  0001 C CNN
F 4 "N/A" H 5250 1800 50  0001 C CNN "Digi-Key Part Number"
	1    5250 1800
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5F143339
P 8075 1975
AR Path="/5F143339" Ref="R?"  Part="1" 
AR Path="/5F11FE7E/5F143339" Ref="R?"  Part="1" 
AR Path="/5F112C11/5F143339" Ref="R2"  Part="1" 
F 0 "R2" H 8145 2021 50  0000 L CNN
F 1 "3K" H 8145 1930 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8005 1975 50  0001 C CNN
F 3 "~" H 8075 1975 50  0001 C CNN
F 4 "RMCF0805JT3K00CT-ND" H 8075 1975 50  0001 C CNN "Digi-Key Part Number"
	1    8075 1975
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F14333F
P 8750 1975
AR Path="/5F14333F" Ref="R?"  Part="1" 
AR Path="/5F11FE7E/5F14333F" Ref="R?"  Part="1" 
AR Path="/5F112C11/5F14333F" Ref="R3"  Part="1" 
F 0 "R3" H 8820 2021 50  0000 L CNN
F 1 "3K" H 8820 1930 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 8680 1975 50  0001 C CNN
F 3 "~" H 8750 1975 50  0001 C CNN
F 4 "RMCF0805JT3K00CT-ND" H 8750 1975 50  0001 C CNN "Digi-Key Part Number"
	1    8750 1975
	1    0    0    -1  
$EndComp
Text GLabel 8075 2275 3    50   BiDi ~ 0
I2C_Data_Int
Text GLabel 8750 2275 3    50   BiDi ~ 0
I2C_Clock_Int
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F143347
P 8075 1625
AR Path="/5F143347" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F143347" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F143347" Ref="#PWR030"  Part="1" 
F 0 "#PWR030" H 8075 1475 50  0001 C CNN
F 1 "+3.3V_Internal" H 8090 1798 50  0000 C CNN
F 2 "" H 8075 1625 50  0001 C CNN
F 3 "" H 8075 1625 50  0001 C CNN
	1    8075 1625
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F14334D
P 8750 1625
AR Path="/5F14334D" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F14334D" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F14334D" Ref="#PWR031"  Part="1" 
F 0 "#PWR031" H 8750 1475 50  0001 C CNN
F 1 "+3.3V_Internal" H 8765 1798 50  0000 C CNN
F 2 "" H 8750 1625 50  0001 C CNN
F 3 "" H 8750 1625 50  0001 C CNN
	1    8750 1625
	1    0    0    -1  
$EndComp
Wire Wire Line
	8075 2275 8075 2125
Wire Wire Line
	8075 1825 8075 1625
Wire Wire Line
	8750 1625 8750 1825
Wire Wire Line
	8750 2125 8750 2275
$Comp
L Device:C C?
U 1 1 5F143357
P 2725 3450
AR Path="/5F143357" Ref="C?"  Part="1" 
AR Path="/5F11FE7E/5F143357" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F143357" Ref="C10"  Part="1" 
F 0 "C10" H 2840 3496 50  0000 L CNN
F 1 "0.1uF" H 2840 3405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2763 3300 50  0001 C CNN
F 3 "~" H 2725 3450 50  0001 C CNN
F 4 "1276-1043-1-ND" H 2725 3450 50  0001 C CNN "Digi-Key Part Number"
	1    2725 3450
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F14335D
P 2725 3600
AR Path="/5F14335D" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F14335D" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F14335D" Ref="#PWR021"  Part="1" 
F 0 "#PWR021" H 2725 3350 50  0001 C CNN
F 1 "GND_Internal" H 2730 3427 50  0000 C CNN
F 2 "" H 2725 3600 50  0001 C CNN
F 3 "" H 2725 3600 50  0001 C CNN
	1    2725 3600
	1    0    0    -1  
$EndComp
Text GLabel 2750 3225 2    50   Output ~ 0
VBat
Wire Wire Line
	2750 3225 2725 3225
Wire Wire Line
	2725 3225 2725 3300
$Comp
L Device:C C?
U 1 1 5F143366
P 3325 3475
AR Path="/5F143366" Ref="C?"  Part="1" 
AR Path="/5F11FE7E/5F143366" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F143366" Ref="C11"  Part="1" 
F 0 "C11" H 3440 3521 50  0000 L CNN
F 1 "0.1uF" H 3440 3430 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3363 3325 50  0001 C CNN
F 3 "~" H 3325 3475 50  0001 C CNN
F 4 "1276-1043-1-ND" H 3325 3475 50  0001 C CNN "Digi-Key Part Number"
	1    3325 3475
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F14336C
P 3325 3625
AR Path="/5F14336C" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F14336C" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F14336C" Ref="#PWR022"  Part="1" 
F 0 "#PWR022" H 3325 3375 50  0001 C CNN
F 1 "GND_Internal" H 3330 3452 50  0000 C CNN
F 2 "" H 3325 3625 50  0001 C CNN
F 3 "" H 3325 3625 50  0001 C CNN
	1    3325 3625
	1    0    0    -1  
$EndComp
Text GLabel 3375 3250 2    50   Output ~ 0
VCurrent
Wire Wire Line
	3375 3250 3325 3250
Wire Wire Line
	3325 3250 3325 3325
$Comp
L Device:C C?
U 1 1 5F146429
P 4850 3675
AR Path="/5F146429" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F146429" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F146429" Ref="C12"  Part="1" 
F 0 "C12" H 4965 3721 50  0000 L CNN
F 1 "0.1uF" H 4965 3630 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4888 3525 50  0001 C CNN
F 3 "~" H 4850 3675 50  0001 C CNN
F 4 "1276-1043-1-ND" H 4850 3675 50  0001 C CNN "Digi-Key Part Number"
	1    4850 3675
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F14642F
P 4850 3525
AR Path="/5F14642F" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F14642F" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F14642F" Ref="#PWR023"  Part="1" 
F 0 "#PWR023" H 4850 3375 50  0001 C CNN
F 1 "+3.3V_Internal" H 4865 3698 50  0000 C CNN
F 2 "" H 4850 3525 50  0001 C CNN
F 3 "" H 4850 3525 50  0001 C CNN
	1    4850 3525
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F146435
P 4850 3825
AR Path="/5F146435" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F146435" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F146435" Ref="#PWR024"  Part="1" 
F 0 "#PWR024" H 4850 3575 50  0001 C CNN
F 1 "GND_Internal" H 4855 3652 50  0000 C CNN
F 2 "" H 4850 3825 50  0001 C CNN
F 3 "" H 4850 3825 50  0001 C CNN
	1    4850 3825
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F14643B
P 5500 3725
AR Path="/5F14643B" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F14643B" Ref="C?"  Part="1" 
AR Path="/5F112C11/5F14643B" Ref="C13"  Part="1" 
F 0 "C13" V 5750 3775 50  0000 L CNN
F 1 "4.7uF" V 5650 3650 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5538 3575 50  0001 C CNN
F 3 "~" H 5500 3725 50  0001 C CNN
F 4 "1276-1244-1-ND" H 5500 3725 50  0001 C CNN "Digi-Key Part Number"
	1    5500 3725
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F146441
P 5500 3575
AR Path="/5F146441" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F146441" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F146441" Ref="#PWR027"  Part="1" 
F 0 "#PWR027" H 5500 3425 50  0001 C CNN
F 1 "+3.3V_Internal" H 5500 3800 50  0000 C CNN
F 2 "" H 5500 3575 50  0001 C CNN
F 3 "" H 5500 3575 50  0001 C CNN
	1    5500 3575
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F146447
P 5500 3875
AR Path="/5F146447" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F146447" Ref="#PWR?"  Part="1" 
AR Path="/5F112C11/5F146447" Ref="#PWR028"  Part="1" 
F 0 "#PWR028" H 5500 3625 50  0001 C CNN
F 1 "GND_Internal" H 5500 3725 50  0000 C CNN
F 2 "" H 5500 3875 50  0001 C CNN
F 3 "" H 5500 3875 50  0001 C CNN
	1    5500 3875
	1    0    0    -1  
$EndComp
Text Notes 5300 3225 2    50   ~ 0
STM32
Wire Wire Line
	2050 3225 1975 3225
Wire Wire Line
	1975 3225 1975 3400
$EndSCHEMATC
