EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 6
Title "Power Management "
Date "2020-07-29"
Rev "2"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Li-ion_Thesis_Symbol_Library:AZ1117-3.3 IC?
U 1 1 5F119AA5
P 1900 3950
AR Path="/5F119AA5" Ref="IC?"  Part="1" 
AR Path="/5F116BFA/5F119AA5" Ref="IC3"  Part="1" 
F 0 "IC3" H 2000 4200 50  0000 C CNN
F 1 "ZLDO1117G33TA" H 2050 4100 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:Voltage_Regulator_SOT-223" H 3250 4050 50  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/AZ1117.pdf" H 3250 3950 50  0001 L CNN
F 4 "0.493" H 1900 3950 50  0001 C CNN "Cost (A$)"
F 5 "AZ1117IH-3.3TRG1DICT-ND" H 1900 3950 50  0001 C CNN "Digi-Key Part Number"
F 6 "AZ1117IH-3.3TRG1" H 1900 3950 50  0001 C CNN "Manufacturer Part Number"
	1    1900 3950
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F119AAB
P 2450 4100
AR Path="/5F119AAB" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AAB" Ref="#PWR041"  Part="1" 
F 0 "#PWR041" H 2450 3950 50  0001 C CNN
F 1 "+3.3V_Internal" V 2465 4228 50  0000 L CNN
F 2 "" H 2450 4100 50  0001 C CNN
F 3 "" H 2450 4100 50  0001 C CNN
	1    2450 4100
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F119AB1
P 1900 4250
AR Path="/5F119AB1" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AB1" Ref="#PWR039"  Part="1" 
F 0 "#PWR039" H 1900 4000 50  0001 C CNN
F 1 "GND_Internal" H 1905 4077 50  0000 C CNN
F 2 "" H 1900 4250 50  0001 C CNN
F 3 "" H 1900 4250 50  0001 C CNN
	1    1900 4250
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR?
U 1 1 5F119AC9
P 1600 3950
AR Path="/5F119AC9" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AC9" Ref="#PWR034"  Part="1" 
F 0 "#PWR034" H 1600 3800 50  0001 C CNN
F 1 "+9V_Internal" V 1615 4077 50  0000 L CNN
F 2 "" H 1600 3950 50  0001 C CNN
F 3 "" H 1600 3950 50  0001 C CNN
	1    1600 3950
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F119AE8
P 1700 1850
AR Path="/5F119AE8" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AE8" Ref="#PWR035"  Part="1" 
F 0 "#PWR035" H 1700 1700 50  0001 C CNN
F 1 "+5V_External" V 1700 2250 50  0000 C CNN
F 2 "" H 1700 1850 50  0001 C CNN
F 3 "" H 1700 1850 50  0001 C CNN
	1    1700 1850
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR?
U 1 1 5F119AEE
P 2700 1850
AR Path="/5F119AEE" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AEE" Ref="#PWR042"  Part="1" 
F 0 "#PWR042" H 2700 1700 50  0001 C CNN
F 1 "+9V_Internal" V 2715 1977 50  0000 L CNN
F 2 "" H 2700 1850 50  0001 C CNN
F 3 "" H 2700 1850 50  0001 C CNN
	1    2700 1850
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F119AF4
P 1700 2000
AR Path="/5F119AF4" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AF4" Ref="#PWR036"  Part="1" 
F 0 "#PWR036" H 1700 1750 50  0001 C CNN
F 1 "GND_External" V 1700 1600 50  0000 C CNN
F 2 "" H 1700 2000 50  0001 C CNN
F 3 "" H 1700 2000 50  0001 C CNN
	1    1700 2000
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F119AFA
P 2700 2000
AR Path="/5F119AFA" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119AFA" Ref="#PWR043"  Part="1" 
F 0 "#PWR043" H 2700 1750 50  0001 C CNN
F 1 "GND_Internal" V 2700 1600 50  0000 C CNN
F 2 "" H 2700 2000 50  0001 C CNN
F 3 "" H 2700 2000 50  0001 C CNN
	1    2700 2000
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:PDSE1-S5-S9-S U?
U 1 1 5F119B03
P 2200 1900
AR Path="/5F119B03" Ref="U?"  Part="1" 
AR Path="/5F116BFA/5F119B03" Ref="U4"  Part="1" 
F 0 "U4" H 2200 2215 50  0000 C CNN
F 1 "PDSE1-S5-S9-S" H 2200 2124 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:CONV_PDSE1-S5-S9-S" H 2200 2350 50  0001 C CNN
F 3 "https://www.xppower.com/portals/0/pdfs/SF_IES01.pdf" H 2200 2350 50  0001 C CNN
F 4 "102-6292-ND" H 2200 1900 50  0001 C CNN "Digi-Key Part Number"
	1    2200 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F119B09
P 2800 2925
AR Path="/5F119B09" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F119B09" Ref="C16"  Part="1" 
F 0 "C16" H 2915 2971 50  0000 L CNN
F 1 "47uF" H 2915 2880 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.4" H 2838 2775 50  0001 C CNN
F 3 "~" H 2800 2925 50  0001 C CNN
F 4 "732-8416-1-ND" H 2800 2925 50  0001 C CNN "Digi-Key Part Number"
	1    2800 2925
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F119B0F
P 2800 3075
AR Path="/5F119B0F" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B0F" Ref="#PWR045"  Part="1" 
F 0 "#PWR045" H 2800 2825 50  0001 C CNN
F 1 "GND_Internal" H 2805 2902 50  0000 C CNN
F 2 "" H 2800 3075 50  0001 C CNN
F 3 "" H 2800 3075 50  0001 C CNN
	1    2800 3075
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5F119B15
P 2800 2775
AR Path="/5F119B15" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B15" Ref="#PWR044"  Part="1" 
F 0 "#PWR044" H 2800 2625 50  0001 C CNN
F 1 "+BATT" H 2650 2925 50  0000 L CNN
F 2 "" H 2800 2775 50  0001 C CNN
F 3 "" H 2800 2775 50  0001 C CNN
	1    2800 2775
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F119B1B
P 2450 3950
AR Path="/5F119B1B" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B1B" Ref="#PWR040"  Part="1" 
F 0 "#PWR040" H 2450 3800 50  0001 C CNN
F 1 "+3.3V_Internal" V 2465 4078 50  0000 L CNN
F 2 "" H 2450 3950 50  0001 C CNN
F 3 "" H 2450 3950 50  0001 C CNN
	1    2450 3950
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 5F119B21
P 1225 2900
AR Path="/5F119B21" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F119B21" Ref="C14"  Part="1" 
F 0 "C14" H 1340 2946 50  0000 L CNN
F 1 "22uF" H 1340 2855 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.4" H 1263 2750 50  0001 C CNN
F 3 "~" H 1225 2900 50  0001 C CNN
F 4 "732-8432-1-ND" H 1225 2900 50  0001 C CNN "Digi-Key Part Number"
	1    1225 2900
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F119B27
P 1225 3050
AR Path="/5F119B27" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B27" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 1225 2800 50  0001 C CNN
F 1 "GND_External" H 1230 2877 50  0000 C CNN
F 2 "" H 1225 3050 50  0001 C CNN
F 3 "" H 1225 3050 50  0001 C CNN
	1    1225 3050
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F119B2D
P 1225 2750
AR Path="/5F119B2D" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B2D" Ref="#PWR032"  Part="1" 
F 0 "#PWR032" H 1225 2600 50  0001 C CNN
F 1 "+5V_External" H 1240 2923 50  0000 C CNN
F 2 "" H 1225 2750 50  0001 C CNN
F 3 "" H 1225 2750 50  0001 C CNN
	1    1225 2750
	1    0    0    -1  
$EndComp
Text Notes 1450 2450 0    50   ~ 0
PDSE1
$Comp
L Device:C C?
U 1 1 5F119B84
P 1850 2925
AR Path="/5F119B84" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F119B84" Ref="C15"  Part="1" 
F 0 "C15" V 2050 2950 50  0000 L CNN
F 1 "4.7uF" V 1975 2675 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1888 2775 50  0001 C CNN
F 3 "~" H 1850 2925 50  0001 C CNN
F 4 "1276-1244-1-ND" H 1850 2925 50  0001 C CNN "Digi-Key Part Number"
	1    1850 2925
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F119B8A
P 1850 3075
AR Path="/5F119B8A" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B8A" Ref="#PWR038"  Part="1" 
F 0 "#PWR038" H 1850 2825 50  0001 C CNN
F 1 "GND_External" H 1900 2900 50  0000 C CNN
F 2 "" H 1850 3075 50  0001 C CNN
F 3 "" H 1850 3075 50  0001 C CNN
	1    1850 3075
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F119B90
P 1850 2775
AR Path="/5F119B90" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F119B90" Ref="#PWR037"  Part="1" 
F 0 "#PWR037" H 1850 2625 50  0001 C CNN
F 1 "+5V_External" H 1850 2975 50  0000 C CNN
F 2 "" H 1850 2775 50  0001 C CNN
F 3 "" H 1850 2775 50  0001 C CNN
	1    1850 2775
	1    0    0    -1  
$EndComp
Wire Notes Line
	3750 6350 3725 6350
$EndSCHEMATC
