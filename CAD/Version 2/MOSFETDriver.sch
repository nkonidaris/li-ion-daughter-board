EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title "MOSFET Driver"
Date "2020-07-29"
Rev "2"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1450 1275 0    50   Output ~ 0
FET_1_EN
Text GLabel 1450 1675 0    50   Output ~ 0
FET_1_APWM
Text GLabel 1450 1375 0    50   Output ~ 0
FET_1_BPWM
Text GLabel 4550 1325 0    50   Output ~ 0
FET_2_EN
Text GLabel 4550 1425 0    50   Output ~ 0
FET_2_BPWM
Text GLabel 4550 1725 0    50   Output ~ 0
FET_2_APWM
Text GLabel 2650 1175 2    50   Output ~ 0
BHO_1
Text GLabel 1350 3175 0    50   Input ~ 0
BHO_1
Text GLabel 2650 1375 2    50   Output ~ 0
BLO_1
Text GLabel 2650 1675 2    50   Output ~ 0
ALO_1
Text GLabel 2650 1875 2    50   Output ~ 0
AHO_1
Text GLabel 5750 1225 2    50   Output ~ 0
BHO_2
Text GLabel 5750 1425 2    50   Output ~ 0
BLO_2
Text GLabel 5750 1725 2    50   Output ~ 0
ALO_2
Text GLabel 5750 1925 2    50   Output ~ 0
AHO_2
Text GLabel 4550 1925 0    50   Output ~ 0
AHB_2
Text GLabel 4550 1225 0    50   Output ~ 0
BHB_2
Text GLabel 1450 1875 0    50   BiDi ~ 0
AHB_1
Text GLabel 1450 1175 0    50   BiDi ~ 0
BHB_1
Text GLabel 1350 3275 0    50   Input ~ 0
BLO_1
Text GLabel 1350 3375 0    50   Input ~ 0
ALO_1
Text GLabel 1350 3475 0    50   Input ~ 0
AHO_1
Text GLabel 4450 3175 0    50   Input ~ 0
BHO_2
Text GLabel 4450 3275 0    50   Input ~ 0
BLO_2
Text GLabel 4450 3375 0    50   Input ~ 0
ALO_2
Text GLabel 4450 3475 0    50   Input ~ 0
AHO_2
Wire Wire Line
	1350 3625 1300 3625
Wire Wire Line
	1300 3625 1300 3675
Wire Wire Line
	1300 3725 1350 3725
Wire Wire Line
	1300 3675 1250 3675
Connection ~ 1300 3675
Wire Wire Line
	1300 3675 1300 3725
Wire Wire Line
	2750 3425 2800 3425
Wire Wire Line
	2800 3425 2800 3525
Wire Wire Line
	2800 3775 2750 3775
Wire Wire Line
	2750 3675 2800 3675
Connection ~ 2800 3675
Wire Wire Line
	2800 3675 2800 3775
Wire Wire Line
	2750 3525 2800 3525
Connection ~ 2800 3525
Wire Wire Line
	4450 3625 4400 3625
Wire Wire Line
	4400 3625 4400 3675
Wire Wire Line
	4400 3725 4450 3725
Wire Wire Line
	4400 3675 4350 3675
Connection ~ 4400 3675
Wire Wire Line
	4400 3675 4400 3725
Wire Wire Line
	5850 3425 5900 3425
Wire Wire Line
	5900 3425 5900 3525
Wire Wire Line
	5900 3775 5850 3775
Wire Wire Line
	5850 3675 5900 3675
Connection ~ 5900 3675
Wire Wire Line
	5900 3675 5900 3775
Wire Wire Line
	5850 3525 5900 3525
Connection ~ 5900 3525
Wire Wire Line
	5900 3525 5900 3575
Wire Wire Line
	5950 3575 5900 3575
Connection ~ 5900 3575
Wire Wire Line
	5900 3575 5900 3675
Wire Wire Line
	2800 3525 2800 3575
Wire Wire Line
	2850 3575 2800 3575
Connection ~ 2800 3575
Wire Wire Line
	2800 3575 2800 3675
Text GLabel 5850 3175 2    50   BiDi ~ 0
Batt_Next_Inner
Text GLabel 2750 3275 2    50   BiDi ~ 0
Batt_Prev_Inner
Text GLabel 2650 1275 2    50   BiDi ~ 0
Batt_Prev_Inner
Text GLabel 5750 1825 2    50   BiDi ~ 0
Batt_Next_Inner
Text GLabel 5850 3275 2    50   BiDi ~ 0
Batt_Next_Outer
Text GLabel 5750 1325 2    50   BiDi ~ 0
Batt_Next_Outer
Text GLabel 2750 3175 2    50   BiDi ~ 0
Batt_Prev_Outer
Text GLabel 2650 1775 2    50   BiDi ~ 0
Batt_Prev_Outer
Text GLabel 1400 2675 0    50   BiDi ~ 0
AHB_1
Text GLabel 1700 2300 2    50   BiDi ~ 0
Batt_Prev_Inner
Text GLabel 1400 2300 0    50   BiDi ~ 0
BHB_1
Text GLabel 4675 2325 2    50   BiDi ~ 0
Batt_Next_Outer
Text GLabel 4375 2675 0    50   Output ~ 0
AHB_2
Text GLabel 4375 2325 0    50   Output ~ 0
BHB_2
$Comp
L power:+BATT #PWR?
U 1 1 5F0FE656
P 1250 3675
AR Path="/5F0FE656" Ref="#PWR?"  Part="1" 
AR Path="/5E874FE6/5F0FE656" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F0FE656" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 1250 3525 50  0001 C CNN
F 1 "+BATT" H 1265 3848 50  0000 C CNN
F 2 "" H 1250 3675 50  0001 C CNN
F 3 "" H 1250 3675 50  0001 C CNN
	1    1250 3675
	0    -1   -1   0   
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5F0FE65C
P 4350 3675
AR Path="/5F0FE65C" Ref="#PWR?"  Part="1" 
AR Path="/5E874FE6/5F0FE65C" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F0FE65C" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 4350 3525 50  0001 C CNN
F 1 "+BATT" H 4365 3848 50  0000 C CNN
F 2 "" H 4350 3675 50  0001 C CNN
F 3 "" H 4350 3675 50  0001 C CNN
	1    4350 3675
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:FDMQ86530L U2
U 1 1 5F0FE665
P 5150 3775
F 0 "U2" H 5150 4645 50  0000 C CNN
F 1 "FDMQ86530L" H 5150 4554 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:IC_FDMQ8205A" H 4850 4275 50  0001 L BNN
F 3 "ON Semiconductor" H 4850 4175 50  0001 L BNN
F 4 "FDMQ86530L" H 5150 3775 50  0001 C CNN "Manufacturer Part Number"
F 5 "FDMQ86530LCT-ND" H 5150 3775 50  0001 C CNN "Digi-Key Part Number"
F 6 "3.6576" H 3300 -3575 50  0001 C CNN "Cost (A$)"
	1    5150 3775
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:MIC4606-2YTS-TR IC1
U 1 1 5F0FE670
P 1450 1175
F 0 "IC1" H 2050 1440 50  0000 C CNN
F 1 "MIC4606-2YTS-TR" H 2050 1349 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:MIC4606_TSSOP-16_4.4x5mm_P0.65mm" H 2500 1275 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/mic4606-2yts-tr/microchip-technology" H 2500 1175 50  0001 L CNN
F 4 "MIC4606-2YTS-CT-ND" H 2500 1075 50  0001 L CNN "Digi-Key Part Number"
F 5 "1.2" H 2500 975 50  0001 L CNN "Height"
F 6 "Microchip" H 2500 675 50  0001 L CNN "Manufacturer Name"
F 7 "MIC4606-2YTS-TR" H 2500 575 50  0001 L CNN "Manufacturer Part Number"
F 8 "2.962" H -2500 -3125 50  0001 C CNN "Cost (A$)"
	1    1450 1175
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:FDMQ86530L U1
U 1 1 5F0FE679
P 2050 3775
F 0 "U1" H 2050 4645 50  0000 C CNN
F 1 "FDMQ86530L" H 2050 4554 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:IC_FDMQ8205A" H 1750 4275 50  0001 L BNN
F 3 "ON Semiconductor" H 1750 4175 50  0001 L BNN
F 4 "FDMQ86530L" H 2050 3775 50  0001 C CNN "Manufacturer Part Number"
F 5 "FDMQ86530LCT-ND" H 2050 3775 50  0001 C CNN "Digi-Key Part Number"
F 6 "3.6576" H -2500 -3575 50  0001 C CNN "Cost (A$)"
	1    2050 3775
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5F0FE67F
P 1550 2675
F 0 "C2" V 1400 2325 50  0000 L CNN
F 1 "4.7uF" V 1350 2575 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1588 2525 50  0001 C CNN
F 3 "~" H 1550 2675 50  0001 C CNN
F 4 "1276-1244-1-ND" H 1550 2675 50  0001 C CNN "Digi-Key Part Number"
	1    1550 2675
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 5F0FE685
P 1550 2300
F 0 "C1" V 1400 1950 50  0000 L CNN
F 1 "4.7uF" V 1350 2200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1588 2150 50  0001 C CNN
F 3 "~" H 1550 2300 50  0001 C CNN
F 4 "1276-1244-1-ND" H 1550 2300 50  0001 C CNN "Digi-Key Part Number"
	1    1550 2300
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 5F0FE68B
P 4525 2325
F 0 "C4" V 4375 1975 50  0000 L CNN
F 1 "4.7uF" V 4325 2225 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4563 2175 50  0001 C CNN
F 3 "~" H 4525 2325 50  0001 C CNN
F 4 "1276-1244-1-ND" H 4525 2325 50  0001 C CNN "Digi-Key Part Number"
	1    4525 2325
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:MIC4606-2YTS-TR IC2
U 1 1 5F0FE696
P 4550 1225
F 0 "IC2" H 5150 1490 50  0000 C CNN
F 1 "MIC4606-2YTS-TR" H 5150 1399 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:MIC4606_TSSOP-16_4.4x5mm_P0.65mm" H 5600 1325 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/mic4606-2yts-tr/microchip-technology" H 5600 1225 50  0001 L CNN
F 4 "MIC4606-2YTS-CT-ND" H 5600 1125 50  0001 L CNN "Digi-Key Part Number"
F 5 "1.2" H 5600 1025 50  0001 L CNN "Height"
F 6 "Microchip" H 5600 725 50  0001 L CNN "Manufacturer Name"
F 7 "MIC4606-2YTS-TR" H 5600 625 50  0001 L CNN "Manufacturer Part Number"
F 8 "2.962" H 3300 -3075 50  0001 C CNN "Cost (A$)"
	1    4550 1225
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5F0FE69C
P 4525 2675
F 0 "C5" V 4375 2325 50  0000 L CNN
F 1 "4.7uF" V 4325 2575 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4563 2525 50  0001 C CNN
F 3 "~" H 4525 2675 50  0001 C CNN
F 4 "1276-1244-1-ND" H 4525 2675 50  0001 C CNN "Digi-Key Part Number"
	1    4525 2675
	0    1    1    0   
$EndComp
Wire Notes Line
	850  725  850  4075
Wire Notes Line
	3900 725  3900 4075
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR014
U 1 1 5F0FE6A4
P 6050 1625
F 0 "#PWR014" H 6050 1475 50  0001 C CNN
F 1 "+9V_Internal" V 6100 1725 50  0000 L CNN
F 2 "" H 6050 1625 50  0001 C CNN
F 3 "" H 6050 1625 50  0001 C CNN
	1    6050 1625
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR013
U 1 1 5F0FE6AA
P 6050 1525
F 0 "#PWR013" H 6050 1275 50  0001 C CNN
F 1 "GND_Internal" V 6100 1425 50  0000 R CNN
F 2 "" H 6050 1525 50  0001 C CNN
F 3 "" H 6050 1525 50  0001 C CNN
	1    6050 1525
	0    -1   -1   0   
$EndComp
Wire Notes Line
	6750 725  6750 4075
Wire Notes Line
	3900 725  6750 725 
Wire Notes Line
	3900 4075 6750 4075
Wire Wire Line
	6050 1525 5750 1525
Wire Wire Line
	5750 1625 6050 1625
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR010
U 1 1 5F0FE6B5
P 5950 3575
F 0 "#PWR010" H 5950 3325 50  0001 C CNN
F 1 "GND_Internal" V 6000 3475 50  0000 R CNN
F 2 "" H 5950 3575 50  0001 C CNN
F 3 "" H 5950 3575 50  0001 C CNN
	1    5950 3575
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR06
U 1 1 5F0FE6BB
P 2850 3575
F 0 "#PWR06" H 2850 3325 50  0001 C CNN
F 1 "GND_Internal" V 2900 3475 50  0000 R CNN
F 2 "" H 2850 3575 50  0001 C CNN
F 3 "" H 2850 3575 50  0001 C CNN
	1    2850 3575
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR08
U 1 1 5F0FE6C1
P 2950 1575
F 0 "#PWR08" H 2950 1425 50  0001 C CNN
F 1 "+9V_Internal" V 3000 1675 50  0000 L CNN
F 2 "" H 2950 1575 50  0001 C CNN
F 3 "" H 2950 1575 50  0001 C CNN
	1    2950 1575
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR07
U 1 1 5F0FE6C7
P 2950 1475
F 0 "#PWR07" H 2950 1225 50  0001 C CNN
F 1 "GND_Internal" V 3000 1375 50  0000 R CNN
F 2 "" H 2950 1475 50  0001 C CNN
F 3 "" H 2950 1475 50  0001 C CNN
	1    2950 1475
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2950 1475 2650 1475
Wire Wire Line
	2650 1575 2950 1575
Wire Notes Line
	3600 725  3600 4075
Wire Notes Line
	850  4075 3600 4075
Wire Notes Line
	850  725  3600 725 
Text GLabel 4675 2675 2    50   BiDi ~ 0
Batt_Next_Inner
Text GLabel 1700 2675 2    50   BiDi ~ 0
Batt_Prev_Outer
Text Notes 3925 900  0    102  ~ 0
Next
Text Notes 875  900  0    102  ~ 0
Previous
Text Notes 3100 925  0    50   ~ 0
A = Outer\nB = Inner
Text Notes 6300 925  0    50   ~ 0
B = Outer\nA = Inner
$Comp
L Device:C C6
U 1 1 5F0FE6DA
P 6000 2500
F 0 "C6" H 6115 2546 50  0000 L CNN
F 1 "0.1uF" H 6115 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6038 2350 50  0001 C CNN
F 3 "~" H 6000 2500 50  0001 C CNN
F 4 "1276-1043-1-ND" H 6000 2500 50  0001 C CNN "Digi-Key Part Number"
	1    6000 2500
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR012
U 1 1 5F0FE6E0
P 6000 2650
F 0 "#PWR012" H 6000 2400 50  0001 C CNN
F 1 "GND_Internal" H 6005 2477 50  0000 C CNN
F 2 "" H 6000 2650 50  0001 C CNN
F 3 "" H 6000 2650 50  0001 C CNN
	1    6000 2650
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR011
U 1 1 5F0FE6E6
P 6000 2350
F 0 "#PWR011" H 6000 2200 50  0001 C CNN
F 1 "+9V_Internal" H 6050 2450 50  0000 L CNN
F 2 "" H 6000 2350 50  0001 C CNN
F 3 "" H 6000 2350 50  0001 C CNN
	1    6000 2350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5F0FE6EC
P 2850 2450
F 0 "C3" H 2965 2496 50  0000 L CNN
F 1 "0.1uF" H 2965 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2888 2300 50  0001 C CNN
F 3 "~" H 2850 2450 50  0001 C CNN
F 4 "1276-1043-1-ND" H 2850 2450 50  0001 C CNN "Digi-Key Part Number"
	1    2850 2450
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR05
U 1 1 5F0FE6F2
P 2850 2600
F 0 "#PWR05" H 2850 2350 50  0001 C CNN
F 1 "GND_Internal" H 2855 2427 50  0000 C CNN
F 2 "" H 2850 2600 50  0001 C CNN
F 3 "" H 2850 2600 50  0001 C CNN
	1    2850 2600
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR04
U 1 1 5F0FE6F8
P 2850 2300
F 0 "#PWR04" H 2850 2150 50  0001 C CNN
F 1 "+9V_Internal" H 2900 2400 50  0000 L CNN
F 2 "" H 2850 2300 50  0001 C CNN
F 3 "" H 2850 2300 50  0001 C CNN
	1    2850 2300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F14A4E0
P 8000 2300
AR Path="/5F14A4E0" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F14A4E0" Ref="C?"  Part="1" 
AR Path="/5F0F169D/5F14A4E0" Ref="C8"  Part="1" 
F 0 "C8" H 8115 2346 50  0000 L CNN
F 1 "4.7uF" H 8115 2255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 8038 2150 50  0001 C CNN
F 3 "~" H 8000 2300 50  0001 C CNN
F 4 "1276-1244-1-ND" H 8000 2300 50  0001 C CNN "Digi-Key Part Number"
	1    8000 2300
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F14A4E6
P 8000 2450
AR Path="/5F14A4E6" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F14A4E6" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F14A4E6" Ref="#PWR018"  Part="1" 
F 0 "#PWR018" H 8000 2200 50  0001 C CNN
F 1 "GND_Internal" H 8005 2277 50  0000 C CNN
F 2 "" H 8000 2450 50  0001 C CNN
F 3 "" H 8000 2450 50  0001 C CNN
	1    8000 2450
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR?
U 1 1 5F14A4EC
P 8000 2150
AR Path="/5F14A4EC" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F14A4EC" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F14A4EC" Ref="#PWR017"  Part="1" 
F 0 "#PWR017" H 8000 2000 50  0001 C CNN
F 1 "+9V_Internal" H 8015 2323 50  0000 C CNN
F 2 "" H 8000 2150 50  0001 C CNN
F 3 "" H 8000 2150 50  0001 C CNN
	1    8000 2150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F14A4F2
P 7450 2300
AR Path="/5F14A4F2" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F14A4F2" Ref="C?"  Part="1" 
AR Path="/5F0F169D/5F14A4F2" Ref="C7"  Part="1" 
F 0 "C7" H 7565 2346 50  0000 L CNN
F 1 "47uF" H 7565 2255 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_5x5.4" H 7488 2150 50  0001 C CNN
F 3 "~" H 7450 2300 50  0001 C CNN
F 4 "732-8416-1-ND" H 7450 2300 50  0001 C CNN "Digi-Key Part Number"
	1    7450 2300
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F14A4F8
P 7450 2450
AR Path="/5F14A4F8" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F14A4F8" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F14A4F8" Ref="#PWR016"  Part="1" 
F 0 "#PWR016" H 7450 2200 50  0001 C CNN
F 1 "GND_Internal" H 7455 2277 50  0000 C CNN
F 2 "" H 7450 2450 50  0001 C CNN
F 3 "" H 7450 2450 50  0001 C CNN
	1    7450 2450
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+9V_Internal #PWR?
U 1 1 5F14A4FE
P 7450 2150
AR Path="/5F14A4FE" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F14A4FE" Ref="#PWR?"  Part="1" 
AR Path="/5F0F169D/5F14A4FE" Ref="#PWR015"  Part="1" 
F 0 "#PWR015" H 7450 2000 50  0001 C CNN
F 1 "+9V_Internal" H 7465 2323 50  0000 C CNN
F 2 "" H 7450 2150 50  0001 C CNN
F 3 "" H 7450 2150 50  0001 C CNN
	1    7450 2150
	1    0    0    -1  
$EndComp
Text Notes 7875 1900 0    50   ~ 0
MIC4606
Text Notes 7325 1900 0    50   ~ 0
MIC4606\n22uF x2
$EndSCHEMATC
