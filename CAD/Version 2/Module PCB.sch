EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 6
Title "Daughter Board MMSPC"
Date "2020-07-29"
Rev "2"
Comp "Nicholas Konidaris"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 1050 4450 0    98   ~ 0
External Connectors
Text Notes 1200 5650 0    98   ~ 0
MOSFET Driver
Text Notes 1050 3300 0    98   ~ 0
External Peripherals
Text Notes 3600 3275 0    98   ~ 0
CPU + Jumper
$Comp
L Connector:TestPoint TP2
U 1 1 5EFC047C
P 3900 4650
F 0 "TP2" H 3900 4875 50  0000 C CNN
F 1 "TestPoint" V 3750 4600 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 4100 4650 50  0001 C CNN
F 3 "~" H 4100 4650 50  0001 C CNN
F 4 "N/A" H 3900 4650 50  0001 C CNN "Digi-Key Part Number"
	1    3900 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5EFC1C04
P 4275 4650
F 0 "TP3" H 4275 4875 50  0000 C CNN
F 1 "TestPoint" V 4150 4650 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 4475 4650 50  0001 C CNN
F 3 "~" H 4475 4650 50  0001 C CNN
F 4 "N/A" H 4275 4650 50  0001 C CNN "Digi-Key Part Number"
	1    4275 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5EFC38AC
P 4600 4650
F 0 "TP4" H 4600 4875 50  0000 C CNN
F 1 "TestPoint" V 4475 4650 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 4800 4650 50  0001 C CNN
F 3 "~" H 4800 4650 50  0001 C CNN
F 4 "N/A" H 4600 4650 50  0001 C CNN "Digi-Key Part Number"
	1    4600 4650
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR01
U 1 1 5EFC3A7A
P 4600 4650
F 0 "#PWR01" H 4600 4400 50  0001 C CNN
F 1 "GND_Internal" V 4600 4250 50  0000 C CNN
F 2 "" H 4600 4650 50  0001 C CNN
F 3 "" H 4600 4650 50  0001 C CNN
	1    4600 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP5
U 1 1 5EFE1341
P 4925 4650
F 0 "TP5" H 4925 4875 50  0000 C CNN
F 1 "TestPoint" V 4800 4650 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_2.0x2.0mm" H 5125 4650 50  0001 C CNN
F 3 "~" H 5125 4650 50  0001 C CNN
F 4 "N/A" H 4925 4650 50  0001 C CNN "Digi-Key Part Number"
	1    4925 4650
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR02
U 1 1 5EFE339A
P 4925 4650
F 0 "#PWR02" H 4925 4400 50  0001 C CNN
F 1 "GND_External" V 4925 4250 50  0000 C CNN
F 2 "" H 4925 4650 50  0001 C CNN
F 3 "" H 4925 4650 50  0001 C CNN
	1    4925 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5F022925
P 3550 4650
F 0 "TP1" H 3550 4875 50  0000 C CNN
F 1 "TestPoint" V 3400 4600 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 3750 4650 50  0001 C CNN
F 3 "~" H 3750 4650 50  0001 C CNN
F 4 "N/A" H 3550 4650 50  0001 C CNN "Digi-Key Part Number"
	1    3550 4650
	1    0    0    -1  
$EndComp
Text GLabel 3550 4650 3    50   Output ~ 0
FET_2_APWM
Text GLabel 3900 4650 3    50   BiDi ~ 0
I2C_Data_Int
Text GLabel 4275 4650 3    50   BiDi ~ 0
I2C_Clock_Int
Text Notes 4000 4125 0    63   ~ 0
Testing Pads
$Sheet
S 825  5125 1925 925 
U 5F0F169D
F0 "MOSFET Driver" 50
F1 "MOSFETDriver.sch" 50
$EndSheet
$Sheet
S 3200 2800 1925 900 
U 5F112C11
F0 "CPU" 50
F1 "CPU.sch" 50
$EndSheet
$Sheet
S 825  1525 1950 800 
U 5F116BFA
F0 "Power Management" 50
F1 "PowerManagement.sch" 50
$EndSheet
Text Notes 1050 2000 0    98   ~ 0
Power Management
$Sheet
S 825  4000 1925 800 
U 5F11B4FC
F0 "Connectors" 50
F1 "Connectors.sch" 50
$EndSheet
$Sheet
S 825  2800 1975 900 
U 5F11FE7E
F0 "Peripherals" 50
F1 "Peripherals.sch" 50
$EndSheet
Wire Notes Line
	3200 4225 3200 5450
Wire Notes Line
	3200 5450 5275 5450
Wire Notes Line
	5275 5450 5275 4225
Wire Notes Line
	3200 4225 5275 4225
$EndSCHEMATC
