EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 6
Title "External Connectors"
Date "2020-07-29"
Rev "2"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1225 1575 2    50   BiDi ~ 0
I2C_Clock_Ext
Text GLabel 1225 1475 2    50   BiDi ~ 0
I2C_Data_Ext
Text GLabel 1225 1275 2    50   BiDi ~ 0
Batt_Next_Outer
Text GLabel 1225 1375 2    50   BiDi ~ 0
Batt_Next_Inner
Text GLabel 3925 2375 0    50   BiDi ~ 0
SYS_SWCLK
Text GLabel 3925 2275 0    50   BiDi ~ 0
SYS_SWDIO
Text GLabel 4825 2125 0    50   Output ~ 0
VTemp
Text GLabel 3900 1225 2    50   Input ~ 0
BATT_IN
Text GLabel 1575 2025 0    50   BiDi ~ 0
I2C_Clock_Ext
Text GLabel 1575 2125 0    50   BiDi ~ 0
I2C_Data_Ext
Text GLabel 1575 2325 0    50   BiDi ~ 0
Batt_Prev_Outer
Text GLabel 1575 2225 0    50   BiDi ~ 0
Batt_Prev_Inner
$Comp
L Li-ion_Thesis_Symbol_Library:Main_Connector_Out J?
U 1 1 5F11E44F
P 1025 1275
AR Path="/5F11E44F" Ref="J?"  Part="1" 
AR Path="/5F11B4FC/5F11E44F" Ref="J1"  Part="1" 
F 0 "J1" H 997 1157 50  0000 R CNN
F 1 "Main Connector Out" H 1775 1675 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Horizontal" H 1025 1275 50  0001 C CNN
F 3 "~" H 1025 1275 50  0001 C CNN
F 4 "S1111EC-06-ND" H -6625 -4675 50  0001 C CNN "Digi-Key Part Number"
F 5 "5055680671" H -6625 -4675 50  0001 C CNN "Manufacturer Part Number"
F 6 "1.151" H -6625 -4675 50  0001 C CNN "Cost (A$)"
	1    1025 1275
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:Temperture_Sensor_Header J?
U 1 1 5F11E458
P 5025 2125
AR Path="/5F11E458" Ref="J?"  Part="1" 
AR Path="/5F11B4FC/5F11E458" Ref="J5"  Part="1" 
F 0 "J5" H 4997 2057 50  0000 R CNN
F 1 "Temperature Sensor" H 5525 2525 50  0000 R CNN
F 2 "Li-ion_Thesis_Footprint_Library:Molex_Pico-Clasp_501331-0307_1x03-1MP_P1.00mm_Vertical" H 5025 2125 50  0001 C CNN
F 3 "~" H 5025 2125 50  0001 C CNN
F 4 "WM7880CT-ND" H -5375 -3875 50  0001 C CNN "Digi-Key Part Number"
F 5 "501331-0307" H -5375 -3875 50  0001 C CNN "Manufacturer Part Number"
F 6 "0.99" H -5375 -3875 50  0001 C CNN "Cost (A$)"
	1    5025 2125
	-1   0    0    1   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:Programming_Header J?
U 1 1 5F11E461
P 4125 2375
AR Path="/5F11E461" Ref="J?"  Part="1" 
AR Path="/5F11B4FC/5F11E461" Ref="J3"  Part="1" 
F 0 "J3" H 4097 2257 50  0000 R CNN
F 1 "Programming Header" H 4875 2025 50  0000 R CNN
F 2 "Li-ion_Thesis_Footprint_Library:Molex_Pico-Clasp_501331-0407_1x04-1MP_P1.00mm_Vertical" H 4125 2375 50  0001 C CNN
F 3 "~" H 4125 2375 50  0001 C CNN
F 4 "WM7881CT-ND" H -6325 -2525 50  0001 C CNN "Digi-Key Part Number"
F 5 "5013310407" H -6325 -2525 50  0001 C CNN "Manufacturer Part Number"
F 6 "0.893" H -6325 -2525 50  0001 C CNN "Cost (A$)"
	1    4125 2375
	-1   0    0    1   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:Main_Connector_In J?
U 1 1 5F11E46A
P 1775 2325
AR Path="/5F11E46A" Ref="J?"  Part="1" 
AR Path="/5F11B4FC/5F11E46A" Ref="J2"  Part="1" 
F 0 "J2" H 1747 2207 50  0000 R CNN
F 1 "Main Connector In" H 2525 1775 50  0000 R CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x06_P2.54mm_Horizontal" H 1775 2325 50  0001 C CNN
F 3 "~" H 1775 2325 50  0001 C CNN
F 4 "S5481-ND" H -7675 -3725 50  0001 C CNN "Digi-Key Part Number"
F 5 "5055680671" H -7675 -3725 50  0001 C CNN "Manufacturer Part Number"
F 6 "1.151" H -7675 -3725 50  0001 C CNN "Cost (A$)"
	1    1775 2325
	-1   0    0    1   
$EndComp
Wire Notes Line
	5375 2625 5375 775 
Wire Notes Line
	825  775  825  2625
Wire Notes Line
	825  2625 5375 2625
Wire Notes Line
	825  775  5375 775 
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F11E474
P 3925 2175
AR Path="/5F11E474" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E474" Ref="#PWR051"  Part="1" 
F 0 "#PWR051" H 3925 1925 50  0001 C CNN
F 1 "GND_Internal" V 3930 2048 50  0000 R CNN
F 2 "" H 3925 2175 50  0001 C CNN
F 3 "" H 3925 2175 50  0001 C CNN
	1    3925 2175
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F11E47A
P 3925 2475
AR Path="/5F11E47A" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E47A" Ref="#PWR052"  Part="1" 
F 0 "#PWR052" H 3925 2325 50  0001 C CNN
F 1 "+3.3V_Internal" V 3940 2603 50  0000 L CNN
F 2 "" H 3925 2475 50  0001 C CNN
F 3 "" H 3925 2475 50  0001 C CNN
	1    3925 2475
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F11E480
P 1225 1175
AR Path="/5F11E480" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E480" Ref="#PWR047"  Part="1" 
F 0 "#PWR047" H 1225 925 50  0001 C CNN
F 1 "GND_External" V 1225 1025 50  0000 R CNN
F 2 "" H 1225 1175 50  0001 C CNN
F 3 "" H 1225 1175 50  0001 C CNN
	1    1225 1175
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F11E486
P 1575 2425
AR Path="/5F11E486" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E486" Ref="#PWR048"  Part="1" 
F 0 "#PWR048" H 1575 2175 50  0001 C CNN
F 1 "GND_External" V 1575 2275 50  0000 R CNN
F 2 "" H 1575 2425 50  0001 C CNN
F 3 "" H 1575 2425 50  0001 C CNN
	1    1575 2425
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F11E48C
P 4825 2225
AR Path="/5F11E48C" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E48C" Ref="#PWR056"  Part="1" 
F 0 "#PWR056" H 4825 1975 50  0001 C CNN
F 1 "GND_Internal" H 4830 2052 50  0000 C CNN
F 2 "" H 4825 2225 50  0001 C CNN
F 3 "" H 4825 2225 50  0001 C CNN
	1    4825 2225
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F11E492
P 4825 2025
AR Path="/5F11E492" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E492" Ref="#PWR055"  Part="1" 
F 0 "#PWR055" H 4825 1875 50  0001 C CNN
F 1 "+3.3V_Internal" H 4840 2198 50  0000 C CNN
F 2 "" H 4825 2025 50  0001 C CNN
F 3 "" H 4825 2025 50  0001 C CNN
	1    4825 2025
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F11E498
P 3550 1675
AR Path="/5F11E498" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E498" Ref="#PWR050"  Part="1" 
F 0 "#PWR050" H 3550 1425 50  0001 C CNN
F 1 "GND_Internal" H 3800 1525 50  0000 R CNN
F 2 "" H 3550 1675 50  0001 C CNN
F 3 "" H 3550 1675 50  0001 C CNN
	1    3550 1675
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F11E49E
P 1575 2525
AR Path="/5F11E49E" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E49E" Ref="#PWR049"  Part="1" 
F 0 "#PWR049" H 1575 2375 50  0001 C CNN
F 1 "+5V_External" V 1575 2925 50  0000 C CNN
F 2 "" H 1575 2525 50  0001 C CNN
F 3 "" H 1575 2525 50  0001 C CNN
	1    1575 2525
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F11E4A4
P 1225 1075
AR Path="/5F11E4A4" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E4A4" Ref="#PWR046"  Part="1" 
F 0 "#PWR046" H 1225 925 50  0001 C CNN
F 1 "+5V_External" V 1225 1425 50  0000 C CNN
F 2 "" H 1225 1075 50  0001 C CNN
F 3 "" H 1225 1075 50  0001 C CNN
	1    1225 1075
	0    1    1    0   
$EndComp
Wire Notes Line
	2625 775  2625 2625
Text Notes 2675 1825 0    98   ~ 0
Internal
Text Notes 1925 1825 0    98   ~ 0
External
$Comp
L Device:Battery_Cell BT?
U 1 1 5F11E4AD
P 3550 1575
AR Path="/5F11E4AD" Ref="BT?"  Part="1" 
AR Path="/5F11B4FC/5F11E4AD" Ref="BT1"  Part="1" 
F 0 "BT1" H 3668 1671 50  0000 L CNN
F 1 "Battery_Cell" H 3668 1580 50  0000 L CNN
F 2 "Li-ion_Thesis_Footprint_Library:BatteryHolder_Keystone_1042_1x18650" V 3550 1635 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p27.pdf" V 3550 1635 50  0001 C CNN
F 4 "36-1042-ND" H 3550 1575 50  0001 C CNN "Digi-Key Part Number"
	1    3550 1575
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 1375 3550 1225
Wire Wire Line
	3550 1225 3600 1225
$Comp
L Device:Fuse F?
U 1 1 5F11E4B5
P 3750 1225
AR Path="/5F11E4B5" Ref="F?"  Part="1" 
AR Path="/5F11B4FC/5F11E4B5" Ref="F1"  Part="1" 
F 0 "F1" V 3553 1225 50  0000 C CNN
F 1 "Fuse" V 3644 1225 50  0000 C CNN
F 2 "Fuse:Fuse_1206_3216Metric" V 3680 1225 50  0001 C CNN
F 3 "~" H 3750 1225 50  0001 C CNN
F 4 "507-1895-1-ND" H 3750 1225 50  0001 C CNN "Digi-Key Part Number"
	1    3750 1225
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Male J?
U 1 1 5F11E4BB
P 4525 1300
AR Path="/5F11E4BB" Ref="J?"  Part="1" 
AR Path="/5F11B4FC/5F11E4BB" Ref="J4"  Part="1" 
F 0 "J4" H 4450 1475 50  0000 C CNN
F 1 "Conn_01x04_Male" H 4650 1575 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 4525 1300 50  0001 C CNN
F 3 "~" H 4525 1300 50  0001 C CNN
F 4 "N/A" H 4525 1300 50  0001 C CNN "Digi-Key Part Number"
	1    4525 1300
	1    0    0    -1  
$EndComp
Text GLabel 4725 1200 2    50   BiDi ~ 0
UARTRX
Text GLabel 4725 1300 2    50   BiDi ~ 0
UARTTX
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F11E4C3
P 4725 1500
AR Path="/5F11E4C3" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E4C3" Ref="#PWR054"  Part="1" 
F 0 "#PWR054" H 4725 1350 50  0001 C CNN
F 1 "+3.3V_Internal" V 4800 1775 50  0000 C CNN
F 2 "" H 4725 1500 50  0001 C CNN
F 3 "" H 4725 1500 50  0001 C CNN
	1    4725 1500
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F11E4C9
P 4725 1400
AR Path="/5F11E4C9" Ref="#PWR?"  Part="1" 
AR Path="/5F11B4FC/5F11E4C9" Ref="#PWR053"  Part="1" 
F 0 "#PWR053" H 4725 1150 50  0001 C CNN
F 1 "GND_Internal" V 4675 1075 50  0000 C CNN
F 2 "" H 4725 1400 50  0001 C CNN
F 3 "" H 4725 1400 50  0001 C CNN
	1    4725 1400
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
