EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 6
Title "External Peripherals"
Date "2020-07-29"
Rev "2"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 2750 1775 2    50   Output ~ 0
VCurrent
Text GLabel 5025 1900 2    50   Output ~ 0
VBat
Wire Wire Line
	4925 1350 4925 1500
Wire Wire Line
	4925 2300 4925 2450
Text GLabel 1450 1775 0    50   Input ~ 0
BATT_IN
$Comp
L Li-ion_Thesis_Symbol_Library:ACS70331EESATR-005B3 IC?
U 1 1 5F12283C
P 1450 1775
AR Path="/5F12283C" Ref="IC?"  Part="1" 
AR Path="/5F11FE7E/5F12283C" Ref="IC4"  Part="1" 
F 0 "IC4" H 2700 2375 50  0000 C CNN
F 1 "ACS70331EESATR-005B3" H 2700 2275 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:Allegro_QFN-12-10-1EP_3x3mm_P0.5mm" H 2500 2175 50  0001 L CNN
F 3 "https://www.allegromicro.com/~/media/Files/Datasheets/ACS70331-Datasheet.ashx?la=en&hash=6ED550430D2A1B2B4764E95E08812EDF083CCD64" H 2500 2075 50  0001 L CNN
F 4 "2.728" H -50 -175 50  0001 C CNN "Cost (A$)"
F 5 "620-1887-1-ND" H -50 -175 50  0001 C CNN "Digi-Key Part Number"
F 6 "ACS70331EESATR-005B3" H 1450 1775 50  0001 C CNN "Manufacturer Part Number"
	1    1450 1775
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5F122842
P 1450 2075
AR Path="/5F122842" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122842" Ref="#PWR057"  Part="1" 
F 0 "#PWR057" H 1450 1925 50  0001 C CNN
F 1 "+BATT" H 1150 2125 50  0000 L CNN
F 2 "" H 1450 2075 50  0001 C CNN
F 3 "" H 1450 2075 50  0001 C CNN
	1    1450 2075
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F122848
P 4925 1650
AR Path="/5F122848" Ref="R?"  Part="1" 
AR Path="/5F11FE7E/5F122848" Ref="R4"  Part="1" 
F 0 "R4" H 4995 1696 50  0000 L CNN
F 1 "100K 1%" H 4995 1605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4855 1650 50  0001 C CNN
F 3 "~" H 4925 1650 50  0001 C CNN
F 4 "RMCF0805FT100KCT-ND" H 4925 1650 50  0001 C CNN "Digi-Key Part Number"
	1    4925 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5F12284E
P 4925 2150
AR Path="/5F12284E" Ref="R?"  Part="1" 
AR Path="/5F11FE7E/5F12284E" Ref="R5"  Part="1" 
F 0 "R5" H 4995 2196 50  0000 L CNN
F 1 "200K 1%" H 4995 2105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4855 2150 50  0001 C CNN
F 3 "~" H 4925 2150 50  0001 C CNN
F 4 "RMCF0805FT200KCT-ND" H 4925 2150 50  0001 C CNN "Digi-Key Part Number"
	1    4925 2150
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR?
U 1 1 5F122854
P 4925 1350
AR Path="/5F122854" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122854" Ref="#PWR072"  Part="1" 
F 0 "#PWR072" H 4925 1200 50  0001 C CNN
F 1 "+BATT" H 4940 1523 50  0000 C CNN
F 2 "" H 4925 1350 50  0001 C CNN
F 3 "" H 4925 1350 50  0001 C CNN
	1    4925 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 1875 2900 1875
Wire Wire Line
	2650 1975 2900 1975
Wire Wire Line
	2900 1875 2900 1975
Connection ~ 2900 1975
Wire Wire Line
	1950 2675 1950 2725
Wire Wire Line
	1950 2725 2000 2725
Wire Wire Line
	2050 2725 2050 2675
Wire Wire Line
	2000 2725 2000 2775
Connection ~ 2000 2725
Wire Wire Line
	2000 2725 2050 2725
$Comp
L Li-ion_Thesis_Symbol_Library:Si8602AB-B-IS U?
U 1 1 5F122864
P 1825 3750
AR Path="/5F122864" Ref="U?"  Part="1" 
AR Path="/5F11FE7E/5F122864" Ref="U5"  Part="1" 
F 0 "U5" H 3125 4150 60  0000 C CNN
F 1 "Si8600AC-B-IS" H 3425 4050 60  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:Si8602AB-B-IS" H 3025 3990 60  0001 C CNN
F 3 "" H 1825 3750 60  0000 C CNN
F 4 "SI8600AC-B-ISRCT-ND" H 1825 3750 50  0001 C CNN "Digi-Key Part Number"
	1    1825 3750
	1    0    0    -1  
$EndComp
Text GLabel 1825 3850 0    50   BiDi ~ 0
I2C_Data_Ext
Text GLabel 1825 3950 0    50   BiDi ~ 0
I2C_Clock_Ext
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F12286C
P 1825 3750
AR Path="/5F12286C" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F12286C" Ref="#PWR058"  Part="1" 
F 0 "#PWR058" H 1825 3600 50  0001 C CNN
F 1 "+5V_External" H 1840 3923 50  0000 C CNN
F 2 "" H 1825 3750 50  0001 C CNN
F 3 "" H 1825 3750 50  0001 C CNN
	1    1825 3750
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F122872
P 1825 4050
AR Path="/5F122872" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122872" Ref="#PWR059"  Part="1" 
F 0 "#PWR059" H 1825 3800 50  0001 C CNN
F 1 "GND_External" H 1830 3877 50  0000 C CNN
F 2 "" H 1825 4050 50  0001 C CNN
F 3 "" H 1825 4050 50  0001 C CNN
	1    1825 4050
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F122878
P 4225 3750
AR Path="/5F122878" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122878" Ref="#PWR070"  Part="1" 
F 0 "#PWR070" H 4225 3600 50  0001 C CNN
F 1 "+3.3V_Internal" H 4240 3923 50  0000 C CNN
F 2 "" H 4225 3750 50  0001 C CNN
F 3 "" H 4225 3750 50  0001 C CNN
	1    4225 3750
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F12287E
P 4225 4050
AR Path="/5F12287E" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F12287E" Ref="#PWR071"  Part="1" 
F 0 "#PWR071" H 4225 3800 50  0001 C CNN
F 1 "GND_Internal" H 4230 3877 50  0000 C CNN
F 2 "" H 4225 4050 50  0001 C CNN
F 3 "" H 4225 4050 50  0001 C CNN
	1    4225 4050
	1    0    0    -1  
$EndComp
Text GLabel 4225 3850 2    50   BiDi ~ 0
I2C_Data_Int
Text GLabel 4225 3950 2    50   BiDi ~ 0
I2C_Clock_Int
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F122887
P 4925 2450
AR Path="/5F122887" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122887" Ref="#PWR073"  Part="1" 
F 0 "#PWR073" H 4925 2200 50  0001 C CNN
F 1 "GND_Internal" H 4930 2277 50  0000 C CNN
F 2 "" H 4925 2450 50  0001 C CNN
F 3 "" H 4925 2450 50  0001 C CNN
	1    4925 2450
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F12288D
P 2000 2775
AR Path="/5F12288D" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F12288D" Ref="#PWR061"  Part="1" 
F 0 "#PWR061" H 2000 2525 50  0001 C CNN
F 1 "GND_Internal" H 2005 2602 50  0000 C CNN
F 2 "" H 2000 2775 50  0001 C CNN
F 3 "" H 2000 2775 50  0001 C CNN
	1    2000 2775
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F122893
P 2900 2125
AR Path="/5F122893" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122893" Ref="#PWR065"  Part="1" 
F 0 "#PWR065" H 2900 1875 50  0001 C CNN
F 1 "GND_Internal" H 2905 1952 50  0000 C CNN
F 2 "" H 2900 2125 50  0001 C CNN
F 3 "" H 2900 2125 50  0001 C CNN
	1    2900 2125
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F122899
P 1950 1175
AR Path="/5F122899" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F122899" Ref="#PWR060"  Part="1" 
F 0 "#PWR060" H 1950 1025 50  0001 C CNN
F 1 "+3.3V_Internal" H 1625 1325 50  0000 C CNN
F 2 "" H 1950 1175 50  0001 C CNN
F 3 "" H 1950 1175 50  0001 C CNN
	1    1950 1175
	1    0    0    -1  
$EndComp
Wire Wire Line
	4925 1800 4925 1900
Wire Wire Line
	5025 1900 4925 1900
Connection ~ 4925 1900
Wire Wire Line
	4925 1900 4925 2000
Text Notes 2475 4375 0    79   ~ 0
External
Text Notes 3075 4375 0    79   ~ 0
Internal
Wire Notes Line
	3025 4400 3025 3450
$Comp
L Connector:TestPoint TP?
U 1 1 5F1228C8
P 4925 1900
AR Path="/5F1228C8" Ref="TP?"  Part="1" 
AR Path="/5F11FE7E/5F1228C8" Ref="TP8"  Part="1" 
F 0 "TP8" V 5000 2100 50  0000 C CNN
F 1 "TestPoint" V 4825 2100 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 5125 1900 50  0001 C CNN
F 3 "~" H 5125 1900 50  0001 C CNN
F 4 "N/A" H 4925 1900 50  0001 C CNN "Digi-Key Part Number"
	1    4925 1900
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 5F1228CE
P 2700 1700
AR Path="/5F1228CE" Ref="TP?"  Part="1" 
AR Path="/5F11FE7E/5F1228CE" Ref="TP7"  Part="1" 
F 0 "TP7" H 2825 1875 50  0000 C CNN
F 1 "TestPoint" H 2675 1950 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 2900 1700 50  0001 C CNN
F 3 "~" H 2900 1700 50  0001 C CNN
F 4 "N/A" H 2700 1700 50  0001 C CNN "Digi-Key Part Number"
	1    2700 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 1775 2700 1775
Wire Wire Line
	2700 1700 2700 1775
Connection ~ 2700 1775
Wire Wire Line
	2700 1775 2750 1775
Wire Wire Line
	2900 1975 2900 2125
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F1228D9
P 2050 1175
AR Path="/5F1228D9" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1228D9" Ref="#PWR062"  Part="1" 
F 0 "#PWR062" H 2050 925 50  0001 C CNN
F 1 "GND_Internal" H 1875 1000 50  0000 C CNN
F 2 "" H 2050 1175 50  0001 C CNN
F 3 "" H 2050 1175 50  0001 C CNN
	1    2050 1175
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5F1391C2
P 2625 5200
AR Path="/5F1391C2" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F1391C2" Ref="C?"  Part="1" 
AR Path="/5F11FE7E/5F1391C2" Ref="C17"  Part="1" 
F 0 "C17" H 2740 5246 50  0000 L CNN
F 1 "0.1uF" H 2740 5155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 2663 5050 50  0001 C CNN
F 3 "~" H 2625 5200 50  0001 C CNN
F 4 "1276-1043-1-ND" H 2625 5200 50  0001 C CNN "Digi-Key Part Number"
	1    2625 5200
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_External #PWR?
U 1 1 5F1391C8
P 2625 5350
AR Path="/5F1391C8" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F1391C8" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1391C8" Ref="#PWR064"  Part="1" 
F 0 "#PWR064" H 2625 5100 50  0001 C CNN
F 1 "GND_External" H 2630 5177 50  0000 C CNN
F 2 "" H 2625 5350 50  0001 C CNN
F 3 "" H 2625 5350 50  0001 C CNN
	1    2625 5350
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+5V_External #PWR?
U 1 1 5F1391CE
P 2625 5050
AR Path="/5F1391CE" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F1391CE" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1391CE" Ref="#PWR063"  Part="1" 
F 0 "#PWR063" H 2625 4900 50  0001 C CNN
F 1 "+5V_External" H 2640 5223 50  0000 C CNN
F 2 "" H 2625 5050 50  0001 C CNN
F 3 "" H 2625 5050 50  0001 C CNN
	1    2625 5050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F1391D4
P 3425 5200
AR Path="/5F1391D4" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F1391D4" Ref="C?"  Part="1" 
AR Path="/5F11FE7E/5F1391D4" Ref="C18"  Part="1" 
F 0 "C18" H 3540 5246 50  0000 L CNN
F 1 "0.1uF" H 3540 5155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3463 5050 50  0001 C CNN
F 3 "~" H 3425 5200 50  0001 C CNN
F 4 "1276-1043-1-ND" H 3425 5200 50  0001 C CNN "Digi-Key Part Number"
	1    3425 5200
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F1391DA
P 3425 5050
AR Path="/5F1391DA" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F1391DA" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1391DA" Ref="#PWR066"  Part="1" 
F 0 "#PWR066" H 3425 4900 50  0001 C CNN
F 1 "+3.3V_Internal" H 3440 5223 50  0000 C CNN
F 2 "" H 3425 5050 50  0001 C CNN
F 3 "" H 3425 5050 50  0001 C CNN
	1    3425 5050
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F1391E0
P 3425 5350
AR Path="/5F1391E0" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F1391E0" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1391E0" Ref="#PWR067"  Part="1" 
F 0 "#PWR067" H 3425 5100 50  0001 C CNN
F 1 "GND_Internal" H 3430 5177 50  0000 C CNN
F 2 "" H 3425 5350 50  0001 C CNN
F 3 "" H 3425 5350 50  0001 C CNN
	1    3425 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F1391E6
P 3775 1875
AR Path="/5F1391E6" Ref="C?"  Part="1" 
AR Path="/5F116BFA/5F1391E6" Ref="C?"  Part="1" 
AR Path="/5F11FE7E/5F1391E6" Ref="C19"  Part="1" 
F 0 "C19" H 3890 1921 50  0000 L CNN
F 1 "0.1uF" H 3890 1830 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 3813 1725 50  0001 C CNN
F 3 "~" H 3775 1875 50  0001 C CNN
F 4 "1276-1043-1-ND" H 3775 1875 50  0001 C CNN "Digi-Key Part Number"
	1    3775 1875
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:+3.3V_Internal #PWR?
U 1 1 5F1391EC
P 3775 1725
AR Path="/5F1391EC" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F1391EC" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1391EC" Ref="#PWR068"  Part="1" 
F 0 "#PWR068" H 3775 1575 50  0001 C CNN
F 1 "+3.3V_Internal" H 3790 1898 50  0000 C CNN
F 2 "" H 3775 1725 50  0001 C CNN
F 3 "" H 3775 1725 50  0001 C CNN
	1    3775 1725
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:GND_Internal #PWR?
U 1 1 5F1391F2
P 3775 2025
AR Path="/5F1391F2" Ref="#PWR?"  Part="1" 
AR Path="/5F116BFA/5F1391F2" Ref="#PWR?"  Part="1" 
AR Path="/5F11FE7E/5F1391F2" Ref="#PWR069"  Part="1" 
F 0 "#PWR069" H 3775 1775 50  0001 C CNN
F 1 "GND_Internal" H 3780 1852 50  0000 C CNN
F 2 "" H 3775 2025 50  0001 C CNN
F 3 "" H 3775 2025 50  0001 C CNN
	1    3775 2025
	1    0    0    -1  
$EndComp
Text Notes 2500 4825 0    50   ~ 0
Si860x
Text Notes 3325 4825 0    50   ~ 0
Si860x
Text Notes 3650 1500 0    50   ~ 0
ACS7033
$EndSCHEMATC
