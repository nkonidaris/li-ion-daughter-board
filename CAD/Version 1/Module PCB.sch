EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Daughter Board MMSCP"
Date "2020-04-15"
Rev "3.1"
Comp "Nicholas Konidaris"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 2700 1950 2    50   Output ~ 0
VCurrent
Text GLabel 7850 6250 2    50   BiDi ~ 0
I2C_Clock
Text GLabel 7850 6150 2    50   BiDi ~ 0
I2C_Data
Text GLabel 7850 5750 2    50   BiDi ~ 0
Batt_Next_Outer
Text GLabel 7850 5850 2    50   BiDi ~ 0
Batt_Next_Inner
Text GLabel 3900 2150 2    50   Output ~ 0
VBat
Wire Wire Line
	3900 2150 3600 2150
Wire Wire Line
	3600 2150 3600 2050
Wire Wire Line
	3600 2250 3600 2150
Connection ~ 3600 2150
Wire Wire Line
	3600 1600 3600 1750
Wire Wire Line
	3600 2550 3600 2700
Text GLabel 9200 1400 2    50   Input ~ 0
VBat
Text GLabel 5600 2000 0    50   BiDi ~ 0
I2C_Clock
Text GLabel 5600 2100 0    50   BiDi ~ 0
I2C_Data
Text GLabel 5600 1200 0    50   Input ~ 0
VCurrent
Text GLabel 5600 1400 0    50   Output ~ 0
FET_1_EN
Text GLabel 9200 1500 2    50   Output ~ 0
FET_1_APWM
Text GLabel 9200 1200 2    50   Output ~ 0
FET_1_BPWM
Text GLabel 5600 1900 0    50   Output ~ 0
FET_2_EN
Text GLabel 10100 4900 0    50   BiDi ~ 0
SYS_SWCLK
Text GLabel 10100 4800 0    50   BiDi ~ 0
SYS_SWDIO
Wire Wire Line
	10250 4700 10100 4700
Wire Wire Line
	10100 4700 10100 4600
Wire Wire Line
	10250 4800 10100 4800
Wire Wire Line
	10250 4900 10100 4900
Wire Wire Line
	10250 5000 10100 5000
Wire Wire Line
	10100 5000 10100 5150
Text GLabel 10100 6000 0    50   Output ~ 0
VTemp
Wire Wire Line
	10100 5800 10100 5900
Wire Wire Line
	10100 5900 10200 5900
Wire Wire Line
	10200 6000 10100 6000
Wire Wire Line
	10200 6100 10100 6100
Wire Wire Line
	10100 6100 10100 6200
Text GLabel 5600 1500 0    50   Input ~ 0
VTemp
Text GLabel 5600 1800 0    50   Output ~ 0
FET_2_BPWM
Text GLabel 5600 1600 0    50   Output ~ 0
FET_2_APWM
Text GLabel 3950 4400 0    50   Output ~ 0
FET_1_EN
Text GLabel 3950 4800 0    50   Output ~ 0
FET_1_APWM
Text GLabel 3950 4500 0    50   Output ~ 0
FET_1_BPWM
Text GLabel 1250 4400 0    50   Output ~ 0
FET_2_EN
Text GLabel 1250 4500 0    50   Output ~ 0
FET_2_BPWM
Text GLabel 1250 4800 0    50   Output ~ 0
FET_2_APWM
Text GLabel 5150 4300 2    50   Output ~ 0
BHO_1
Text GLabel 3850 7050 0    50   Input ~ 0
BHO_1
Text GLabel 5150 4500 2    50   Output ~ 0
BLO_1
Text GLabel 5150 4800 2    50   Output ~ 0
ALO_1
Text GLabel 5150 5000 2    50   Output ~ 0
AHO_1
Text GLabel 2450 4300 2    50   Output ~ 0
BHO_2
Text GLabel 2450 4500 2    50   Output ~ 0
BLO_2
Text GLabel 2450 4800 2    50   Output ~ 0
ALO_2
Text GLabel 2450 5000 2    50   Output ~ 0
AHO_2
Text GLabel 1250 5000 0    50   Output ~ 0
AHB_2
Text GLabel 1250 4300 0    50   Output ~ 0
BHB_2
Text GLabel 3950 5000 0    50   BiDi ~ 0
AHB_1
Text GLabel 3950 4300 0    50   BiDi ~ 0
BHB_1
Text GLabel 3850 6950 0    50   Input ~ 0
BLO_1
Text GLabel 3850 6850 0    50   Input ~ 0
ALO_1
Text GLabel 3850 6750 0    50   Input ~ 0
AHO_1
Text GLabel 1150 7050 0    50   Input ~ 0
BHO_2
Text GLabel 1150 6950 0    50   Input ~ 0
BLO_2
Text GLabel 1150 6850 0    50   Input ~ 0
ALO_2
Text GLabel 1150 6750 0    50   Input ~ 0
AHO_2
Wire Wire Line
	9200 2300 9400 2300
Text GLabel 5600 2300 0    50   BiDi ~ 0
SYS_SWCLK
Text GLabel 5600 2200 0    50   BiDi ~ 0
SYS_SWDIO
Text GLabel 9100 4900 2    50   Output ~ 0
BATT_IN
Text GLabel 1500 1950 0    50   Input ~ 0
BATT_IN
Wire Wire Line
	2450 4600 2850 4600
Wire Wire Line
	2850 4700 2450 4700
Wire Wire Line
	5150 4700 5550 4700
Wire Wire Line
	3850 7200 3800 7200
Wire Wire Line
	3800 7200 3800 7250
Wire Wire Line
	3800 7300 3850 7300
Wire Wire Line
	3800 7250 3750 7250
Connection ~ 3800 7250
Wire Wire Line
	3800 7250 3800 7300
Wire Wire Line
	5250 7000 5300 7000
Wire Wire Line
	5300 7000 5300 7100
Wire Wire Line
	5300 7350 5250 7350
Wire Wire Line
	5250 7250 5300 7250
Connection ~ 5300 7250
Wire Wire Line
	5300 7250 5300 7350
Wire Wire Line
	5250 7100 5300 7100
Connection ~ 5300 7100
Wire Wire Line
	1150 7200 1100 7200
Wire Wire Line
	1100 7200 1100 7250
Wire Wire Line
	1100 7300 1150 7300
Wire Wire Line
	1100 7250 1050 7250
Connection ~ 1100 7250
Wire Wire Line
	1100 7250 1100 7300
Wire Wire Line
	2550 7000 2600 7000
Wire Wire Line
	2600 7000 2600 7100
Wire Wire Line
	2600 7350 2550 7350
Wire Wire Line
	2550 7250 2600 7250
Connection ~ 2600 7250
Wire Wire Line
	2600 7250 2600 7350
Wire Wire Line
	2550 7100 2600 7100
Connection ~ 2600 7100
Wire Wire Line
	2600 7100 2600 7150
Wire Wire Line
	2650 7150 2600 7150
Connection ~ 2600 7150
Wire Wire Line
	2600 7150 2600 7250
Wire Wire Line
	5300 7100 5300 7150
Wire Wire Line
	5350 7150 5300 7150
Connection ~ 5300 7150
Wire Wire Line
	5300 7150 5300 7250
Text GLabel 9250 6250 0    50   BiDi ~ 0
I2C_Clock
Text GLabel 9250 6150 0    50   BiDi ~ 0
I2C_Data
Text GLabel 9250 5750 0    50   BiDi ~ 0
Batt_Prev_Outer
Text GLabel 9250 5850 0    50   BiDi ~ 0
Batt_Prev_Inner
Text GLabel 5250 6850 2    50   BiDi ~ 0
Batt_Next_Inner
Text GLabel 5250 6750 2    50   BiDi ~ 0
Batt_Prev_Inner
Text GLabel 5150 4900 2    50   BiDi ~ 0
Batt_Prev_Inner
Text GLabel 5150 4400 2    50   BiDi ~ 0
Batt_Next_Inner
Text GLabel 2550 6850 2    50   BiDi ~ 0
Batt_Next_Outer
Text GLabel 2450 4400 2    50   BiDi ~ 0
Batt_Next_Outer
Text GLabel 2550 6750 2    50   BiDi ~ 0
Batt_Prev_Outer
Text GLabel 2450 4900 2    50   BiDi ~ 0
Batt_Prev_Outer
NoConn ~ 9200 1700
NoConn ~ 5600 1300
Wire Wire Line
	10350 1900 10350 2000
Wire Wire Line
	9950 2050 9950 1900
Wire Wire Line
	9950 1900 10350 1900
Wire Wire Line
	10350 2300 10350 2400
Wire Wire Line
	10350 2400 10150 2400
Wire Wire Line
	10150 2400 9950 2400
Wire Wire Line
	9950 2400 9950 2350
Connection ~ 10150 2400
Text GLabel 4400 5850 0    50   BiDi ~ 0
AHB_1
Text GLabel 4700 5850 2    50   BiDi ~ 0
Batt_Prev_Inner
Text GLabel 4700 6200 2    50   BiDi ~ 0
Batt_Prev_Inner
Text GLabel 4400 6200 0    50   BiDi ~ 0
BHB_1
Text GLabel 2000 5800 2    50   BiDi ~ 0
Batt_Next_Outer
Text GLabel 2000 6150 2    50   BiDi ~ 0
Batt_Next_Outer
Text GLabel 1700 6150 0    50   Output ~ 0
AHB_2
Text GLabel 1700 5800 0    50   Output ~ 0
BHB_2
Wire Wire Line
	5550 4600 5150 4600
Wire Notes Line
	10750 4150 7300 4150
Wire Notes Line
	7300 4150 7300 6500
Wire Notes Line
	7300 6500 10750 6500
Wire Notes Line
	10750 4150 10750 6500
Text Notes 7350 4350 0    98   ~ 0
External Connectors
Wire Notes Line
	3350 7650 3350 3850
Wire Notes Line
	3350 3850 650  3850
Wire Notes Line
	650  3850 650  7650
Wire Notes Line
	650  7650 3350 7650
Wire Notes Line
	3400 7650 6050 7650
Wire Notes Line
	6050 7650 6050 3850
Wire Notes Line
	6050 3850 3400 3850
Wire Notes Line
	3400 3850 3400 7650
Text Notes 650  3750 0    98   ~ 0
MOSFET + Driver Outer
Text Notes 3400 3750 0    98   ~ 0
MOSFET + Driver Inner
Wire Notes Line
	4350 3250 4350 950 
Wire Notes Line
	4350 950  1000 950 
Wire Notes Line
	1000 950  1000 3250
Wire Notes Line
	1000 3250 4350 3250
Wire Notes Line
	10800 4050 10800 2900
Wire Notes Line
	10800 2900 7300 2900
Wire Notes Line
	7300 2900 7300 4050
Wire Notes Line
	7300 4050 10800 4050
Text Notes 1000 850  0    98   ~ 0
Sensors
Text Notes 7350 3050 0    98   ~ 0
Power Management
Wire Notes Line
	10950 550  4850 550 
Wire Notes Line
	4850 550  4850 2800
Wire Notes Line
	4850 2800 10950 2800
Wire Notes Line
	10950 550  10950 2800
Text Notes 4950 800  0    98   ~ 0
CPU + Jumper
$Comp
L Li-ion_Thesis_Symbol_Library:ACS70331EESATR-005B3 IC2
U 1 1 5E822D9E
P 1500 1950
F 0 "IC2" H 2750 2550 50  0000 C CNN
F 1 "ACS70331EESATR-005B3" H 2750 2450 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:Allegro_QFN-12-10-1EP_3x3mm_P0.5mm" H 2550 2350 50  0001 L CNN
F 3 "https://www.allegromicro.com/~/media/Files/Datasheets/ACS70331-Datasheet.ashx?la=en&hash=6ED550430D2A1B2B4764E95E08812EDF083CCD64" H 2550 2250 50  0001 L CNN
F 4 "Allegro Microsystems" H 2550 1950 50  0001 L CNN "Manufacturer_Name"
F 5 "ACS70331EESATR-005B3" H 2550 1850 50  0001 L CNN "Manufacturer_Part_Number"
F 6 "2.728" H 0   0   50  0001 C CNN "Cost (A$)"
F 7 "620-1887-1-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
	1    1500 1950
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR022
U 1 1 5E8288F1
P 9200 2100
F 0 "#PWR022" H 9200 1950 50  0001 C CNN
F 1 "+3.3V" V 9200 2350 50  0000 C CNN
F 2 "" H 9200 2100 50  0001 C CNN
F 3 "" H 9200 2100 50  0001 C CNN
	1    9200 2100
	0    1    1    0   
$EndComp
$Comp
L power:+12V #PWR018
U 1 1 5E829D91
P 7950 3450
F 0 "#PWR018" H 7950 3300 50  0001 C CNN
F 1 "+12V" H 7965 3623 50  0000 C CNN
F 2 "" H 7950 3450 50  0001 C CNN
F 3 "" H 7950 3450 50  0001 C CNN
	1    7950 3450
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR016
U 1 1 5E82A1BA
P 8250 3750
F 0 "#PWR016" H 8250 3500 50  0001 C CNN
F 1 "GND" H 8255 3577 50  0000 C CNN
F 2 "" H 8250 3750 50  0001 C CNN
F 3 "" H 8250 3750 50  0001 C CNN
	1    8250 3750
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR017
U 1 1 5E82A5E2
P 8800 3450
F 0 "#PWR017" H 8800 3300 50  0001 C CNN
F 1 "+3.3V" V 8815 3578 50  0000 L CNN
F 2 "" H 8800 3450 50  0001 C CNN
F 3 "" H 8800 3450 50  0001 C CNN
	1    8800 3450
	0    1    1    0   
$EndComp
$Comp
L Device:C C7
U 1 1 5E82BAE8
P 9900 3500
F 0 "C7" H 10015 3546 50  0000 L CNN
F 1 "100nF" H 10015 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9938 3350 50  0001 C CNN
F 3 "~" H 9900 3500 50  0001 C CNN
	1    9900 3500
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR034
U 1 1 5E82C1D3
P 9900 3350
F 0 "#PWR034" H 9900 3200 50  0001 C CNN
F 1 "+12V" H 9915 3523 50  0000 C CNN
F 2 "" H 9900 3350 50  0001 C CNN
F 3 "" H 9900 3350 50  0001 C CNN
	1    9900 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR035
U 1 1 5E82C572
P 9900 3650
F 0 "#PWR035" H 9900 3400 50  0001 C CNN
F 1 "GND" H 9905 3477 50  0000 C CNN
F 2 "" H 9900 3650 50  0001 C CNN
F 3 "" H 9900 3650 50  0001 C CNN
	1    9900 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR021
U 1 1 5E830056
P 9100 4800
F 0 "#PWR021" H 9100 4550 50  0001 C CNN
F 1 "GND" H 9105 4627 50  0000 C CNN
F 2 "" H 9100 4800 50  0001 C CNN
F 3 "" H 9100 4800 50  0001 C CNN
	1    9100 4800
	0    -1   -1   0   
$EndComp
$Comp
L power:+BATT #PWR02
U 1 1 5E8321F8
P 1500 2250
F 0 "#PWR02" H 1500 2100 50  0001 C CNN
F 1 "+BATT" H 1200 2300 50  0000 L CNN
F 2 "" H 1500 2250 50  0001 C CNN
F 3 "" H 1500 2250 50  0001 C CNN
	1    1500 2250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5E832C32
P 2050 2950
F 0 "#PWR05" H 2050 2700 50  0001 C CNN
F 1 "GND" H 2055 2777 50  0000 C CNN
F 2 "" H 2050 2950 50  0001 C CNN
F 3 "" H 2050 2950 50  0001 C CNN
	1    2050 2950
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR04
U 1 1 5E833D59
P 2000 1350
F 0 "#PWR04" H 2000 1200 50  0001 C CNN
F 1 "+3.3V" H 2015 1523 50  0000 C CNN
F 2 "" H 2000 1350 50  0001 C CNN
F 3 "" H 2000 1350 50  0001 C CNN
	1    2000 1350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR026
U 1 1 5E836550
P 9400 2300
F 0 "#PWR026" H 9400 2050 50  0001 C CNN
F 1 "GND" H 9405 2127 50  0000 C CNN
F 2 "" H 9400 2300 50  0001 C CNN
F 3 "" H 9400 2300 50  0001 C CNN
	1    9400 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR020
U 1 1 5E86625A
P 7850 6050
F 0 "#PWR020" H 7850 5800 50  0001 C CNN
F 1 "GND" V 7850 5800 50  0000 C CNN
F 2 "" H 7850 6050 50  0001 C CNN
F 3 "" H 7850 6050 50  0001 C CNN
	1    7850 6050
	0    -1   -1   0   
$EndComp
$Comp
L power:+12V #PWR08
U 1 1 5E866264
P 2850 4700
F 0 "#PWR08" H 2850 4550 50  0001 C CNN
F 1 "+12V" V 2850 4900 50  0000 C CNN
F 2 "" H 2850 4700 50  0001 C CNN
F 3 "" H 2850 4700 50  0001 C CNN
	1    2850 4700
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 5E88904C
P 3600 1900
F 0 "R1" H 3670 1946 50  0000 L CNN
F 1 "100K" H 3670 1855 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3530 1900 50  0001 C CNN
F 3 "~" H 3600 1900 50  0001 C CNN
	1    3600 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5E889410
P 3600 2400
F 0 "R2" H 3670 2446 50  0000 L CNN
F 1 "200K" H 3670 2355 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3530 2400 50  0001 C CNN
F 3 "~" H 3600 2400 50  0001 C CNN
	1    3600 2400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR010
U 1 1 5E889620
P 3600 2700
F 0 "#PWR010" H 3600 2450 50  0001 C CNN
F 1 "GND" H 3605 2527 50  0000 C CNN
F 2 "" H 3600 2700 50  0001 C CNN
F 3 "" H 3600 2700 50  0001 C CNN
	1    3600 2700
	1    0    0    -1  
$EndComp
$Comp
L power:+BATT #PWR09
U 1 1 5E8898AB
P 3600 1600
F 0 "#PWR09" H 3600 1450 50  0001 C CNN
F 1 "+BATT" H 3615 1773 50  0000 C CNN
F 2 "" H 3600 1600 50  0001 C CNN
F 3 "" H 3600 1600 50  0001 C CNN
	1    3600 1600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR030
U 1 1 5E8A400C
P 10100 4600
F 0 "#PWR030" H 10100 4350 50  0001 C CNN
F 1 "GND" H 10100 4450 50  0000 C CNN
F 2 "" H 10100 4600 50  0001 C CNN
F 3 "" H 10100 4600 50  0001 C CNN
	1    10100 4600
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR029
U 1 1 5E8A44B0
P 10100 5150
F 0 "#PWR029" H 10100 5000 50  0001 C CNN
F 1 "+3.3V" H 10115 5323 50  0000 C CNN
F 2 "" H 10100 5150 50  0001 C CNN
F 3 "" H 10100 5150 50  0001 C CNN
	1    10100 5150
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR031
U 1 1 5E8AAAD7
P 10100 5800
F 0 "#PWR031" H 10100 5650 50  0001 C CNN
F 1 "+3.3V" H 10115 5973 50  0000 C CNN
F 2 "" H 10100 5800 50  0001 C CNN
F 3 "" H 10100 5800 50  0001 C CNN
	1    10100 5800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR032
U 1 1 5E8AADD6
P 10100 6200
F 0 "#PWR032" H 10100 5950 50  0001 C CNN
F 1 "GND" H 10105 6027 50  0000 C CNN
F 2 "" H 10100 6200 50  0001 C CNN
F 3 "" H 10100 6200 50  0001 C CNN
	1    10100 6200
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR019
U 1 1 5E990F4B
P 7850 5950
F 0 "#PWR019" H 7850 5800 50  0001 C CNN
F 1 "+12V" V 7850 6200 50  0000 C CNN
F 2 "" H 7850 5950 50  0001 C CNN
F 3 "" H 7850 5950 50  0001 C CNN
	1    7850 5950
	0    1    1    0   
$EndComp
$Comp
L power:+12V #PWR015
U 1 1 5E9914EC
P 5550 4700
F 0 "#PWR015" H 5550 4550 50  0001 C CNN
F 1 "+12V" V 5550 4900 50  0000 C CNN
F 2 "" H 5550 4700 50  0001 C CNN
F 3 "" H 5550 4700 50  0001 C CNN
	1    5550 4700
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5E991BA3
P 2850 4600
F 0 "#PWR07" H 2850 4350 50  0001 C CNN
F 1 "GND" V 2850 4400 50  0000 C CNN
F 2 "" H 2850 4600 50  0001 C CNN
F 3 "" H 2850 4600 50  0001 C CNN
	1    2850 4600
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR014
U 1 1 5E992325
P 5550 4600
F 0 "#PWR014" H 5550 4350 50  0001 C CNN
F 1 "GND" V 5550 4400 50  0000 C CNN
F 2 "" H 5550 4600 50  0001 C CNN
F 3 "" H 5550 4600 50  0001 C CNN
	1    5550 4600
	0    -1   -1   0   
$EndComp
$Comp
L power:+BATT #PWR011
U 1 1 5EB9014D
P 3750 7250
AR Path="/5EB9014D" Ref="#PWR011"  Part="1" 
AR Path="/5E874FE6/5EB9014D" Ref="#PWR?"  Part="1" 
F 0 "#PWR011" H 3750 7100 50  0001 C CNN
F 1 "+BATT" H 3765 7423 50  0000 C CNN
F 2 "" H 3750 7250 50  0001 C CNN
F 3 "" H 3750 7250 50  0001 C CNN
	1    3750 7250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR013
U 1 1 5EB90DBD
P 5350 7150
F 0 "#PWR013" H 5350 6900 50  0001 C CNN
F 1 "GND" H 5355 6977 50  0000 C CNN
F 2 "" H 5350 7150 50  0001 C CNN
F 3 "" H 5350 7150 50  0001 C CNN
	1    5350 7150
	0    -1   -1   0   
$EndComp
$Comp
L power:+BATT #PWR01
U 1 1 5EB93D70
P 1050 7250
AR Path="/5EB93D70" Ref="#PWR01"  Part="1" 
AR Path="/5E874FE6/5EB93D70" Ref="#PWR?"  Part="1" 
F 0 "#PWR01" H 1050 7100 50  0001 C CNN
F 1 "+BATT" H 1065 7423 50  0000 C CNN
F 2 "" H 1050 7250 50  0001 C CNN
F 3 "" H 1050 7250 50  0001 C CNN
	1    1050 7250
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5EB93D78
P 2650 7150
F 0 "#PWR06" H 2650 6900 50  0001 C CNN
F 1 "GND" H 2655 6977 50  0000 C CNN
F 2 "" H 2650 7150 50  0001 C CNN
F 3 "" H 2650 7150 50  0001 C CNN
	1    2650 7150
	0    -1   -1   0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:FDMQ86530L U1
U 1 1 5E932787
P 1850 7350
F 0 "U1" H 1850 8220 50  0000 C CNN
F 1 "FDMQ86530L" H 1850 8129 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:IC_FDMQ8205A" H 1550 7850 50  0001 L BNN
F 3 "ON Semiconductor" H 1550 7750 50  0001 L BNN
F 4 "FDMQ86530L" H 1850 7350 50  0001 C CNN "Manufacturer Part Number"
F 5 "FDMQ86530LCT-ND" H 1850 7350 50  0001 C CNN "Digi-Key Part Number"
F 6 "3.6576" H 0   0   50  0001 C CNN "Cost (A$)"
	1    1850 7350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5E958D31
P 9450 3500
F 0 "C5" H 9565 3546 50  0000 L CNN
F 1 "100nF" H 9565 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9488 3350 50  0001 C CNN
F 3 "~" H 9450 3500 50  0001 C CNN
	1    9450 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR028
U 1 1 5E958D37
P 9450 3650
F 0 "#PWR028" H 9450 3400 50  0001 C CNN
F 1 "GND" H 9455 3477 50  0000 C CNN
F 2 "" H 9450 3650 50  0001 C CNN
F 3 "" H 9450 3650 50  0001 C CNN
	1    9450 3650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR027
U 1 1 5E96AC50
P 9450 3350
F 0 "#PWR027" H 9450 3200 50  0001 C CNN
F 1 "+3.3V" H 9465 3523 50  0000 C CNN
F 2 "" H 9450 3350 50  0001 C CNN
F 3 "" H 9450 3350 50  0001 C CNN
	1    9450 3350
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:MIC4606-2YTS-TR IC3
U 1 1 5E924D27
P 3950 4300
F 0 "IC3" H 4550 4565 50  0000 C CNN
F 1 "MIC4606-2YTS-TR" H 4550 4474 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:MIC4606_TSSOP-16_4.4x5mm_P0.65mm" H 5000 4400 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/mic4606-2yts-tr/microchip-technology" H 5000 4300 50  0001 L CNN
F 4 "MIC4606-2YTS-CT-ND" H 5000 4200 50  0001 L CNN "Digi-Key Part Number"
F 5 "1.2" H 5000 4100 50  0001 L CNN "Height"
F 6 "Microchip" H 5000 3800 50  0001 L CNN "Manufacturer Name"
F 7 "MIC4606-2YTS-TR" H 5000 3700 50  0001 L CNN "Manufacturer Part Number"
F 8 "2.962" H 0   0   50  0001 C CNN "Cost (A$)"
	1    3950 4300
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:AZ1117-3.3 IC4
U 1 1 5E821E3B
P 8250 3450
F 0 "IC4" H 8350 3700 50  0000 C CNN
F 1 "ZLDO1117G33TA" H 8400 3600 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:Voltage_Regulator_SOT-223" H 9600 3550 50  0001 L CNN
F 3 "https://www.diodes.com/assets/Datasheets/AZ1117.pdf" H 9600 3450 50  0001 L CNN
F 4 "0.493" H 8250 3450 50  0001 C CNN "Cost (A$)"
F 5 "AZ1117IH-3.3TRG1DICT-ND" H 8250 3450 50  0001 C CNN "Digi-Key Part Number"
F 6 "AZ1117IH-3.3TRG1" H 8250 3450 50  0001 C CNN "Manufacturer Part Number"
	1    8250 3450
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:FDMQ86530L U2
U 1 1 5E931BEE
P 4550 7350
F 0 "U2" H 4550 8220 50  0000 C CNN
F 1 "FDMQ86530L" H 4550 8129 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:IC_FDMQ8205A" H 4250 7850 50  0001 L BNN
F 3 "ON Semiconductor" H 4250 7750 50  0001 L BNN
F 4 "FDMQ86530L" H 4550 7350 50  0001 C CNN "Manufacturer Part Number"
F 5 "FDMQ86530LCT-ND" H 4550 7350 50  0001 C CNN "Digi-Key Part Number"
F 6 "3.6576" H 0   0   50  0001 C CNN "Cost (A$)"
	1    4550 7350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR024
U 1 1 5EA5F659
P 9250 6050
F 0 "#PWR024" H 9250 5800 50  0001 C CNN
F 1 "GND" V 9250 5800 50  0000 C CNN
F 2 "" H 9250 6050 50  0001 C CNN
F 3 "" H 9250 6050 50  0001 C CNN
	1    9250 6050
	0    1    1    0   
$EndComp
$Comp
L power:+12V #PWR023
U 1 1 5EA5F663
P 9250 5950
F 0 "#PWR023" H 9250 5800 50  0001 C CNN
F 1 "+12V" V 9250 6200 50  0000 C CNN
F 2 "" H 9250 5950 50  0001 C CNN
F 3 "" H 9250 5950 50  0001 C CNN
	1    9250 5950
	0    -1   -1   0   
$EndComp
$Comp
L Jumper:SolderJumper_2_Open JP1
U 1 1 5EA70FEA
P 10350 2150
F 0 "JP1" V 10304 2218 50  0000 L CNN
F 1 "SolderJumper_2_Open" V 10200 1900 50  0000 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 10350 2150 50  0001 C CNN
F 3 "~" H 10350 2150 50  0001 C CNN
	1    10350 2150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR033
U 1 1 5EA729E6
P 10150 2400
F 0 "#PWR033" H 10150 2150 50  0001 C CNN
F 1 "GND" H 10155 2227 50  0000 C CNN
F 2 "" H 10150 2400 50  0001 C CNN
F 3 "" H 10150 2400 50  0001 C CNN
	1    10150 2400
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5EA743E9
P 9950 2200
F 0 "C6" H 10065 2246 50  0000 L CNN
F 1 "100nF" H 10065 2155 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 9988 2050 50  0001 C CNN
F 3 "~" H 9950 2200 50  0001 C CNN
	1    9950 2200
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:Main_Connector_Out J1
U 1 1 5E9035AE
P 7650 5950
F 0 "J1" H 7622 5832 50  0000 R CNN
F 1 "Main Connector Out" H 8250 6450 50  0000 R CNN
F 2 "Li-ion_Thesis_Footprint_Library:MOLEX_Micro-Lock_Plus_P1.25mm" H 7650 5950 50  0001 C CNN
F 3 "~" H 7650 5950 50  0001 C CNN
F 4 "WM17037CT-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "5055680671" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 6 "1.151" H 0   0   50  0001 C CNN "Cost (A$)"
	1    7650 5950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5E92E84C
P 4550 5850
F 0 "C3" V 4400 5500 50  0000 L CNN
F 1 "100nF" V 4350 5750 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4588 5700 50  0001 C CNN
F 3 "~" H 4550 5850 50  0001 C CNN
	1    4550 5850
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 5E92F08C
P 4550 6200
F 0 "C4" V 4400 5850 50  0000 L CNN
F 1 "100nF" V 4350 6100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4588 6050 50  0001 C CNN
F 3 "~" H 4550 6200 50  0001 C CNN
	1    4550 6200
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 5E92D009
P 1850 5800
F 0 "C1" V 1700 5450 50  0000 L CNN
F 1 "100nF" V 1650 5700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1888 5650 50  0001 C CNN
F 3 "~" H 1850 5800 50  0001 C CNN
	1    1850 5800
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:MIC4606-2YTS-TR IC1
U 1 1 5E92B19D
P 1250 4300
F 0 "IC1" H 1850 4565 50  0000 C CNN
F 1 "MIC4606-2YTS-TR" H 1850 4474 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:MIC4606_TSSOP-16_4.4x5mm_P0.65mm" H 2300 4400 50  0001 L CNN
F 3 "https://www.arrow.com/en/products/mic4606-2yts-tr/microchip-technology" H 2300 4300 50  0001 L CNN
F 4 "MIC4606-2YTS-CT-ND" H 2300 4200 50  0001 L CNN "Digi-Key Part Number"
F 5 "1.2" H 2300 4100 50  0001 L CNN "Height"
F 6 "Microchip" H 2300 3800 50  0001 L CNN "Manufacturer Name"
F 7 "MIC4606-2YTS-TR" H 2300 3700 50  0001 L CNN "Manufacturer Part Number"
F 8 "2.962" H 0   0   50  0001 C CNN "Cost (A$)"
	1    1250 4300
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5E932808
P 1850 6150
F 0 "C2" V 1700 5800 50  0000 L CNN
F 1 "100nF" V 1650 6050 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 1888 6000 50  0001 C CNN
F 3 "~" H 1850 6150 50  0001 C CNN
	1    1850 6150
	0    1    1    0   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:Temperture_Sensor_Header J4
U 1 1 5E9352AB
P 10400 6000
F 0 "J4" H 10372 5932 50  0000 R CNN
F 1 "Temperature Sensor" H 10950 5500 50  0000 R CNN
F 2 "Li-ion_Thesis_Footprint_Library:Molex_Pico-Clasp_501331-0307_1x03-1MP_P1.00mm_Vertical" H 10400 6000 50  0001 C CNN
F 3 "~" H 10400 6000 50  0001 C CNN
F 4 "WM7880CT-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "501331-0307" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 6 "0.99" H 0   0   50  0001 C CNN "Cost (A$)"
	1    10400 6000
	-1   0    0    1   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:Programming_Header J5
U 1 1 5E935723
P 10450 4900
F 0 "J5" H 10422 4782 50  0000 R CNN
F 1 "Programming Header" H 11100 4350 50  0000 R CNN
F 2 "Li-ion_Thesis_Footprint_Library:Molex_Pico-Clasp_501331-0407_1x04-1MP_P1.00mm_Vertical" H 10450 4900 50  0001 C CNN
F 3 "~" H 10450 4900 50  0001 C CNN
F 4 "WM7881CT-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "5013310407" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 6 "0.893" H 0   0   50  0001 C CNN "Cost (A$)"
	1    10450 4900
	-1   0    0    1   
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:Battery_Connector J2
U 1 1 5E945273
P 8900 4800
F 0 "J2" H 9000 5050 50  0000 C CNN
F 1 "Battery_Connector" H 9000 4950 50  0000 C CNN
F 2 "Li-ion_Thesis_Footprint_Library:MOLEX_Micro-Lock_Plus_Con2_P1.25mm" H 8900 4800 50  0001 C CNN
F 3 "~" H 8900 4800 50  0001 C CNN
F 4 "WM7893CT-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "5015680207" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 6 "0.819" H 0   0   50  0001 C CNN "Cost (A$)"
	1    8900 4800
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:Main_Connector_In J3
U 1 1 5E92DBAD
P 9450 6050
F 0 "J3" H 9422 5932 50  0000 R CNN
F 1 "Main Connector In" H 9950 5500 50  0000 R CNN
F 2 "Li-ion_Thesis_Footprint_Library:MOLEX_Micro-Lock_Plus_P1.25mm" H 9450 6050 50  0001 C CNN
F 3 "~" H 9450 6050 50  0001 C CNN
F 4 "WM17037CT-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
F 5 "5055680671" H 0   0   50  0001 C CNN "Manufacturer Part Number"
F 6 "1.151" H 0   0   50  0001 C CNN "Cost (A$)"
	1    9450 6050
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR0101
U 1 1 5E99576A
P 8800 3600
F 0 "#PWR0101" H 8800 3450 50  0001 C CNN
F 1 "+3.3V" V 8815 3728 50  0000 L CNN
F 2 "" H 8800 3600 50  0001 C CNN
F 3 "" H 8800 3600 50  0001 C CNN
	1    8800 3600
	0    1    1    0   
$EndComp
$Comp
L Device:LED D1
U 1 1 5E92F6E2
P 10550 1350
F 0 "D1" H 10543 1566 50  0000 C CNN
F 1 "LED" H 10543 1475 50  0000 C CNN
F 2 "LED_SMD:LED_0805_2012Metric" H 10550 1350 50  0001 C CNN
F 3 "~" H 10550 1350 50  0001 C CNN
	1    10550 1350
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR03
U 1 1 5E930078
P 10550 1500
F 0 "#PWR03" H 10550 1250 50  0001 C CNN
F 1 "GND" H 10555 1327 50  0000 C CNN
F 2 "" H 10550 1500 50  0001 C CNN
F 3 "" H 10550 1500 50  0001 C CNN
	1    10550 1500
	1    0    0    -1  
$EndComp
$Comp
L Li-ion_Thesis_Symbol_Library:STM32G030F6P6 U3
U 1 1 5E96B826
P 5600 1200
F 0 "U3" H 7400 1587 60  0000 C CNN
F 1 "STM32G030F6P6" H 7400 1481 60  0000 C CNN
F 2 "Package_SO:TSSOP-20_4.4x6.5mm_P0.65mm" H 7400 1440 60  0001 C CNN
F 3 "https://www.st.com/resource/en/datasheet/stm32g030f6.pdf" H 5600 1200 60  0001 C CNN
F 4 "1.782" H 0   0   50  0001 C CNN "Cost (A$)"
F 5 "STM32G030F6P6-ND" H 0   0   50  0001 C CNN "Digi-Key Part Number"
	1    5600 1200
	1    0    0    -1  
$EndComp
Text GLabel 9200 1900 2    50   BiDi ~ 0
NRST
Text GLabel 10150 1900 1    50   BiDi ~ 0
NRST
Text GLabel 5600 1700 0    50   Output ~ 0
LED
Text GLabel 10550 900  1    50   Output ~ 0
LED
NoConn ~ 9200 1300
$Comp
L power:GND #PWR0102
U 1 1 5E97033D
P 2950 2300
F 0 "#PWR0102" H 2950 2050 50  0001 C CNN
F 1 "GND" H 2955 2127 50  0000 C CNN
F 2 "" H 2950 2300 50  0001 C CNN
F 3 "" H 2950 2300 50  0001 C CNN
	1    2950 2300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 2050 2950 2050
Wire Wire Line
	2700 2150 2950 2150
Wire Wire Line
	2950 2050 2950 2150
Connection ~ 2950 2150
Wire Wire Line
	2950 2150 2950 2250
Wire Wire Line
	2700 2250 2950 2250
Connection ~ 2950 2250
Wire Wire Line
	2950 2250 2950 2300
Wire Wire Line
	2000 2850 2000 2900
Wire Wire Line
	2000 2900 2050 2900
Wire Wire Line
	2100 2900 2100 2850
Wire Wire Line
	2050 2900 2050 2950
Connection ~ 2050 2900
Wire Wire Line
	2050 2900 2100 2900
$Comp
L Device:R R3
U 1 1 5E97C3F7
P 10550 1050
F 0 "R3" H 10620 1096 50  0000 L CNN
F 1 "65" H 10620 1005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 10480 1050 50  0001 C CNN
F 3 "~" H 10550 1050 50  0001 C CNN
	1    10550 1050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5E98EEBA
P 10400 3500
F 0 "C8" H 10515 3546 50  0000 L CNN
F 1 "100nF" H 10515 3455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 10438 3350 50  0001 C CNN
F 3 "~" H 10400 3500 50  0001 C CNN
	1    10400 3500
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR012
U 1 1 5E98EEC4
P 10400 3350
F 0 "#PWR012" H 10400 3200 50  0001 C CNN
F 1 "+12V" H 10415 3523 50  0000 C CNN
F 2 "" H 10400 3350 50  0001 C CNN
F 3 "" H 10400 3350 50  0001 C CNN
	1    10400 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR025
U 1 1 5E98EECE
P 10400 3650
F 0 "#PWR025" H 10400 3400 50  0001 C CNN
F 1 "GND" H 10405 3477 50  0000 C CNN
F 2 "" H 10400 3650 50  0001 C CNN
F 3 "" H 10400 3650 50  0001 C CNN
	1    10400 3650
	1    0    0    -1  
$EndComp
$EndSCHEMATC
